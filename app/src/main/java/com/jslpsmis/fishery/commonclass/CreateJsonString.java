package com.jslpsmis.fishery.commonclass;

import com.google.gson.Gson;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CreateJsonString {
    public static void CreateJson(String tableName) {
        if(tableName.equals("tbltracFisheryPlan")){
            String json1 = new Gson().toJson(AddWaterbodynew.fisheryPlantblList);
            JSONArray JSONArray_Jsrp = null;
            JSONObject jsonObjectcombined = new JSONObject();
            try {
                JSONArray_Jsrp = new JSONArray(json1);
                jsonObjectcombined.put("tbltracFisheryPlan", JSONArray_Jsrp);
                AddWaterbodynew.sDataFisheryPlantbl = jsonObjectcombined.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
