package com.jslpsmis.fishery.commonclass;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.jslpsmis.fishery.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by sonu on 4/4/2018.
 */

public class Loader  {

   public static PopupWindow popup;

    public Loader(Context context, View v){
        LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = li.inflate(R.layout.loader, (ViewGroup) v.findViewById(R.id.loader));
        popup = new PopupWindow(layout, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT, false);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ProgressBar progressBar = (ProgressBar)layout.findViewById(R.id.spin_kit);
        FadingCircle doubleBounce = new FadingCircle();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }
}

