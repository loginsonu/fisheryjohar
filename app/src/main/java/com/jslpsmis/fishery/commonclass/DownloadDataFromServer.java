package com.jslpsmis.fishery.commonclass;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.interactor.Counter;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

public class DownloadDataFromServer implements Counter {
    private String flag,whr,whr1;
    public ProgressDialog dialog;
    Dataprovider dataprovider;
    private  Context context;
    private String tableIndentifier;
    int count=0;

    public DownloadDataFromServer(Context context,String whr,String whr1,String flag,String tableIndentifier){
        this.context = context;
        this.flag = flag;
        this.whr = whr;
        this.whr1 = whr1;
        this.tableIndentifier = tableIndentifier;
        dataprovider = new Dataprovider(context);
        new DownloadDataApi().execute();
    }

    @Override
    public void totalmembercount(int counts) {
        count=counts;
    }

    public class  DownloadDataApi extends AsyncTask<String,Void,String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(dialog==null){
                dialog = new ProgressDialog(context);
                dialog.setMessage("आवश्यक डेटा डाउनलोड किया जा रहा है");
                dialog.setCancelable(false);
                dialog.show();
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialog!=null){
                dialog.dismiss();
            }
            //producer member download.
            if(tableIndentifier.equals("producergrpmemtb")){
                JSONArray array = null;
                String pgMemName;
                String shg;
                List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();
                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            ProducerGroupMemberActivityModel model2 = new ProducerGroupMemberActivityModel();
                            model2.setDistrictCode(object.optString("DistrictCode"));
                            model2.setBlockCode(object.optString("BlockCode"));
                            model2.setClusterCode(object.optString("ClusterCode"));
                            model2.setPgCode(object.optString("PGCode"));
                            model2.setGroupCode(object.optString("GroupCode"));
                            model2.setGroupMemberCode(object.optString("Group_M_Code"));
                            model2.setFatherName(object.optString("FatherName"));
                            model2.setHusbandName(object.optString("HusbandName"));
                            if(object.optString("MemberName_H").equals("null")) {
                                pgMemName = object.optString("MemberName");
                                model2.setMemberName(object.optString("MemberName"));
                                count++;
                            }else{
                                pgMemName = object.optString("MemberName_H");
                                model2.setMemberName(object.optString("MemberName_H"));
                                count++;
                            }

                            if(object.optString("Group_Name_H").equals("null")) {
                                shg = object.optString("Group_Name");
                                model2.setGroupName(object.optString("Group_Name"));
                            }else{
                                shg = object.optString("Group_Name_H");
                                model2.setGroupName(object.optString("Group_Name_H"));
                            }
                            listProducerMember.add(model2);
                            if(Constant.action.equals("insert")){
                                boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                        , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                        , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "insert");

                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                            , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                            , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "update");
                                }
                                boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                        , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                        , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "insert");

                            }
                        }
                        Toast.makeText(context,"producer members Downloaded",Toast.LENGTH_SHORT).show();
                        }
                        else{
                        Toast.makeText(context,"No producer member data found",Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }

            }

            //water body download
            if(tableIndentifier.equals("tblWaterBodyType")){
                JSONArray array = null;
                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            if(Constant.action.equals("insert")){
                                boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"),object.optString("TypeCode"),"insert");

                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"), object.optString("TypeCode"), "delete");
                                }
                                boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"),object.optString("TypeCode"),"insert");

                            }

                        }


                        Toast.makeText(context,"Water body Type Downloaded",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context,"No water body data found",Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }
            //waterbodyowner
            if(tableIndentifier.equals("tblWaterBodyOwner")){
                JSONArray array = null;

                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            if(Constant.action.equals("insert")){
                                boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"),object.optString("OwnerCode"),"insert");
                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"), object.optString("OwnerCode"), "delete");
                                }
                                boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"),object.optString("OwnerCode"),"insert");
                            }
                        }
                        Toast.makeText(context,"Water body owner Type Downloaded",Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(context,"No water body owner data found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }

            }


            //Selected Fishery
            if(tableIndentifier.equals("tblSelectedFishery")){
                JSONArray array = null;
                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            if(Constant.action.equals("insert")){
                                boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"),object.optString("FisheryCode"),"insert");
                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"), object.optString("FisheryCode"), "delete");
                                }
                                boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"),object.optString("FisheryCode"),"insert");
                            }
                        }

                        Toast.makeText(context,"Selected Fishery Downloaded",Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(context,"No Data of Selected Fishery Found",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }

            if(tableIndentifier.equals("tblItemRequiredFishBreeding")){
                JSONArray array = null;
                String itemName;

                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            if(!object.optString("ItemNamehindi").equals("")){
                                itemName = object.optString("ItemNamehindi");
                            }else{
                                itemName = object.optString("ItemName");
                            }
                            if(Constant.action.equals("insert")){

                                boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName,object.optString("ItemCode"),object.optString("Budgetcode"),"insert");

                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName, object.optString("ItemCode"), object.optString("Budgetcode"),"delete");
                                }
                                boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName,object.optString("ItemCode"),object.optString("Budgetcode"),"insert");
                            }
                        }
                        Toast.makeText(context,"Item Required Fish Breeding Downloaded",Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(context,"No Data of Item Required Fish Breeding Found",Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }

            if(tableIndentifier.equals("tblRateFishBreedingPerUnit")){
                JSONArray array = null;
                String itemName;
                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            if(!object.optString("ItemNamehindi").equals("")){
                                itemName = object.optString("ItemNamehindi");
                            }else{
                                itemName = object.optString("ItemName");
                            }
                            if(Constant.action.equals("insert")){

                                boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                        object.optString("Code"),"insert",object.optString("AreaDependent"));
                            }
                            else{
                                if (i == 0) {
                                    //for deleting data for the first time
                                    boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                            object.optString("Code"),"delete","");
                                }
                                boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                        object.optString("Code"),"insert",object.optString("AreaDependent"));

                            }

                        }

                        Toast.makeText(context,"Item Rate Map table Downloaded",Toast.LENGTH_SHORT).show();


                    }else{
                        Toast.makeText(context,"No Data of Item Rate Map table Found",Toast.LENGTH_SHORT).show();

                    }


                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }

            //tblAddWaterBody
            if(tableIndentifier.equals("tblAddWaterBody")){
                JSONArray array = null;
                String PgCode;
                String GrpMemCode;
                String GrpCode;
                String WaterBodyCode;
                String WaterBodyOwnerCode;
                String NoOfMonthWaterAvailable;
                String SelectedFishery;
                String IsFisheryHappening;
                String IsTrainingTaken;
                String ExpectedAreaInDecimal;
                String TotalProductionInQuintal;
                String TotalIncome;
                String WaterBodyPicPath;
                String FromWhereTraining;
                String Lat;
                String Long;
                String Location;
                String waterbodyid;
                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            PgCode = object.optString("PgCode");
                            GrpMemCode = object.optString("GrpMemCode");
                            GrpCode = object.optString("GrpCode");
                            WaterBodyCode = object.optString("WaterBodyCode");
                            WaterBodyOwnerCode = object.optString("WaterBodyOwnerCode");
                            NoOfMonthWaterAvailable = object.optString("NoOfMonthWaterAvailable");
                            IsFisheryHappening = object.optString("IsFisheryHappening");
                            IsTrainingTaken = object.optString("IsTrainingTaken");
                            ExpectedAreaInDecimal = object.optString("ExpectedAreaInDecimal");
                            TotalProductionInQuintal = object.optString("TotalProductionInQuintal");
                            TotalIncome = object.optString("TotalIncome");
                            WaterBodyPicPath = "/storage/emulated/0/Pictures/"+object.optString("WaterBodyPicPath");
                            FromWhereTraining = object.optString("FromWhereTraining");
                            Lat = object.optString("Lat");
                            Long = object.optString("Long");
                            Location = object.optString("Location");
                            waterbodyid = object.optString("WaterBodyId");
                            SelectedFishery = object.optString("SelectedFishery");

                       boolean check = dataprovider.AddWaterBodyMaster(waterbodyid,PgCode,GrpMemCode,GrpCode,WaterBodyCode,
                               WaterBodyOwnerCode,NoOfMonthWaterAvailable,SelectedFishery,IsFisheryHappening,IsTrainingTaken,FromWhereTraining,
                               ExpectedAreaInDecimal,TotalProductionInQuintal,TotalIncome,WaterBodyPicPath,Lat,Long,Location,"","insert");

                        }

                        Toast.makeText(context,"waterbody data Downloaded",Toast.LENGTH_SHORT).show();


                    }else{
                        Toast.makeText(context,"No Data of water waterbody Found",Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }

            //tbltracFisheryPlan

            if(tableIndentifier.equals("tbltracFisheryPlan")){
                JSONArray array = null;
                String Fisheryplanid;
                String Waterbodyid;
                String Fisheryplandate;
                String Selectedfishery;
                String Totalamount;
                String Createdby;
                String Isexported ="true";
                String Individualcost;
                String Budgetcode;
                String Pgcode;
                String Grpmemcode;
                String Grpcode;
                String Areadependent;
                String status;

                try{
                    JSONObject jsonObj = new JSONObject(result);
                    array = jsonObj.getJSONArray("Master");

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            Fisheryplanid = object.optString("Fisheryplanid");
                            Waterbodyid = object.optString("Waterbodyid");
                            Fisheryplandate = object.optString("Fisheryplandate");
                            Selectedfishery = object.optString("Selectedfishery");
                            Totalamount = object.optString("Totalamount");
                            Createdby = object.optString("Createdby");
                            Individualcost = object.optString("Individualcost");
                            Budgetcode = object.optString("Budgetcode");
                            Pgcode = object.optString("Pgcode");
                            Grpmemcode = object.optString("Grpmemcode");
                            Grpcode = object.optString("Grpcode");
                            Areadependent = object.optString("Areadependent");
                            status=object.optString("status");

                            FisheryPlantbl data = new FisheryPlantbl(Fisheryplanid,Waterbodyid,Fisheryplandate,Selectedfishery,
                                    Totalamount,Createdby,Isexported,Individualcost,Budgetcode,
                                    Pgcode,Grpmemcode,Grpcode,Areadependent,"","","","",status);
                            data.save();
                        }

                        Toast.makeText(context,"Fishery plan data Downloaded",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context,"No Data of fishery plan Found",Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception e){
                    Toast.makeText(context,"Entered in catch after data download",Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String resultt ="";
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJOHAR);

            request.addProperty("whr", whr);
            request.addProperty("flag", flag);
            request.addProperty("whr1", whr1);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                SoapPrimitive resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){

            }
            return resultt;
        }
    }



}
