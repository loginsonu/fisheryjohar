package com.jslpsmis.fishery.commonclass;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.jslpsmis.fishery.DashboardActivity;
import com.jslpsmis.fishery.dataprovider.Dataprovider;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class UploadDataToServerTable {

    private String methodName;
    private Context mContext;
    private String jsonDataString;
    private String tableIndentifier;
    private ProgressDialog dialog;
    private Dataprovider dataprovider;

    public UploadDataToServerTable(Context context, String methodName, String jsonDataString, String tableIndentifier){
        this.methodName = methodName;
        this.mContext = context;
        this.jsonDataString = jsonDataString;
        this.tableIndentifier = tableIndentifier;
        dataprovider = new Dataprovider(context);
        new UploadClass().execute();
    }

    public class UploadClass extends AsyncTask<String,Void,String> {
        String SOAP_ACTION = "http://tempuri.org/"+methodName;
        String METHOD_NAME = methodName;
        String NAMESPACE = "http://tempuri.org/";
        String URL = "http://johar.swalekha.in/Webservices/WebService.asmx";
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(dialog==null){
                dialog = new ProgressDialog(mContext);
                dialog.setMessage("कृपया प्रतीक्षा करें, डेटा अपलोड होरा है");
                dialog.setCancelable(false);
                dialog.show();
            }

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialog!=null){
                dialog.dismiss();
            }

            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Table");
                if(array.length()>0){
                    JSONObject object = array.getJSONObject(0);

                    int res = object.optInt("RetValue");
                    if(res == 1){
                        updateTable(tableIndentifier);

                    }
                    else{
                        //alert("असफल हुआ","क्रॉप अपलोड फेल्ड");

                    }


                }

            }catch (Exception e){

            }
        }

        private void updateTable(String tableIndentifier) {
            if(tableIndentifier.equals("tblAddWaterBody")){
                if(DashboardActivity.listAddwater.size()>0){
//                    dataprovider.updateAddWaterBody(DashboardActivity.pgCode,DashboardActivity.grpMemberCode,DashboardActivity.grpCode);
                    Constant.uploadWaterBodyInProgress = false;
                    Toast.makeText(mContext,"Add water body data successfully uploaded",Toast.LENGTH_LONG).show();
  //                  DashboardActivity.btnUpload.setVisibility(View.GONE);

                }
            }

            if(tableIndentifier.equals("tbltracFisheryPlan")){
                if(DashboardActivity.fisheryPlantblList.size()>0){
                    for(int i = 0; i< DashboardActivity.fisheryPlantblList.size(); i++){
                        DashboardActivity.fisheryPlantblList.get(i).setIsexported("true");
                        DashboardActivity.fisheryPlantblList.get(i).save();
                    }
                    Toast.makeText(mContext,"FIshery Plan data successfully uploaded",Toast.LENGTH_LONG).show();
                 //   DashboardActivity.btnUpload.setVisibility(View.GONE);
                }
            }


        }

        @Override
        protected String doInBackground(String... strings) {


            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            request.addProperty("sData", jsonDataString);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            //HttpTransportSE httpTransport = new HttpTransportSE(URL, 120000);
            HttpTransportSE httpTransport = new HttpTransportSE(URL, 520000);

            try{
                httpTransport.call(SOAP_ACTION, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){

            }
            return resultt;
        }
    }
}
