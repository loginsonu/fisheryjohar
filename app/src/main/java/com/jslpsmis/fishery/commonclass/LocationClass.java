package com.jslpsmis.fishery.commonclass;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

public class LocationClass implements OnLocationUpdatedListener {

    private LocationGooglePlayServicesProvider provider;
    Context context;
    public static String currentLocation ="";
    String getCurrentLocation ="";
    public static double lat = 0;
    public  static  double longi=0;

    public LocationClass(Context context){
        this.context = context;
        startLocation();
        showLast();

    }

    public String getLocation(){
        if(!currentLocation.equals("")){
            return  currentLocation;
        }
        else {
            return getCurrentLocation;
        }
    }

    public String getAccurateLocation(){
        return currentLocation;
    }


    @Override
    public void onLocationUpdated(Location location) {
        showLocation(location);
    }

    private void startLocation() {

        provider = new LocationGooglePlayServicesProvider();
        //provider.setCheckLocationSettings(true);
        turnGPS();
        SmartLocation smartLocation = new SmartLocation.Builder(context).logging(true).build();
        smartLocation.location(provider).start(this);

    }

    private void turnGPS() {
        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);

        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(context)
                .checkLocationSettings(settingsBuilder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response =
                            task.getResult(ApiException.class);
                } catch (ApiException ex) {
                    switch (ex.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvableApiException =
                                        (ResolvableApiException) ex;
                                resolvableApiException
                                        .startResolutionForResult((Activity) context,
                                                1);
                            } catch (IntentSender.SendIntentException e) {

                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }
            }
        });

    }

    public void stopLocation() {
        SmartLocation.with(context).location().stop();

    }

    private void showLast() {
        Location lastLocation = SmartLocation.with(context).location().getLastLocation();
        if (lastLocation != null) {
            lat = lastLocation.getLatitude();
            longi = lastLocation.getLongitude();
            getAddress(lat,longi);
        }


    }

    private void showLocation(Location location) {
        if (location != null) {

             lat = location.getLatitude();
             longi = location.getLongitude();

            getAddress(lat,longi);

            // We are going to get the address for the current position
            SmartLocation.with(context).geocoding().reverse(location, new OnReverseGeocodingListener() {
                @Override
                public void onAddressResolved(Location original, List<Address> results) {
                    if (results.size() > 0) {
                        Address result = results.get(0);
                        StringBuilder builder = new StringBuilder();
                        List<String> addressElements = new ArrayList<>();
                        for (int i = 0; i <= result.getMaxAddressLineIndex(); i++) {
                            addressElements.add(result.getAddressLine(i));
                        }
                        builder.append(TextUtils.join(", ", addressElements));
                        currentLocation = builder.toString();
                    }
                }
            });
        } else {

        }
    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 2);
            Address obj = addresses.get(1);
            String add = obj.getAddressLine(0);
            getCurrentLocation = add;
           // Toast.makeText(context,add, Toast.LENGTH_SHORT).show();

//            placeName = add;
//            Log.v("IGA", "Address" + add);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(context, e.getMessage() + ", please turn on network", Toast.LENGTH_SHORT).show();
        }
    }

}
