package com.jslpsmis.fishery.commonclass;

import com.jslpsmis.fishery.model.BudgetModel;

import java.util.ArrayList;
import java.util.List;

public class BudgetCodeClass {
    private static final String[] budgetCode = new String[] {"JO.4.5.1","JO.4.5.2","JO.4.7.2","JO.4.8.2","JO.4.1.1","JO.4.7.1","JO.4.8.1"};

    private static final String[] benPer = new String[] {"10","10","0","35","10","10","35"};

    private static final String[] benConvg = new String[] {"30","30","40","0","0","0","0"};

    private static final String[] joharBank= new String[] {"60","60","60","65","90","90","65"};

    public static List<BudgetModel> getListData(){
        List<BudgetModel> list = new ArrayList<>();
        for (int i = 0; i < budgetCode.length; i++)
        {

            BudgetModel dashboardFragmentModel = new BudgetModel(budgetCode[i],benPer[i],benConvg[i],joharBank[i]);
            // Binds all strings into an array
            list.add(dashboardFragmentModel);

        }
        return list;
    }
}
