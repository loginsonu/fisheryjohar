package com.jslpsmis.fishery.dataprovider;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jslpsmis.fishery.DatabaseHelper;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.model.AddWaterBodySpinnerModel;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.jslpsmis.fishery.model.FisheryPlanModel;
import com.jslpsmis.fishery.model.InputIndentModel;
import com.jslpsmis.fishery.model.ProducerActivityModel;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;


public class Dataprovider {

    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "Fishery";

    public SQLiteDatabase dbcpa_db;
    private Cursor cursor;
    private Context context;
    private DatabaseHelper dbHelper;
    MDMSharedPreference mdmSharedPreference;

    public Dataprovider(Context _context) {
        try {
            this.context = _context;
            DatabaseHelper dbHelper;
            dbHelper = DatabaseHelper.getInstance(context, DATABASE_NAME);
            dbcpa_db = dbHelper.getDatabase();
            cursor = null;
            mdmSharedPreference = MDMSharedPreference.getInstance(_context);
        } catch (Exception exp) {
            System.out.println(exp);
        }
    }

    public int getMaxRecord(String Sql) {
        int iIntegerValue = 0;
        try {
            cursor = null;
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(Sql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    iIntegerValue = cursor.getInt(0);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getMaxRecord :: " + exception.getMessage());
        }
        return iIntegerValue;
    }

    public void executeSql(String Sql) {
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            dbcpa_db.execSQL(Sql);
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in executeSql :: " + exception.getMessage());
        }
    }

    public String getGUID(String Sql) {
        String Guid = null;
        try {
            cursor = null;
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(Sql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    Guid = cursor.getString(0);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getMaxRecord :: " + exception.getMessage());
        }
        return Guid;
    }


    public String getRecord(String Sql) {
        String iValue = "";
        try {
            cursor = null;
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(Sql, null);
            if (cursor != null) {

                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    iValue = cursor.getString(0);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getMaxRecord :: " + exception.getMessage());
        }
        return iValue;
    }

     /********************Inserting in Login Table***********************/
    public boolean saveLoginData(String contactNumber, String password, String clusterCode, String districtCode, String blockCode, String cadreId
            , String cadreName, String cadreType, String userId, String action){
        String sql = "";
        if(action.equals("insert")) {
             sql = "Insert into logintb(contactno,password,clustercode,districtcode,blockcode,cadreid,cadrename,cadretype,userid,CreatedBy,CreatedOn,GUID)values('"
                    + contactNumber
                    + "','"
                    + password
                    + "','"
                    + clusterCode
                    + "','"
                    + districtCode
                    + "','"
                    + blockCode
                    + "','"
                    + cadreId
                    + "','"
                    + cadreName
                    + "','"
                    + cadreType
                    + "','"
                    + userId
                     + "','"
                     + userName()
                     + "','"
                     + date()
                     + "','"
                     + random()
                    + "')";
        }
        else{
            sql = "Delete from logintb where cadreid ='"+cadreId+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in logintb :: " + e.getMessage());
            return false;
        }
    }

    /********************Inserting in Producer Group Table***********************/
    public boolean saveProducerGroupData(String pgCode, String pgName, String clusterCode, String districtCode
            , String blockCode, String cadreId, String action){
        String sql = "";
        if(action.equals("insert")) {
             sql = "Insert into producergrptb(pgcode,pgname,clustercode,districtcode,blockcode,cadreid,CreatedBy,CreatedOn,GUID)values('"
                    + pgCode
                    + "','"
                    + pgName
                    + "','"
                    + clusterCode
                    + "','"
                    + districtCode
                    + "','"
                    + blockCode
                     + "','"
                     + cadreId
                     + "','"
                     + userName()
                     + "','"
                     + date()
                     + "','"
                     + random()
                    + "')";
        }else{
            sql = "Delete from producergrptb where cadreid ='"+cadreId+"'";
            //sql = "Update producergrptb set pgcode='"+pgCode+"', pgname ='"+pgName+"',clustercode ='"+clusterCode+"' where clustercode='"+clusterCode+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrptbl :: " + e.getMessage());
            return false;
        }
    }

   /****************Getting all data from producer group table******************/
    public ArrayList<ProducerActivityModel> getProducerList(String cadreId) {
        ArrayList<ProducerActivityModel> producerGrpList =  new ArrayList<ProducerActivityModel>();
        String sql = "";
        sql = "Select * from producergrptb where cadreid ='" + cadreId + "'";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
           int i = cursor.getCount();

            if (cursor != null) {

                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {

                     ProducerActivityModel model = new ProducerActivityModel();
                     model.setPgCode(cursor.getString(cursor.getColumnIndex(("pgcode"))));
                     model.setPgName(cursor.getString(cursor.getColumnIndex(("pgname"))));
                     model.setClusterCode(cursor.getString(cursor.getColumnIndex(("clustercode"))));
                     model.setDistrictCode(cursor.getString(cursor.getColumnIndex(("districtcode"))));
                     model.setBlockCode(cursor.getString(cursor.getColumnIndex(("blockcode"))));
                     model.setCadreId(cursor.getString(cursor.getColumnIndex(("cadreid"))));

                     producerGrpList.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return producerGrpList;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return producerGrpList;
    }


    /********************Inserting in Producer Group Member Table***********************/
    public boolean saveProducerGroupMemberData(String memberName, String husbandName, String grpName, String grpCode, String grpMemberCode, String pgCode
            , String districtCode, String blockCode, String clusterCode, String cadreId, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into producergrpmemtb(membername,husbandname,grpname,grpcode,grpmembercode,pgcode,districtcode,blockcode,clustercode,cadreid,CreatedBy,CreatedOn,GUID)values('"
                    + memberName
                    + "','"
                    + husbandName
                    + "','"
                    + grpName
                    + "','"
                    + grpCode
                    + "','"
                    + grpMemberCode
                    + "','"
                    + pgCode
                    + "','"
                    + districtCode
                    + "','"
                    + blockCode
                    + "','"
                    + clusterCode
                    + "','"
                    + cadreId
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else{
            sql = "Delete from producergrpmemtb where cadreid ='"+cadreId+"'";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    /****************Getting all data from producer group table******************/
    public ArrayList<ProducerGroupMemberActivityModel> getProducerGrpMemList(String pgCode) {

        ArrayList<ProducerGroupMemberActivityModel> producerGrpMemeberList =  new ArrayList<ProducerGroupMemberActivityModel>();
        String sql = "";
        sql = "Select * from producergrpmemtb where pgcode ='" + pgCode + "'";

        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {

                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {
                    ProducerGroupMemberActivityModel model = new ProducerGroupMemberActivityModel();
                    model.setMemberName(cursor.getString(cursor.getColumnIndex(("membername"))));
                    model.setHusbandName(cursor.getString(cursor.getColumnIndex(("husbandname"))));
                    model.setGroupName(cursor.getString(cursor.getColumnIndex(("grpname"))));
                    model.setGroupCode(cursor.getString(cursor.getColumnIndex(("grpcode"))));
                    model.setGroupMemberCode(cursor.getString(cursor.getColumnIndex(("grpmembercode"))));
                    model.setPgCode(cursor.getString(cursor.getColumnIndex(("pgcode"))));
                    model.setDistrictCode(cursor.getString(cursor.getColumnIndex(("districtcode"))));
                    model.setBlockCode(cursor.getString(cursor.getColumnIndex(("blockcode"))));
                    model.setClusterCode(cursor.getString(cursor.getColumnIndex(("clustercode"))));
                    model.setCadreId(cursor.getString(cursor.getColumnIndex(("cadreid"))));

                    producerGrpMemeberList.add(model);

                    cursor.moveToNext();
                }
                cursor.close();
            }
            return producerGrpMemeberList;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return producerGrpMemeberList;
    }

    public boolean saveWaterBodyType(String name, String code, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblWaterBodyType(Name,Code,CreatedBy,CreatedOn,GUID)values('"
                    + name
                    + "','"
                    + code
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else{
            sql = "Delete from tblWaterBodyType";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }

        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public boolean saveWaterBodyOwner(String name, String code, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblWaterBodyOwner(Name,Code,CreatedBy,CreatedOn,GUID)values('"
                    + name
                    + "','"
                    + code
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else{
            sql = "Delete from tblWaterBodyOwner";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }

        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public boolean saveFisheryType(String name, String code, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblSelectedFishery(Name,Code,CreatedBy,CreatedOn,GUID)values('"
                    + name
                    + "','"
                    + code
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                     + "')";
        }else{
            sql = "Delete from tblSelectedFishery";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }


    public ArrayList<AddWaterBodySpinnerModel> getwaterBodyTypeList() {
        ArrayList<AddWaterBodySpinnerModel> list =  new ArrayList<AddWaterBodySpinnerModel>();
        String sql = "";
        sql = "Select * from tblWaterBodyType";

        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    AddWaterBodySpinnerModel model = new AddWaterBodySpinnerModel();
                    model.setName(cursor.getString(cursor.getColumnIndex(("Name"))));
                    model.setCode(cursor.getString(cursor.getColumnIndex(("Code"))));

                    list.add(model);

                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return list;
    }

    public ArrayList<AddWaterBodySpinnerModel> getwaterBodyOwnerList() {

        ArrayList<AddWaterBodySpinnerModel> list =  new ArrayList<AddWaterBodySpinnerModel>();
        String sql = "";
        sql = "Select * from tblWaterBodyOwner ";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {

                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {

                    AddWaterBodySpinnerModel model = new AddWaterBodySpinnerModel();
                    model.setName(cursor.getString(cursor.getColumnIndex(("Name"))));
                    model.setCode(cursor.getString(cursor.getColumnIndex(("Code"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return list;
    }

    public ArrayList<AddWaterBodySpinnerModel> getSelectedFisheryList() {

        ArrayList<AddWaterBodySpinnerModel> list =  new ArrayList<AddWaterBodySpinnerModel>();
        String sql = "";
        sql = "Select * from tblSelectedFishery ";

        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {

                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {

                    AddWaterBodySpinnerModel model = new AddWaterBodySpinnerModel();
                    model.setName(cursor.getString(cursor.getColumnIndex(("Name"))));
                    model.setCode(cursor.getString(cursor.getColumnIndex(("Code"))));

                    list.add(model);

                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return list;
    }

    public boolean AddWaterBody(String waterBodyId,String pgCode, String grpMemCode,String grpCode,String waterBodyCode,String waterBodyOwnerCode,
                                String waterAvailableMoth,String selectedFishery,String isfisheryHappening,
                                String istrainingtaken,String fromwheretraining,String expectedArea, String totalProduction,
                                String totalIncome, String imagePath,String Lat,String Long,String Location,String guid, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblAddWaterBody(WaterBodyId,PgCode,GrpMemCode,GrpCode,WaterBodyCode,WaterBodyOwnerCode,NoOfMonthWaterAvailable,SelectedFishery,IsFisheryHappening,IsTrainingTaken,FromWhereTraining,ExpectedAreaInDecimal,TotalProductionInQuintal,TotalIncome,WaterBodyPicPath,CreatedBy,CreatedOn,GUID,IsDeleted,Lat,Long,Location)values('"
                    + waterBodyId
                    + "','"
                    + pgCode
                    + "','"
                    + grpMemCode
                    + "','"
                    + grpCode
                    + "','"
                    + waterBodyCode
                    + "','"
                    + waterBodyOwnerCode
                    + "','"
                    + waterAvailableMoth
                    + "','"
                    + selectedFishery
                    + "','"
                    + isfisheryHappening
                    + "','"
                    + istrainingtaken
                    + "','"
                    + fromwheretraining
                    + "','"
                    + expectedArea
                    + "','"
                    + totalProduction
                    + "','"
                    + totalIncome
                    + "','"
                    + imagePath
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "','"
                    + "0"
                    + "','"
                    + Lat
                    + "','"
                    + Long
                    + "','"
                    + Location
                    + "')";
        }else if(action.equals("delete")){
            sql = "Delete from tblAddWaterBody where PgCode ='"+pgCode+"' and WaterBodyId ='"+waterBodyId+"'";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }else{
            sql = "Update tblAddWaterBody set WaterBodyCode='"+waterBodyCode+"', WaterBodyOwnerCode ='"+waterBodyOwnerCode+"',NoOfMonthWaterAvailable ='"+waterAvailableMoth+"',SelectedFishery ='"+selectedFishery+"',IsFisheryHappening ='"+isfisheryHappening+"',IsTrainingTaken ='"+istrainingtaken+"',FromWhereTraining ='"+fromwheretraining+"',ExpectedAreaInDecimal ='"+expectedArea+"',TotalProductionInQuintal ='"+totalProduction+"',TotalIncome ='"+totalIncome+"',WaterBodyPicPath ='"+imagePath+"',GrpMemCode ='"+grpMemCode+"',GrpCode ='"+grpCode+"'where GUID='"+guid+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }


    public boolean AddWaterBodyMaster(String waterBodyId,String pgCode, String grpMemCode,String grpCode,String waterBodyCode,String waterBodyOwnerCode,
                                String waterAvailableMoth,String selectedFishery,String isfisheryHappening,
                                String istrainingtaken,String fromwheretraining,String expectedArea, String totalProduction,
                                String totalIncome, String imagePath,String Lat,String Long,String Location,String guid, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblAddWaterBody(IsExported,WaterBodyId,PgCode,GrpMemCode,GrpCode,WaterBodyCode,WaterBodyOwnerCode,NoOfMonthWaterAvailable,SelectedFishery,IsFisheryHappening,IsTrainingTaken,FromWhereTraining,ExpectedAreaInDecimal,TotalProductionInQuintal,TotalIncome,WaterBodyPicPath,CreatedBy,CreatedOn,GUID,IsDeleted,Lat,Long,Location)values('"
                    + "true"
                    + "','"
                    + waterBodyId
                    + "','"
                    + pgCode
                    + "','"
                    + grpMemCode
                    + "','"
                    + grpCode
                    + "','"
                    + waterBodyCode
                    + "','"
                    + waterBodyOwnerCode
                    + "','"
                    + waterAvailableMoth
                    + "','"
                    + selectedFishery
                    + "','"
                    + isfisheryHappening
                    + "','"
                    + istrainingtaken
                    + "','"
                    + fromwheretraining
                    + "','"
                    + expectedArea
                    + "','"
                    + totalProduction
                    + "','"
                    + totalIncome
                    + "','"
                    + imagePath
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "','"
                    + "0"
                    + "','"
                    + Lat
                    + "','"
                    + Long
                    + "','"
                    + Location
                    + "')";
        }else if(action.equals("delete")){
            sql = "Delete from tblAddWaterBody where PgCode ='"+pgCode+"' and GrpMemCode ='"+grpMemCode+"'and GrpCode ='"+grpCode+"'and GUID ='"+guid+"'";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }else{
            sql = "Update tblAddWaterBody set WaterBodyCode='"+waterBodyCode+"', WaterBodyOwnerCode ='"+waterBodyOwnerCode+"',NoOfMonthWaterAvailable ='"+waterAvailableMoth+"',SelectedFishery ='"+selectedFishery+"',IsFisheryHappening ='"+isfisheryHappening+"',IsTrainingTaken ='"+istrainingtaken+"',FromWhereTraining ='"+fromwheretraining+"',ExpectedAreaInDecimal ='"+expectedArea+"',TotalProductionInQuintal ='"+totalProduction+"',TotalIncome ='"+totalIncome+"',WaterBodyPicPath ='"+imagePath+"'  where GUID='"+guid+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public ArrayList<AddWaterModel> getAddWaterList(String pgCode) {
        ArrayList<AddWaterModel> list =  new ArrayList<AddWaterModel>();
        String sql = "";
        /*"select * from tblAddWaterBody where PgCode ='"+pgCode+"'";*/
        sql = "SELECT * FROM tblAddWaterBody WHERE PgCode ='"+pgCode+"' GROUP BY WaterBodyId";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    AddWaterModel model = new AddWaterModel();
                    model.setPgCode(cursor.getString(cursor.getColumnIndex(("PgCode"))));
                    model.setGrpCode(cursor.getString(cursor.getColumnIndex(("GrpCode"))));
                    model.setGrpMemberCode(cursor.getString(cursor.getColumnIndex(("GrpMemCode"))));
                    model.setWaterBodyCode(cursor.getString(cursor.getColumnIndex(("WaterBodyCode"))));
                    model.setWaterBodyOwnerCode(cursor.getString(cursor.getColumnIndex(("WaterBodyOwnerCode"))));
                    model.setNoOfMonthAvailable(cursor.getString(cursor.getColumnIndex(("NoOfMonthWaterAvailable"))));
                    model.setSelectedFishery(cursor.getString(cursor.getColumnIndex(("SelectedFishery"))));
                    model.setIsFisheryHappenig(cursor.getString(cursor.getColumnIndex(("IsFisheryHappening"))));
                    model.setIsTrainingTaken(cursor.getString(cursor.getColumnIndex(("IsTrainingTaken"))));
                    model.setFromwheretraing(cursor.getString(cursor.getColumnIndex(("FromWhereTraining"))));
                    model.setTotalArea(cursor.getString(cursor.getColumnIndex(("ExpectedAreaInDecimal"))));
                    model.setTotalProduction(cursor.getString(cursor.getColumnIndex(("TotalProductionInQuintal"))));
                    model.setTotalIncome(cursor.getString(cursor.getColumnIndex(("TotalIncome"))));
                    model.setWaterBodyPicPath(cursor.getString(cursor.getColumnIndex(("WaterBodyPicPath"))));
                    model.setGuid(cursor.getString(cursor.getColumnIndex(("GUID"))));
                    model.setIsExported(cursor.getString(cursor.getColumnIndex(("IsExported"))));
                    model.setIsDeleted(cursor.getString(cursor.getColumnIndex(("IsDeleted"))));
                    model.setLat(cursor.getString(cursor.getColumnIndex(("Lat"))));
                    model.setLongg(cursor.getString(cursor.getColumnIndex(("Long"))));
                    model.setLocation(cursor.getString(cursor.getColumnIndex(("Location"))));
                    model.setWaterBodyId(cursor.getString(cursor.getColumnIndex(("WaterBodyId"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;
        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: "
                    + exception.getMessage());
        }
        return list;
    }

    public ArrayList<AddWaterModel> getAddWaterListAll(String pgCode) {

        ArrayList<AddWaterModel> list =  new ArrayList<AddWaterModel>();
        String sql = "";

        sql = "Select * from tblAddWaterBody where PgCode ='"+pgCode+"' and IsExported IS NULL";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    AddWaterModel model = new AddWaterModel();
                    model.setPgCode(cursor.getString(cursor.getColumnIndex(("PgCode"))));
                    model.setGrpCode(cursor.getString(cursor.getColumnIndex(("GrpCode"))));
                    model.setGrpMemberCode(cursor.getString(cursor.getColumnIndex(("GrpMemCode"))));

                    model.setWaterBodyCode(cursor.getString(cursor.getColumnIndex(("WaterBodyCode"))));
                    model.setWaterBodyOwnerCode(cursor.getString(cursor.getColumnIndex(("WaterBodyOwnerCode"))));
                    model.setNoOfMonthAvailable(cursor.getString(cursor.getColumnIndex(("NoOfMonthWaterAvailable"))));
                    model.setSelectedFishery(cursor.getString(cursor.getColumnIndex(("SelectedFishery"))));
                    model.setIsFisheryHappenig(cursor.getString(cursor.getColumnIndex(("IsFisheryHappening"))));
                    model.setIsTrainingTaken(cursor.getString(cursor.getColumnIndex(("IsTrainingTaken"))));
                    model.setFromwheretraing(cursor.getString(cursor.getColumnIndex(("FromWhereTraining"))));
                    model.setTotalArea(cursor.getString(cursor.getColumnIndex(("ExpectedAreaInDecimal"))));
                    model.setTotalProduction(cursor.getString(cursor.getColumnIndex(("TotalProductionInQuintal"))));
                    model.setTotalIncome(cursor.getString(cursor.getColumnIndex(("TotalIncome"))));
                    model.setWaterBodyPicPath(cursor.getString(cursor.getColumnIndex(("WaterBodyPicPath"))));
                    model.setGuid(cursor.getString(cursor.getColumnIndex(("GUID"))));
                    model.setIsExported(cursor.getString(cursor.getColumnIndex(("IsExported"))));
                    model.setIsDeleted(cursor.getString(cursor.getColumnIndex(("IsDeleted"))));
                    model.setLat(cursor.getString(cursor.getColumnIndex(("Lat"))));
                    model.setLongg(cursor.getString(cursor.getColumnIndex(("Long"))));
                    model.setLocation(cursor.getString(cursor.getColumnIndex(("Location"))));
                    model.setWaterBodyId(cursor.getString(cursor.getColumnIndex(("WaterBodyId"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;
        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: " + exception.getMessage());
        }
        return list;
    }

    public ArrayList<AddWaterModel> getAddWaterListAll2(String pgCode, String WaterBodyIds) {
        ArrayList<AddWaterModel> list =  new ArrayList<AddWaterModel>();
        String sql = "";
        //"' and WaterBodyId ='"+waterBodyId+"'";
        sql = "Select * from tblAddWaterBody where PgCode ='"+pgCode+"' and IsExported IS NULL and WaterBodyId ='"+WaterBodyIds+"'";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    AddWaterModel model = new AddWaterModel();
                    model.setPgCode(cursor.getString(cursor.getColumnIndex(("PgCode"))));
                    model.setGrpCode(cursor.getString(cursor.getColumnIndex(("GrpCode"))));
                    model.setGrpMemberCode(cursor.getString(cursor.getColumnIndex(("GrpMemCode"))));

                    model.setWaterBodyCode(cursor.getString(cursor.getColumnIndex(("WaterBodyCode"))));
                    model.setWaterBodyOwnerCode(cursor.getString(cursor.getColumnIndex(("WaterBodyOwnerCode"))));
                    model.setNoOfMonthAvailable(cursor.getString(cursor.getColumnIndex(("NoOfMonthWaterAvailable"))));
                    model.setSelectedFishery(cursor.getString(cursor.getColumnIndex(("SelectedFishery"))));
                    model.setIsFisheryHappenig(cursor.getString(cursor.getColumnIndex(("IsFisheryHappening"))));
                    model.setIsTrainingTaken(cursor.getString(cursor.getColumnIndex(("IsTrainingTaken"))));
                    model.setFromwheretraing(cursor.getString(cursor.getColumnIndex(("FromWhereTraining"))));
                    model.setTotalArea(cursor.getString(cursor.getColumnIndex(("ExpectedAreaInDecimal"))));
                    model.setTotalProduction(cursor.getString(cursor.getColumnIndex(("TotalProductionInQuintal"))));
                    model.setTotalIncome(cursor.getString(cursor.getColumnIndex(("TotalIncome"))));
                    model.setWaterBodyPicPath(cursor.getString(cursor.getColumnIndex(("WaterBodyPicPath"))));
                    model.setGuid(cursor.getString(cursor.getColumnIndex(("GUID"))));
                    model.setIsExported(cursor.getString(cursor.getColumnIndex(("IsExported"))));
                    model.setIsDeleted(cursor.getString(cursor.getColumnIndex(("IsDeleted"))));
                    model.setLat(cursor.getString(cursor.getColumnIndex(("Lat"))));
                    model.setLongg(cursor.getString(cursor.getColumnIndex(("Long"))));
                    model.setLocation(cursor.getString(cursor.getColumnIndex(("Location"))));
                    model.setWaterBodyId(cursor.getString(cursor.getColumnIndex(("WaterBodyId"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;
        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: " + exception.getMessage());
        }
        return list;
    }

    public ArrayList<AddWaterModel> ShowMemberslist(String pgCode, String WaterBodyIds) {
        ArrayList<AddWaterModel> list =  new ArrayList<AddWaterModel>();
        String sql = "";
        //"' and WaterBodyId ='"+waterBodyId+"'";
        sql = "Select * from tblAddWaterBody where PgCode ='"+pgCode+"' and WaterBodyId ='"+WaterBodyIds+"'";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    AddWaterModel model = new AddWaterModel();
                    model.setPgCode(cursor.getString(cursor.getColumnIndex(("PgCode"))));
                    model.setGrpCode(cursor.getString(cursor.getColumnIndex(("GrpCode"))));
                    model.setGrpMemberCode(cursor.getString(cursor.getColumnIndex(("GrpMemCode"))));

                    model.setWaterBodyCode(cursor.getString(cursor.getColumnIndex(("WaterBodyCode"))));
                    model.setWaterBodyOwnerCode(cursor.getString(cursor.getColumnIndex(("WaterBodyOwnerCode"))));
                    model.setNoOfMonthAvailable(cursor.getString(cursor.getColumnIndex(("NoOfMonthWaterAvailable"))));
                    model.setSelectedFishery(cursor.getString(cursor.getColumnIndex(("SelectedFishery"))));
                    model.setIsFisheryHappenig(cursor.getString(cursor.getColumnIndex(("IsFisheryHappening"))));
                    model.setIsTrainingTaken(cursor.getString(cursor.getColumnIndex(("IsTrainingTaken"))));
                    model.setFromwheretraing(cursor.getString(cursor.getColumnIndex(("FromWhereTraining"))));
                    model.setTotalArea(cursor.getString(cursor.getColumnIndex(("ExpectedAreaInDecimal"))));
                    model.setTotalProduction(cursor.getString(cursor.getColumnIndex(("TotalProductionInQuintal"))));
                    model.setTotalIncome(cursor.getString(cursor.getColumnIndex(("TotalIncome"))));
                    model.setWaterBodyPicPath(cursor.getString(cursor.getColumnIndex(("WaterBodyPicPath"))));
                    model.setGuid(cursor.getString(cursor.getColumnIndex(("GUID"))));
                    model.setIsExported(cursor.getString(cursor.getColumnIndex(("IsExported"))));
                    model.setIsDeleted(cursor.getString(cursor.getColumnIndex(("IsDeleted"))));
                    model.setLat(cursor.getString(cursor.getColumnIndex(("Lat"))));
                    model.setLongg(cursor.getString(cursor.getColumnIndex(("Long"))));
                    model.setLocation(cursor.getString(cursor.getColumnIndex(("Location"))));
                    model.setWaterBodyId(cursor.getString(cursor.getColumnIndex(("WaterBodyId"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;
        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from producer group table :: " + exception.getMessage());
        }
        return list;
    }


    public boolean tblRateFishBreedingPerUnitSave(String itemCOde, String measurementUnit,String ratePerUnit,String quantityPerUnit,String code, String action,String areaDependent){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblRateFishBreedingPerUnit(AreaDependent,ItemCode,MeasurementUnit,RatePerUnit,QuantityPerUnit,Code,CreatedBy,CreatedOn,GUID)values('"
                    +areaDependent
                    + "','"
                    + itemCOde
                    + "','"
                    + measurementUnit
                    + "','"
                    + ratePerUnit
                    + "','"
                    + quantityPerUnit
                    + "','"
                    + code
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else{
            sql = "Delete from tblRateFishBreedingPerUnit";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }

        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public boolean tblItemRequiredFishBreedingSave(String itemName, String itemCOde,String budgetcode, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblItemRequiredFishBreeding(BudgetCode,ItemName,ItemCode,CreatedBy,CreatedOn,GUID)values('"
                    + budgetcode
                    + "','"
                    + itemName
                    + "','"
                    + itemCOde
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else{
            sql = "Delete from tblItemRequiredFishBreeding";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }
        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public ArrayList<InputIndentModel> getInputIndentList(String selectedFisheyCode) {
        ArrayList<InputIndentModel> inputTypeList =  new ArrayList<InputIndentModel>();
        String sql = "";
        sql = "SELECT tblRateFishBreedingPerUnit.AreaDependent,tblRateFishBreedingPerUnit.itemCode,tblItemRequiredFishBreeding.BudgetCode,tblItemRequiredFishBreeding.itemName,tblRateFishBreedingPerUnit.MeasurementUnit,tblRateFishBreedingPerUnit.ratePerUnit,tblRateFishBreedingPerUnit.QuantityPerUnit ,tblRateFishBreedingPerUnit.Code FROM tblRateFishBreedingPerUnit INNER JOIN tblItemRequiredFishBreeding ON tblRateFishBreedingPerUnit.ItemCode = tblItemRequiredFishBreeding.itemCode where Code ='" + selectedFisheyCode + "'";

        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {

                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {

                    InputIndentModel model = new InputIndentModel();
                    //public String farm_name,farm_location,land_type,total_land,latitude,longititude;
                    model.setItemCode(cursor.getString(cursor.getColumnIndex(("ItemCode"))));
                    model.setItemName(cursor.getString(cursor.getColumnIndex(("ItemName"))));
                    model.setMeasurementUnit(cursor.getString(cursor.getColumnIndex(("MeasurementUnit"))));
                    model.setRatePerUnit(cursor.getString(cursor.getColumnIndex(("RatePerUnit"))));
                    model.setQuantityPerUnit(cursor.getString(cursor.getColumnIndex(("QuantityPerUnit"))));
                    model.setSelectedFisheryCode(cursor.getString(cursor.getColumnIndex(("Code"))));
                    model.setAreaDependent(cursor.getString(cursor.getColumnIndex(("AreaDependent"))));
                    model.setBudgetCode(cursor.getString(cursor.getColumnIndex(("BudgetCode"))));

                    inputTypeList.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return inputTypeList;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from land info table :: "
                    + exception.getMessage());
        }
        return inputTypeList;
    }


    public boolean updateAddWaterBody(String pgCode,String wbids){

        String sql = "";
        String isExported = "true";
        sql = "Update tblAddWaterBody set IsExported='"+isExported+"'  where PgCode='" + pgCode +"' and  WaterBodyId='" + wbids+"'";

        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception

            return false;
        }
    }

    public boolean saveFIsheryPlanData(String FisheyId, String WaterBodyId,String FisheryPlanDate,String SelectedFishery,String TotalAmount, String action){
        String sql = "";
        if(action.equals("insert")) {
            sql = "Insert into tblFisheryPlan(FisheyId,WaterBodyId,FisheryPlanDate,SelectedFishery,TotalAmount,CreatedBy,CreatedOn,GUID)values('"
                    + FisheyId
                    + "','"
                    + WaterBodyId
                    + "','"
                    + FisheryPlanDate
                    + "','"
                    + SelectedFishery
                    + "','"
                    + TotalAmount
                    + "','"
                    + userName()
                    + "','"
                    + date()
                    + "','"
                    + random()
                    + "')";
        }else if(action.equals("delete")){
            sql = "Delete from tblFisheryPlan where FisheyId='"+FisheyId+"' ";
            //sql = "Update producergrpmemtb set farmername='"+farmerName+"', husbandname ='"+husbandName+"',shg ='"+shg+"',grpcode ='"+grpCode+"',grpmembercode ='"+grpMemberCode+"',pgcode ='"+pgCode+"' where pgcode='"+pgCode+"'";
        }else{
            sql = "Update tblFisheryPlan set  TotalAmount='"+TotalAmount+"',SelectedFishery='"+SelectedFishery+"'  where FisheyId='" + FisheyId +"' ";

        }

        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            //Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    public ArrayList<FisheryPlanModel> getFisheryPlanList(String waterBodyId) {
        ArrayList<FisheryPlanModel> list =  new ArrayList<FisheryPlanModel>();
        String sql = "";
        sql = "SELECT * FROM tblFisheryPlan where WaterBodyId ='" + waterBodyId + "'";
        cursor = null;
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            cursor = dbcpa_db.rawQuery(sql, null);
            int i = cursor.getCount();

            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    FisheryPlanModel model = new FisheryPlanModel();
                    //public String farm_name,farm_location,land_type,total_land,latitude,longititude;
                    model.setFisheryId(cursor.getString(cursor.getColumnIndex(("FisheyId"))));
                    model.setWaterBodyId(cursor.getString(cursor.getColumnIndex(("WaterBodyId"))));
                    model.setDate(cursor.getString(cursor.getColumnIndex(("FisheryPlanDate"))));
                    model.setSelectedFishey(cursor.getString(cursor.getColumnIndex(("SelectedFishery"))));
                    model.setTotalAmount(cursor.getString(cursor.getColumnIndex(("TotalAmount"))));
                    model.setIsExported(cursor.getString(cursor.getColumnIndex(("IsExported"))));

                    list.add(model);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return list;

        } catch (Exception exception) {
            Log.e("DataProvider", "Error while fetching data from land info table :: "
                    + exception.getMessage());
        }
        return list;
    }

    public boolean deleteData(String tableName){
        String sql = "";
        if(tableName.equals("logintb")) {
            sql = "Delete from logintb";
        }

        if(tableName.equals("producergrpmemtb")) {
            sql = "Delete from producergrpmemtb";
        }

        if(tableName.equals("producergrptb")) {
            sql = "Delete from producergrptb";
        }

        if(tableName.equals("tblAddWaterBody")) {
            sql = "Delete from tblAddWaterBody";
        }

        if(tableName.equals("tblFisheryPlan")) {
            sql = "Delete from tblFisheryPlan";
        }

        if(tableName.equals("tblItemRequiredFishBreeding")) {
            sql = "Delete from tblItemRequiredFishBreeding";
        }

        if(tableName.equals("tblRateFishBreedingPerUnit")) {
            sql = "Delete from tblRateFishBreedingPerUnit";
        }

        if(tableName.equals("tblSelectedFishery")) {
            sql = "Delete from tblSelectedFishery";
        }

        if(tableName.equals("tblWaterBodyOwner")) {
            sql = "Delete from tblWaterBodyOwner";
        }

        if(tableName.equals("tblWaterBodyType")) {
            sql = "Delete from tblWaterBodyType";
        }


        try {
            executeSql(sql);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            // Log.e("DataProvider", "Error while instering in producergrpMembertbl :: " + e.getMessage());
            return false;
        }
    }

    private String date() {
        java.util.Calendar date = java.util.Calendar.getInstance();
        java.text.DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return  dateFormat.format(date.getTime());
    }

    private String userName(){
        String user = mdmSharedPreference.getString("userNamee");
        return  user;
    }

    public String random() {
        char[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                .toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 30; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        return sb.toString();
    }

}



