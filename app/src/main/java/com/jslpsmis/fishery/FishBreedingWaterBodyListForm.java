package com.jslpsmis.fishery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.adapter.InputIndentCalculationFishery;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.Cropplanningmemberstbl;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterBodySpinnerModel;
import com.jslpsmis.fishery.model.InputIndentModel;
import com.jslpsmis.fishery.model.PriceAndRateModel;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import butterknife.BindView;

public class FishBreedingWaterBodyListForm extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TextView pgName,shg,farmerName;
    List<AddWaterBodySpinnerModel> selectedFisheryList;
    List<String> selectedFisheryName = new ArrayList<>();
    List<String> selectedFisheryCode= new ArrayList<>();
    public ArrayAdapter<String> selectedFisheryAdapter;
    Dataprovider dataprovider;
    Spinner spinner4;
    String selectedFisheryNameString,selectedFisheryCodeString;
    ArrayList<InputIndentModel> indentModelArrayList;
    ArrayList<InputIndentModel> indentModelArrayList1;
    List<PriceAndRateModel> priceAndRateModelList;
    MDMSharedPreference mdmSharedPreference;

    InputIndentCalculationFishery aAdapter;
    RecyclerView recyclerView;
    float totalPrice;
    LinearLayout totalPriceContainer;
    TextView totalPriceText;
    Button save;
    String FisheryyId;
    List<FisheryPlantbl> fisheryPlantblList;
    String waterBodyId;
    String waterBodyArea;
    String Pgcode,PgName;
    ArrayList<String> CropPlanningMemberModel;
    ArrayList<String> grpMemCodeToBeAdded;
    ArrayList<String> grpCodeToBeAdded;
    @BindView(R.id.backButtonCropPlanForm1)
    ImageView backButtonCropPlanForm1;
    String grpMemCode,grpCode;
    ProgressDialog progress;
    TextView payment_date;
    ImageView call_calender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fish_breeding_water_body_list_form);

        Intent intent = getIntent();
        waterBodyId = intent.getStringExtra("waterBodyId");
        waterBodyArea = intent.getStringExtra("wbArea");
        Pgcode= intent.getStringExtra("pgcode");
        PgName= intent.getStringExtra("grpname");
        CropPlanningMemberModel = (ArrayList<String>) getIntent().getSerializableExtra("membersarray");
        grpMemCodeToBeAdded = (ArrayList<String>) getIntent().getSerializableExtra("grpMemCodeToBeAdded");
        grpCodeToBeAdded = (ArrayList<String>) getIntent().getSerializableExtra("grpCodeToBeAdded");

        init();
    }

    private void init() {
        mdmSharedPreference = MDMSharedPreference.getInstance(this);
        dataprovider = new Dataprovider(this);
        spinner4 = findViewById(R.id.spinner4);
        pgName = findViewById(R.id.textView56);
        shg = findViewById(R.id.textView57);
        farmerName = findViewById(R.id.headingCropPlanLandInfo);
        recyclerView = findViewById(R.id.inputIndentList);
        totalPriceContainer = findViewById(R.id.totalContId);
        totalPriceText = findViewById(R.id.totalCostIdd);
        save = findViewById(R.id.btnnSaveBaseLineLandInfoForm);
        payment_date=(TextView)findViewById(R.id.payment_date);
        call_calender=(ImageView)findViewById(R.id.call_calender);

        //farmerName.setText("मत्स्य पालन योजना"+"("+DashboardActivity.memberName+")");
        pgName.setText(PgName);
        shg.setText("अनुमनित छेत्रफल :"+waterBodyArea);

        selectedFisheryList = dataprovider.getSelectedFisheryList();
        if(selectedFisheryList.size()>0){
            selectedFisheryName.add("चयनित मत्स्यकी");
            selectedFisheryCode.add("");
            for(int i =0;i<selectedFisheryList.size();i++){
                String name = selectedFisheryList.get(i).getName();
                String code = selectedFisheryList.get(i).getCode();
                selectedFisheryName.add(name);
                selectedFisheryCode.add(code);
            }
            selectedFisheryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, selectedFisheryName);
            spinner4.setAdapter(selectedFisheryAdapter);
        }

        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFisheryNameString = parent.getItemAtPosition(position).toString();
                selectedFisheryCodeString = selectedFisheryCode.get(position);
                InputIndentFunc();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        call_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        FishBreedingWaterBodyListForm.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                dpd.show(FishBreedingWaterBodyListForm.this.getFragmentManager(), "Datepickerdialog");
            }
        });

        backButtonCropPlanForm1=(ImageView)findViewById(R.id.backButtonCropPlanForm1);
        backButtonCropPlanForm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FishBreedingWaterBodyListForm.this, InputIndentCalculationForWaterBody.class);
                intent.putExtra("pgCode",Pgcode);
                intent.putExtra("grpname",PgName);
                intent.putExtra("waterBodyId",waterBodyId);
                intent.putExtra("wbArea",waterBodyArea);
                startActivity(intent);
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(save.getText().equals("अपडेट")){
                    validation1();
                }else{
                    validation();
                }
            }
        });
    }

    private void validation() {
        if(totalPrice==0){
               alert(getString(R.string.errorr), "सेलेक्ट फिशरी");
        }else if(payment_date.getText().toString().equals("")){

            alert(getString(R.string.errorr), "पहले मत्स्य पालन तिथि चुनें(*)");

        }else if(payment_date.getText().toString().equals("पहले मत्स्य पालन तिथि चुनें(*)")){

            alert(getString(R.string.errorr), "पहले मत्स्य पालन तिथि चुनें(*)");

        } else{
            //DialogShow();
            String FisheryId= random()+Pgcode;
            for (int j = 0; j < CropPlanningMemberModel.size(); j++) {
                try {
                    grpMemCode = grpMemCodeToBeAdded.get(j);
                    grpCode = grpCodeToBeAdded.get(j);
                }catch (Exception e){
                    e.printStackTrace();
                }
            for(int i=0;i<indentModelArrayList1.size();i++){
                float area = Float.parseFloat(waterBodyArea);
                float quantity;
                if(indentModelArrayList1.get(i).getAreaDependent().equals("1")) {
                    quantity = Float.parseFloat(indentModelArrayList1.get(i).getQuantityPerUnit()) * area;
                }else{
                    quantity = Float.parseFloat(indentModelArrayList1.get(i).getQuantityPerUnit());
                }
                float price = Float.parseFloat(indentModelArrayList1.get(i).getRatePerUnit())*quantity;

                    FisheryPlantbl data = new FisheryPlantbl(FisheryId, waterBodyId, payment_date.getText().toString(), selectedFisheryCodeString, totalPrice + "", mdmSharedPreference.getString("cadre"), "false", price + "", indentModelArrayList1.get(i).getBudgetCode(), Pgcode, grpMemCode, grpCode, indentModelArrayList1.get(i).getAreaDependent(), date("y"), date("month"), indentModelArrayList1.get(i).getItemName(), indentModelArrayList1.get(i).getItemCode(), "0");
                    data.save();
                }
              }
                for (int j = 0; j < CropPlanningMemberModel.size(); j++) {
                    if(FisheryId.length()>0) {
                        Cropplanningmemberstbl cropplanningmemberstbl = new Cropplanningmemberstbl(UUID.randomUUID().toString(),
                                CropPlanningMemberModel.get(j), Pgcode, grpMemCodeToBeAdded.get(j), grpCodeToBeAdded.get(j), "0", waterBodyId, FisheryId, "", "");

                        cropplanningmemberstbl.save();
                    }
            }
           // DialogClose();
            Intent intent = new Intent(FishBreedingWaterBodyListForm.this, AddWaterbodynew.class);
            intent.putExtra("pgCode",Pgcode);
            intent.putExtra("pgName", AddWaterbodynew.pgName);
            intent.putExtra("grpname",PgName);
            startActivity(intent);
            finish();
        }
    }

    private void validation1() {
        if(totalPrice==0){
            alert(getString(R.string.errorr), "सेलेक्ट फिशरी");
        }else if(payment_date.getText().toString().equals("")){

            alert(getString(R.string.errorr), "पहले मत्स्य पालन तिथि चुनें(*)");

        }else if(payment_date.getText().toString().equals("पहले मत्स्य पालन तिथि चुनें(*)")){

            alert(getString(R.string.errorr), "पहले मत्स्य पालन तिथि चुनें(*)");

        } else{
            dataprovider.saveFIsheryPlanData(FisheryyId,waterBodyId,payment_date.getText().toString(),selectedFisheryCodeString,totalPrice+"",
                    "update");
            Intent intent = new Intent(FishBreedingWaterBodyListForm.this, AddWaterbodynew.class);
            intent.putExtra("pgCode",Pgcode);
            intent.putExtra("pgName", AddWaterbodynew.pgName);
            intent.putExtra("flag","old");
            intent.putExtra("grpname",PgName);
            startActivity(intent);
            finish();
        }
    }

    private String date() {
        Calendar date = Calendar.getInstance();
        java.text.DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return  dateFormat.format(date.getTime());
    }

    public String random() {
        char[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                .toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 30; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show(FishBreedingWaterBodyListForm.this);
    }

    private void InputIndentFunc() {
        indentModelArrayList = dataprovider.getInputIndentList(selectedFisheryCodeString);

        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("selectedfishery").eq(selectedFisheryCodeString))
                .list();

        indentModelArrayList1 = new ArrayList<>();
        if(fisheryPlantblList.size()>0){
            if(indentModelArrayList.size()>0){
                for(int i=0;i<indentModelArrayList.size();i++){
                   if(indentModelArrayList.get(i).getAreaDependent().equals("1")){
                        //itemCode,itemName,measurementUnit,RatePerUnit,QuantityPerUnit,selectedFisheryCode,areaDependent,budgetCode;
                       InputIndentModel model = new InputIndentModel();
                        model.setItemCode(indentModelArrayList.get(i).getItemCode());
                        model.setItemName(indentModelArrayList.get(i).getItemName());
                        model.setMeasurementUnit(indentModelArrayList.get(i).getMeasurementUnit());
                        model.setRatePerUnit(indentModelArrayList.get(i).getRatePerUnit());
                        model.setQuantityPerUnit(indentModelArrayList.get(i).getQuantityPerUnit());
                        model.setSelectedFisheryCode(indentModelArrayList.get(i).getSelectedFisheryCode());
                        model.setAreaDependent(indentModelArrayList.get(i).getAreaDependent());
                        model.setBudgetCode(indentModelArrayList.get(i).getBudgetCode());
                        indentModelArrayList1.add(model);
                    }
                }
            }
        }else{
            indentModelArrayList1=indentModelArrayList;
        }
        calculatePrice();
        aAdapter = new InputIndentCalculationFishery(this, priceAndRateModelList);
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(verticalLayoutmanager);
        recyclerView.setAdapter(aAdapter);
    }

    private void calculatePrice() {
        priceAndRateModelList = new ArrayList<>();
        totalPrice = 0;
        totalPriceContainer.setVisibility(View.GONE);
        if(indentModelArrayList1.size()>0){
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(2);
            formatter.setMaximumFractionDigits(2);
            for(int i=0;i<indentModelArrayList1.size();i++){
                PriceAndRateModel model = new PriceAndRateModel();
                model.setName(indentModelArrayList1.get(i).getItemName());
                //to set Quantity, from server quantity is according to 1 decimal
                float area = Float.parseFloat(waterBodyArea);
                float quantity;
                if(indentModelArrayList1.get(i).getAreaDependent().equals("1")) {
                     quantity = Float.parseFloat(indentModelArrayList1.get(i).getQuantityPerUnit()) * area;
                }else{
                    quantity = Float.parseFloat(indentModelArrayList1.get(i).getQuantityPerUnit());
                }
                String quantityRoundOff = formatter.format(quantity);
                model.setQuantity(quantityRoundOff+"");
                float price = Float.parseFloat(indentModelArrayList1.get(i).getRatePerUnit())*quantity;
                String priceRoundOff = formatter.format(price);
                model.setPrice(priceRoundOff+"");
                model.setMeasureMentUnit(indentModelArrayList1.get(i).getMeasurementUnit());
                priceAndRateModelList.add(model);
                totalPrice = totalPrice+price;
            }
                totalPriceContainer.setVisibility(View.INVISIBLE);
                totalPriceText.setText("कुल कीमत:"+totalPrice);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constant.isEdit.equals("true")){
            Constant.isEdit = "false";
            Intent intent = getIntent();
            String fisheryName = intent.getStringExtra("selectedFishery");
            FisheryyId = intent.getStringExtra("fisheryId");
            setSpinText(spinner4,fisheryName);
            save.setText("अपडेट");
        }
    }

    private String date(String month) {
        if (month.equals("M")) {
            Calendar date = Calendar.getInstance();
            java.text.DateFormat dateFormat = new SimpleDateFormat("MMMM", Locale.getDefault());
            return  dateFormat.format(date.getTime());
        }else if(month.equals("month")){
            Calendar date = Calendar.getInstance();
            java.text.DateFormat dateFormat = new SimpleDateFormat("M", Locale.getDefault());
            return  dateFormat.format(date.getTime());
        } else if(month.equals("day")){
            Calendar date = Calendar.getInstance();
            java.text.DateFormat dateFormat = new SimpleDateFormat("dd", Locale.getDefault());
            return  dateFormat.format(date.getTime());
        } else{
            Calendar date = Calendar.getInstance();
            java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
            return  dateFormat.format(date.getTime());
        }
    }

    public void setSpinText(Spinner spin, String text)
    {
        for(int i= 0; i < spin.getAdapter().getCount(); i++)
        {
            if(spin.getAdapter().getItem(i).toString().contains(text))
            {
                spin.setSelection(i);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(FishBreedingWaterBodyListForm.this, AddWaterbodynew.class);
        intent.putExtra("pgCode", AddWaterbodynew.pgCode);
        intent.putExtra("pgName", AddWaterbodynew.pgName);
        intent.putExtra("grpname", AddWaterbodynew.grpname);
        startActivity(intent);
        finish();
    }

    private void DialogShow() {
        progress= new ProgressDialog(FishBreedingWaterBodyListForm.this);
        progress.setMessage("कृपया प्रतीक्षा करें");
        progress.setCancelable(false);
        progress.show();
    }

    private void DialogClose(){
        if(progress!=null){
            progress.dismiss();
            //  backupDatabase();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        String newDay = dayOfMonth + "";
        String newMonth = (monthOfYear + 1) + "";
        if ((monthOfYear + 1) < 10) {
            newMonth = "0" + newMonth;
        }
        if (dayOfMonth < 10) {
            newDay = "0" + dayOfMonth;
        }
        String newDate =  year+ "-" + newMonth + "-" + newDay;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(date);
            date2 = sdf.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1 != null && date1.compareTo(date2) > 0) {
            Toast.makeText(FishBreedingWaterBodyListForm.this, "मान्य तिथि चुनें", Toast.LENGTH_LONG).show();
        }
        else {
            payment_date.setText(newDate);
            payment_date.setHint("");
        }
    }

}
