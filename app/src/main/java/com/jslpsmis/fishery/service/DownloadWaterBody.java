package com.jslpsmis.fishery.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class DownloadWaterBody extends Service {
    public static Context context;
    Dataprovider dataprovider;
    MDMSharedPreference mdmSharedPreference;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        dataprovider = new Dataprovider(context);
        mdmSharedPreference = MDMSharedPreference.getInstance(this);

        new WaterBodyApi().execute();

    }

    /*****************Input Required api ************************/
    public class  WaterBodyApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        //Remiaining inserting data in the table
                        String  waterBodyId=object.getString("WaterBodyId");
                        String  pgcode=object.optString("PgCode");
                        String  grpMemCode=object.optString("GrpMemCode");
                        String  grpCode=object.optString("GrpCode");
                        String  waterTypeString=object.optString("WaterBodyCode");
                        String  waterOwnerString=object.optString("WaterBodyOwnerCode");
                        String  waterAvailabilityString=object.optString("NoOfMonthWaterAvailable");
                        String  fisheryString=object.optString("SelectedFishery");


                        String  isFisheryHappening=object.optString("IsFisheryHappening");
                        String  isfishKeeperTakenTraining=object.optString("IsTrainingTaken");
                        String  questiontwodependent=object.optString("FromWhereTraining");
                        String  areaId=object.optString("ExpectedAreaInDecimal");
                        String  fishAverageProduction=object.optString("TotalProductionInQuintal");
                        String  fishAverageValue=object.optString("TotalIncome");
                        String  pathWaterBodyPic=object.optString("WaterBodyPicPath");

                        String  latitute=object.optString("Lat");
                        String  longititude=object.optString("Long");
                        String  waterBodyLocation=object.optString("Location");

                        boolean check = dataprovider.AddWaterBody(waterBodyId,pgcode,grpMemCode,grpCode, waterTypeString, waterOwnerString, waterAvailabilityString,
                                fisheryString, isFisheryHappening, isfishKeeperTakenTraining, questiontwodependent, areaId, fishAverageProduction,
                                fishAverageValue, pathWaterBodyPic,
                                latitute + "", longititude + "", waterBodyLocation, "", "insert");
                        if (check) {
                            new StyleableToast
                                    .Builder(context)
                                    .text("Water body Downloaded")
                                    .iconStart(R.drawable.right)
                                    .textColor(Color.WHITE)
                                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                    .show();
                        }
                    }
                }else{
                     stopSelf();
                }
            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJOHAR);

            request.addProperty("flag", AppConstant.tblAddWaterBody);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJOHAR, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                  stopSelf();
            }
            return resultt;
        }
    }
}
