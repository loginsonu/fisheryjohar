package com.jslpsmis.fishery.service;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

public class VolleyString {

    private StringRequest mStringRequest;
    private String url;
    private String tableIndentifier;
    VolleyListner volleyListner;

    public interface VolleyListner{
        void onResponseSuccess(String tableIndentifier, String result);
        void onResponseFailure(String tableIdentifier);
    }

    public VolleyString(String url, String tableIndentifier, VolleyListner volleyListner) {
        this.tableIndentifier=tableIndentifier;
        this.url=url;
        this.volleyListner = volleyListner;
    }


    public StringRequest getString(){
        mStringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">")+2);
                    String result = XmlString.replaceAll("</string>","");
                    volleyListner.onResponseSuccess(tableIndentifier,result);
                    },
                error -> {

                }
        ) {

            @Override
            protected Map<String, String> getParams()
            {

                return null;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postLogin(String userId, String password){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        response = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                        volleyListner.onResponseSuccess(tableIndentifier, response);
                    }catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                 System.out.println("");
                })
                 {
               @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("UserID", userId);
                params.put("Password",password);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postIncomeExpenditure(String jsonData,String version){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("sData", jsonData);
                params.put("version",version);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postGetIncomeExpenditureMaster(String flag){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("flag", "");
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postGetIncomeExpenditure(String Data){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("Data", Data);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postFisherySelectedMembers(String jsonData,String version){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("sData", jsonData);
                params.put("version",version);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }
    public StringRequest postWaterBody(String jsonData,String version){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("sData", jsonData);
                params.put("version",version);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }


    public StringRequest posttracFisheryPlan(String jsonData,String version){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("sData", jsonData);
                params.put("version",version);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postCropPlanningMemebers(String Data){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                try {
                    params.put("PgCode", Data);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postWaterBodyDownload(String data, String flag){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("whr", data);
                params.put("flag", flag);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }

    public StringRequest postDownloadFisheryPlan(String data, String flag){
        StringRequest mStringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String XmlString = response.substring(response.indexOf("\">") + 2);
                    String result = XmlString.replaceAll("</string>", "");
                    volleyListner.onResponseSuccess(tableIndentifier, result);
                },
                error -> {
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("whr", data);
                params.put("flag", flag);
                return params;
            }
        };
        mStringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return mStringRequest;
    }
}
