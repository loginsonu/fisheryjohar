package com.jslpsmis.fishery.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.DashboardActivity;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.dataprovider.Dataprovider;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import br.com.goncalves.pugnotification.notification.PugNotification;

public class UploadWaterBody extends Service {
    public static Context context;
    Dataprovider dataprovider;
    MDMSharedPreference mdmSharedPreference;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        dataprovider = new Dataprovider(context);
        mdmSharedPreference = MDMSharedPreference.getInstance(this);

        new UploadptchesData().execute();

}




    public class UploadptchesData extends AsyncTask<String,Void,String> {

        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();



        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Table");
                if(array.length()>0){
                    JSONObject object = array.getJSONObject(0);

                    int res = object.optInt("RetValue");
                    if(res == 1){

                        dataprovider.updateAddWaterBody(DashboardActivity.pgCode, DashboardActivity.grpMemberCode);
                        Constant.uploadWaterBodyInProgress = false;
                        PugNotification.with(context)
                                .load()
                                .title("Upload Add Water Body Data completed")
                                .message("Upload Add Water Body Data completed")
                                .bigTextStyle("Upload Add Water Body Data completed")
                                .smallIcon(R.mipmap.ic_launcher)
                                .largeIcon(R.mipmap.ic_launcher)
                                .flags(Notification.DEFAULT_ALL)
                                .simple()
                                .build();

                        stopSelf();
                        //btnUpload.setVisibility(View.GONE);

                    }
                    else{
                        //alert("असफल हुआ","क्रॉप अपलोड फेल्ड");
                        stopSelf();
                        Constant.uploadWaterBodyInProgress = false;
                    }


                }

            }catch (Exception e){
                stopSelf();
                Constant.uploadWaterBodyInProgress = false;
            }
        }

        @Override
        protected String doInBackground(String... strings) {


            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.Upload_tblAddWaterBody);
            request.addProperty("sData", DashboardActivity.sSaveDateAddWaterBody);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            //HttpTransportSE httpTransport = new HttpTransportSE(URL, 120000);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domainJohar, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.Upload_tblAddWaterBody, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
                Constant.uploadWaterBodyInProgress = false;
            }
            return resultt;
        }
    }


}
