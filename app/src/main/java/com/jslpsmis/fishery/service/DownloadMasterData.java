package com.jslpsmis.fishery.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.LoginActivity;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

import br.com.goncalves.pugnotification.notification.PugNotification;


public class DownloadMasterData extends Service {
    public static Context context;
    Dataprovider dataprovider;
    MDMSharedPreference mdmSharedPreference;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        dataprovider = new Dataprovider(context);
        mdmSharedPreference = MDMSharedPreference.getInstance(this);

        new ProducerMemberApi().execute();

    }

    /*****************Input Required api ************************/
    public class  ProducerMemberApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String shg;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);

                        ProducerGroupMemberActivityModel model2 = new ProducerGroupMemberActivityModel();
                        model2.setDistrictCode(object.optString("DistrictCode"));
                        model2.setBlockCode(object.optString("BlockCode"));
                        model2.setClusterCode(object.optString("ClusterCode"));
                        model2.setPgCode(object.optString("PGCode"));
                        model2.setGroupCode(object.optString("GroupCode"));
                        model2.setGroupMemberCode(object.optString("Group_M_Code"));
                        model2.setFatherName(object.optString("FatherName"));
                        model2.setHusbandName(object.optString("HusbandName"));
                        if(object.optString("MemberName_H").equals("null")) {
                            pgMemName = object.optString("MemberName");
                            model2.setMemberName(object.optString("MemberName"));
                        }else{
                            pgMemName = object.optString("MemberName_H");
                            model2.setMemberName(object.optString("MemberName_H"));
                        }

                        if(object.optString("Group_Name_H").equals("null")) {
                            shg = object.optString("Group_Name");
                            model2.setGroupName(object.optString("Group_Name"));
                        }else{
                            shg = object.optString("Group_Name_H");
                            model2.setGroupName(object.optString("Group_Name_H"));
                        }
                        listProducerMember.add(model2);
                        if(Constant.action.equals("insert")){
                            boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                    , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                    , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "insert");

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                        , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                        , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "update");
                            }
                            boolean check = dataprovider.saveProducerGroupMemberData(pgMemName, object.optString("HusbandName"), shg
                                    , object.optString("GroupCode"), object.optString("Group_M_Code"), object.optString("PGCode")
                                    , object.optString("DistrictCode"), object.optString("BlockCode"), object.optString("ClusterCode"), object.optString("CadreId"), "insert");

                        }

                    }

                    if(listProducerMember.size()>0){

                       new WaterBodyTypeApi().execute();

                    }


                }else{
                     stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJICA);

            request.addProperty("whr", LoginActivity.userName);
            request.addProperty("flag", AppConstant.AMM_M);
            request.addProperty("whr1", LoginActivity.password);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                  stopSelf();
            }
            return resultt;
        }
    }

    /*****************Input Required api ************************/
    public class  WaterBodyTypeApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String shg;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);

                        if(Constant.action.equals("insert")){
                            boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"),object.optString("TypeCode"),"insert");

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"), object.optString("TypeCode"), "delete");
                            }
                                boolean check = dataprovider.saveWaterBodyType(object.optString("TypeName"),object.optString("TypeCode"),"insert");

                        }

                    }

                  new WaterBodyOwnerApi().execute();

                }else{
                    stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJICA);

            request.addProperty("flag", AppConstant.tblWaterBodyType);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
            }
            return resultt;
        }
    }

    /*****************Input Required api ************************/
    public class  WaterBodyOwnerApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String shg;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);

                        if(Constant.action.equals("insert")){
                            boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"),object.optString("OwnerCode"),"insert");

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"), object.optString("OwnerCode"), "delete");
                            }
                            boolean check = dataprovider.saveWaterBodyOwner(object.optString("OwnerName"),object.optString("OwnerCode"),"insert");

                        }

                    }

                    new SelectedFishryApi().execute();

                }else{
                    stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJICA);

            request.addProperty("flag", AppConstant.tblmstwaterbodyowner);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
            }
            return resultt;
        }
    }

    /*****************Input Required api ************************/
    public class  SelectedFishryApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String shg;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);

                        if(Constant.action.equals("insert")){
                            boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"),object.optString("FisheryCode"),"insert");

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"), object.optString("FisheryCode"), "delete");
                            }
                            boolean check = dataprovider.saveFisheryType(object.optString("FisheryName"),object.optString("FisheryCode"),"insert");

                        }

                    }

                   new ItemRequiredForFishBreedingApi().execute();

                }else{
                    stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJICA);

            request.addProperty("flag", AppConstant.tblSelectedFishery);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
            }
            return resultt;
        }
    }




    public class  ItemRequiredForFishBreedingApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String itemName;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        if(!object.optString("ItemNamehindi").equals("")){
                              itemName = object.optString("ItemNamehindi");
                        }else{
                            itemName = object.optString("ItemName");
                        }
                        if(Constant.action.equals("insert")){

                            boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName,object.optString("ItemCode"),object.optString("Budgetcode"),"insert");

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName, object.optString("ItemCode"), object.optString("Budgetcode"),"delete");
                            }
                            boolean check = dataprovider.tblItemRequiredFishBreedingSave(itemName,object.optString("ItemCode"),object.optString("Budgetcode"),"insert");

                        }

                    }

                    new tblRateFishBreedingPerUnitApi().execute();

                }else{
                    stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJOHAR);

            request.addProperty("flag", AppConstant.tblMstItemRequiredFishBreeding);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domainJohar, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJOHAR, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
            }
            return resultt;
        }
    }


    public class  tblRateFishBreedingPerUnitApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String resultt = "";
        String pgMemName;
        String itemName;
        List<ProducerGroupMemberActivityModel> listProducerMember = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        if(!object.optString("ItemNamehindi").equals("")){
                            itemName = object.optString("ItemNamehindi");
                        }else{
                            itemName = object.optString("ItemName");
                        }
                        if(Constant.action.equals("insert")){

                            boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                    object.optString("Code"),"insert",object.optString("AreaDependent"));

                        }
                        else{
                            if (i == 0) {
                                //for deleting data for the first time
                                boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                        object.optString("Code"),"delete","");
                            }
                            boolean check = dataprovider.tblRateFishBreedingPerUnitSave(object.optString("Itemcode"),object.optString("MeasurementUnit"),object.optString("RatePerUnit"),object.optString("QuantityPerUnit"),
                                    object.optString("Code"),"insert",object.optString("AreaDependent"));

                        }

                    }

                    PugNotification.with(context)
                            .load()
                            .title("Master Data Download Completed")
                            .message("Master Data Download Successfully")
                            .bigTextStyle("Master Data Download Successfully")
                            .smallIcon(R.mipmap.ic_launcher)
                            .largeIcon(R.mipmap.ic_launcher)
                            .flags(Notification.DEFAULT_ALL)
                            .simple()
                            .build();
                    mdmSharedPreference.putString("masterDataDownLoadComplete","true");
                    stopSelf();

                }else{
                    stopSelf();
                }


            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJOHAR);

            request.addProperty("flag", AppConstant.tblRateFishBreedingPerUnit);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER12);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domainJohar, 520000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJOHAR, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                stopSelf();
            }
            return resultt;
        }
    }


}
