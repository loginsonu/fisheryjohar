package com.jslpsmis.fishery;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sonu on 4/4/2018.
 */

public class RecyclerViewAnimation {
    public static void animate(RecyclerView.ViewHolder holder, boolean goesDown){
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(holder.itemView, "translationX",
                0,0);
        animatorTranslateX.setDuration(400);

        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(holder.itemView,"translationY",150,120,80,40,20,0);
        animatorTranslateY.setDuration(400);

//        ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(holder.itemView, "rotationY",
//                180,0);
//        animatorTranslateX.setDuration(2000);
        animatorSet.playTogether(animatorTranslateX,animatorTranslateY);
        animatorSet.start();

        //goesDown==true ?
    }
}
