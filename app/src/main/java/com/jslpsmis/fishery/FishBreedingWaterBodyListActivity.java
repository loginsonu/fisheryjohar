package com.jslpsmis.fishery;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.adapter.FishBreedingWaterBodyListAdapter;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;

import java.util.ArrayList;

public class FishBreedingWaterBodyListActivity extends AppCompatActivity {

    TextView tvFarmerName,pgName,shgName;
    RecyclerView recyclerView;
    Dataprovider dataprovider;
    ArrayList<AddWaterModel> list;
    FishBreedingWaterBodyListAdapter aAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fish_breeding_water_body_list);
        init();
    }

    private void init() {
       // Intent intent = getIntent();
//        selectedFisheryName = intent.getStringExtra("selectFisheryName");
//        selectedFisheryCode = intent.getStringExtra("selectFisheryCode");

        tvFarmerName  = (TextView) findViewById(R.id.headingCropPlanLandInfo);
        pgName = (TextView) findViewById(R.id.textView10);
        shgName = (TextView) findViewById(R.id.textView40);

        tvFarmerName.setText("जल श्रोत"+"("+ DashboardActivity.memberName+")");
        pgName.setText(ProducerMemberActivity.pgName);
        shgName.setText(DashboardActivity.grpName);
        recyclerView = findViewById(R.id.recyclerViewBaseLineLandInfo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataprovider = new Dataprovider(this);
        list = dataprovider.getAddWaterList(AddWaterbodynew.pgCode);

        if(list.size()>0){
            aAdapter = new FishBreedingWaterBodyListAdapter(this,list);

            LinearLayoutManager verticalLayoutmanager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(FishBreedingWaterBodyListActivity.this, AddWaterbodynew.class);
        intent.putExtra("pgCode", AddWaterbodynew.pgCode);
        intent.putExtra("pgName", AddWaterbodynew.pgName);
        intent.putExtra("grpname", AddWaterbodynew.grpname);
        startActivity(intent);
        finish();
    }
}
