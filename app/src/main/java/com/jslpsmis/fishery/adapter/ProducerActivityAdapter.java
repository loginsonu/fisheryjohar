package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.ProducerActivityModel;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;
import com.jslpsmis.fishery.newmodule.ProducerMemberNewActivity;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by sonu on 4/4/2018.
 */

public class ProducerActivityAdapter extends RecyclerView.Adapter<ProducerActivityAdapter.MyViewHolder>  {

    Context context;
    ArrayList<ProducerActivityModel> itemList;
    private ArrayList<ProducerActivityModel> newItemList;
    Dataprovider dataprovider;
    PopupWindow popup;
    RecyclerView recyclerView;
    ProducerGroupMemberActivityPopUpAdapter aAdapter;
    private static String getshgname;
    ArrayList<String> getpgname;
    public static String totalcount;

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        ConstraintLayout container,new_container;
        TextView tvProducerGroup,memberCountProducerGroup;
        ImageView memberImage;

        public MyViewHolder(View view) {
            super(view);
            container = (ConstraintLayout) view.findViewById(R.id.container);
            new_container = (ConstraintLayout)view.findViewById(R.id.new_container);
            tvProducerGroup = (TextView) view.findViewById(R.id.tv_producerGroupName);
            memberCountProducerGroup = view.findViewById(R.id.memberCountProducerActivity);
            memberImage = view.findViewById(R.id.imageView9);
        }
    }

    public ProducerActivityAdapter(Context context, ArrayList<ProducerActivityModel> itemList, ArrayList<String> getpgname) {
           this.context = context;
           this.itemList = itemList;
           this.newItemList = new ArrayList<ProducerActivityModel>();
           this.newItemList.addAll(itemList);
           dataprovider = new Dataprovider(context);
           this.getpgname=getpgname;
    }

    @Override
    public ProducerActivityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_activity_producer, parent, false);
        return new ProducerActivityAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProducerActivityAdapter.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);
        ProducerActivityModel item = itemList.get(position);
        holder.tvProducerGroup.setText(item.getPgName());
        String pgCode = item.getPgCode();

        //holder.tvProducerGroup.setText(getpgname.get(i));
//        try {
//            if(getpgname.size()>0) {
//                for (int i = 0; i < getpgname.size(); i++) {
//                    holder.tvProducerGroup.setText(getpgname.get(i));
//                    Log.d("name",getpgname.get(i));
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        holder.memberImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = li.inflate(R.layout.view_producermem_list2, (ViewGroup) view.findViewById(R.id.producermemid));
                recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
                ArrayList<ProducerGroupMemberActivityModel> listt = new ArrayList<>();
                listt= dataprovider.getProducerGrpMemList(pgCode);
                totalcount=String.valueOf(listt.size());
                aAdapter = new ProducerGroupMemberActivityPopUpAdapter(context,listt);
                LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(verticalLayoutmanager);
                recyclerView.setAdapter(aAdapter);
                popup = new PopupWindow(layout, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
                popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
            }
        });

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProducerMemberNewActivity.class);
                intent.putExtra("pgCode",item.getPgCode());
                intent.putExtra("pgName",item.getPgName());
                intent.putExtra("flag","old");
                intent.putExtra("grpname",holder.tvProducerGroup.getText().toString());
                //should be commented out for testing pgcode is hardcoded for now
                // intent.putExtra("pgCode","3803");
                context.startActivity(intent);
            }
        });

        holder.new_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddWaterbodynew.class);
                intent.putExtra("pgCode",item.getPgCode());
                intent.putExtra("pgName",item.getPgName());
                intent.putExtra("flag","new");
                //should be commented out for testing pgcode is hardcoded for now
                // intent.putExtra("pgCode","3803");
                context.startActivity(intent);
            }
        });

        if(!TextUtils.isEmpty(totalcount)) {
            holder.memberCountProducerGroup.setText(totalcount);
        }else{
            ArrayList<ProducerGroupMemberActivityModel> listt = new ArrayList<>();
            listt= dataprovider.getProducerGrpMemList(pgCode);
            holder.memberCountProducerGroup.setText(listt.size()+"");
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();
        if (charText.length() == 0) {
            itemList.addAll(newItemList);
        } else {
            for (ProducerActivityModel dd : newItemList) {
                if (dd.getPgName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    itemList.add(dd);
                }
            }
        }
        notifyDataSetChanged();
    }
}
