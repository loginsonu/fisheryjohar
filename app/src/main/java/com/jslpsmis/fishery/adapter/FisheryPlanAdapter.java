package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.InputIndentCalculationForWaterBody;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;

import java.util.List;

/**
 * Created by sonu on 4/4/2018.
 */

public class FisheryPlanAdapter extends RecyclerView.Adapter<FisheryPlanAdapter.MyViewHolder> {

    Context context;
    List<FisheryPlantbl> list;
    Dataprovider dataprovider;
    String waterBodyAreas,waterBodyIds,flag;



    public class MyViewHolder extends RecyclerView.ViewHolder  {


        TextView date,totalCost,selectedFishery;
        ImageView edit,delete;


        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.textView2 );
            totalCost = (TextView) view.findViewById(R.id.textView39 );
            selectedFishery = view.findViewById(R.id.textView41 );
            edit = view.findViewById(R.id.imageView13);
            delete = view.findViewById(R.id.imageView14);
            //resourcesCount = view.findViewById(R.id.textView38);
            }
    }

    public FisheryPlanAdapter(Context context, List<FisheryPlantbl> itemList, String waterBodyAreas, String waterBodyIds, String flag) {
           this.context = context;
           this.list = itemList;
           dataprovider = new Dataprovider(context);
           this.waterBodyAreas=waterBodyAreas;
           this.waterBodyIds=waterBodyIds;
           this.flag=flag;

    }

    @Override
    public FisheryPlanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_fishey_plan, parent, false);

        return new FisheryPlanAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FisheryPlanAdapter.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);

        FisheryPlantbl item = list.get(position);

        if(item.getIsexported().equals("true")){
            holder.delete.setVisibility(View.GONE);
        }

        if(flag.equals("view")){
            holder.delete.setVisibility(View.GONE);
        }

        holder.date.setText(item.getFisheryplandate());
        holder.totalCost.setText(item.getTotalamount());
        String query = "Select Name from tblSelectedFishery where  Code='" + item.getSelectedfishery() + "' ";

        String selectedFisheryName =dataprovider.getRecord(query);
        holder.selectedFishery.setText(selectedFisheryName);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FisheryPlantbl.deleteAll(FisheryPlantbl.class, "fisheryplanid = ?", item.getFisheryplanid());
                Toast.makeText(context,"fishery plan  deleted successfully",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, InputIndentCalculationForWaterBody.class);
                intent.putExtra("waterBodyId",waterBodyIds);
                intent.putExtra("wbArea",waterBodyAreas);
                context.startActivity(intent);
                ((InputIndentCalculationForWaterBody)context).finish();
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, FishBreedingWaterBodyListForm.class);
//                intent.putExtra("selectedFishery",selectedFisheryName);
//                intent.putExtra("fisheryId",item.getFisheryId());
//                Constant.isEdit = "true";
//                context.startActivity(intent);

            }
        });
   }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
