package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AddWaterBodyActivity;
import com.jslpsmis.fishery.FishBreedingWaterBodyListActivity;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.DashboardModel;

import java.util.List;

/**
 * Created by sonu on 4/4/2018.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {

    Context context;
    List<DashboardModel> list;

    Dataprovider dataprovider;


    String selectFisheryCodeString;

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        ConstraintLayout container;
        TextView heading,resourcesCount;
        ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            container = (ConstraintLayout) view.findViewById(R.id.container_dashboard);
            heading = (TextView) view.findViewById(R.id.heading_dashboard);
            icon = view.findViewById(R.id.imageView2);
            //resourcesCount = view.findViewById(R.id.textView38);

            }

    }

    public DashboardAdapter(Context context, List<DashboardModel> itemList) {
           this.context = context;
           this.list = itemList;
           dataprovider = new Dataprovider(context);

    }

    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_dashboard, parent, false);

        return new DashboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);

        DashboardModel item = list.get(position);
        holder.heading.setText(item.getName());
        holder.icon.setImageResource(item.getImageIcon());

//        listPlot = dataprovider.getLandInfoList(DashboardActivity.farmerCode, DashboardActivity.pgCode,DashboardActivity.grpcode);
//        listCrop = dataprovider.getCropInfoListAll(DashboardActivity.pgCode, DashboardActivity.farmerCode,DashboardActivity.grpcode);
//        lisCropPlan = dataprovider.getCropPlannedInfoAll(DashboardActivity.farmerCode,DashboardActivity.pgCode,DashboardActivity.grpcode);



        
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.getName().equals("आधारभूत सर्वेक्षण")) {
                    callRespectiveClass(0);

                }

                else if(item.getName().equals("मत्स्य पालन योजना")) {
                    Intent intent = new Intent(context, FishBreedingWaterBodyListActivity.class);
                    context.startActivity(intent);

                }

                else if (item.getName().equals("फसल योजना")){
                    callRespectiveClass(2);

                }
            }
        });

   }

    private void callRespectiveClass(int pos) {
        switch (pos){
            case 0 :
                Intent intent = new Intent(context, AddWaterBodyActivity.class);
               // intent.putExtra("baseLine","land");
                context.startActivity(intent);
                break;

            case 1 :
                Intent intent1 = new Intent(context, FishBreedingWaterBodyListActivity.class);
                context.startActivity(intent1);
                break;

            case 2 :
               // alert("मौसम का चयन करें","मौसम का नाम चुनें जिसके लिए आप फसल योजना बना रहे हैं");
                break;


        }
    }

//    private void alert(String heading, String message){
//        AlertView alert = new AlertView(heading, message, AlertStyle.DIALOG);
//
//        for(int i =0;i<selectedFisheryList.size();i++){
//            String name = selectedFisheryList.get(i).getName();
//            String code = selectedFisheryList.get(i).getCode();
//            selectedFisheryName.add(name);
//            selectedFisheryCode.add(code);
//            alert.addAction(new AlertAction(name, AlertActionStyle.DEFAULT, action -> {
//                System.out.print("");
//                for(int j=0;j<selectedFisheryName.size();j++){
//                    String namee= selectedFisheryName.get(j);
//                    if(namee.equals(action.getTitle())){
//                        selectFisheryCodeString = selectedFisheryCode.get(j);
//                    }
//                }
//
//            Intent intent = new Intent(context, FishBreedingWaterBodyListActivity.class);
//            intent.putExtra("selectFisheryName",action.getTitle());
//            intent.putExtra("selectFisheryCode",selectFisheryCodeString);
//            context.startActivity(intent);
//            }));
//        }
//
//
//        alert.show((DashboardActivity)context);
//    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
