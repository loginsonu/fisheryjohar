package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.model.ContributionJoharModel;

import java.util.List;

/**
 * Created by sonu on 4/4/2018.
 */

public class ContributionJoharAdapter extends RecyclerView.Adapter<ContributionJoharAdapter.MyViewHolder> {

    Context context;
    List<ContributionJoharModel> list;



    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView item;
        TextView cost;
        TextView johar;
        TextView beneficiary;
        TextView convergence;

        public MyViewHolder(View view) {
            super(view);
            item = view.findViewById(R.id.item);
            cost = view.findViewById(R.id.cost);
            johar = view.findViewById(R.id.johar);
            beneficiary = view.findViewById(R.id.beneficiary);
            convergence = view.findViewById(R.id.convergence);
        }

    }

    public ContributionJoharAdapter(Context context, List<ContributionJoharModel> itemList) {
        this.context = context;
        this.list = itemList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_contribution_johar_activity, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);

        ContributionJoharModel item = list.get(position);
        holder.item.setText(item.getItemname());
        holder.cost.setText(item.getUnitcost());
        holder.johar.setText(item.getJohar());
        holder.beneficiary.setText(item.getBeneficiary());
        holder.convergence.setText(item.getConvergence());

        if(position==list.size()-1){
            holder.item.setTypeface(holder.item.getTypeface(), Typeface.BOLD);
            holder.cost.setTypeface(holder.cost.getTypeface(), Typeface.BOLD);
            holder.johar.setTypeface(holder.johar.getTypeface(), Typeface.BOLD);
            holder.beneficiary.setTypeface(holder.beneficiary.getTypeface(), Typeface.BOLD);
            holder.convergence.setTypeface(holder.convergence.getTypeface(), Typeface.BOLD);
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
