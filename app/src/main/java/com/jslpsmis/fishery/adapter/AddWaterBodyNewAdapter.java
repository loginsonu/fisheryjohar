package com.jslpsmis.fishery.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AddWaterBodyForm;
import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.DatabaseHelper;
import com.jslpsmis.fishery.InputIndentCalculationForWaterBody;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.commonclass.CheckConnectivity;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.Cropplanningmemberstbl;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterBodySpinnerModel;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;
import com.jslpsmis.fishery.model.ShowMemebrModel;
import com.jslpsmis.fishery.model.tblCropplanningshowmodel;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;
import com.jslpsmis.fishery.newmodule.IncomeExpActivity;
import com.jslpsmis.fishery.service.VolleyString;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class AddWaterBodyNewAdapter extends RecyclerView.Adapter<AddWaterBodyNewAdapter.MyViewHolder>implements VolleyString.VolleyListner  {

    Context context;
    List<AddWaterModel> list;
    Dataprovider dataprovider;
    PopupWindow popupWindow;
    ImageView close;
    String Pgcode,PgName,grpcode,grpmemcode;
    List<String> list2 = new ArrayList<>();
    ListView recyclerView,inexlist;
    CustomAdapter aAdapter;
    PopupWindow popup;
    CompletedIncomeExpenditure completedIncomeExpenditure;
    ListView spinnercheck,spinnerselectedlist;
    CustomAdapterWithCheckbox adaptermembers;
    ShowAdapterWithCheckbox showAdapterWithCheckbox;
    Button buttonCreateGroup;
    String croppplaningid,wbid,counter,wbids;
    ArrayList<String> CropPlanningMemberModel=new ArrayList<>();
    public String sSaveDateAddWaterBody;
    List<AddWaterModel> listAddwater;
    MDMSharedPreference mdmSharedPreference;
    ArrayList<ShowMemebrModel> grpmemberAdded=new ArrayList<>();
    JSONArray array = null,array2=null;
    public SQLiteDatabase dbcpa_db;
    private Cursor cursor;
    List<FisheryPlantbl> fisheryPlantblList;
    private DatabaseHelper dbHelper;
    String saveselectedmembers="",tbltracFisheryPlans="";

    Boolean deletestatus=false;
    AlertDialog.Builder builder;
    List<AddWaterBodySpinnerModel> selectedFisheryList;
    List<tblCropplanningshowmodel> tblCropplanningshowmodels;
    String versionString;
    ProgressDialog progress;

    @Override
    public void onResponseSuccess(String tableIndentifier, String result) {
        if(tableIndentifier.equals(AppConstant.Upload_tblAddWaterBodystatus)){
                 try {
                     JSONObject jsonObj = new JSONObject(result);
                     array = jsonObj.getJSONArray("Table");
                     if (array.length() > 0) {
                         JSONObject object = array.getJSONObject(0);
                         int res = object.optInt("RetValue");
                         if (res == 1) {
                             for (int i = 0; i < listAddwater.size(); i++) {
                                 dataprovider.updateAddWaterBody(Pgcode,wbids);
                                 String sql = "";
                                 String isExported = "true";
                                 sql = "Update tblAddWaterBody set IsExported='" + isExported + "'  where PgCode='" + Pgcode + "' and  WaterBodyId='" + wbids + "'";
                                 try {
                                     executeSql(sql);
                                 } catch (Exception e) {
                                     // TODO: handle exception
                                 }
                             }
                            // listAddwater.clear();
                            // Constant.uploadWaterBodyInProgress = false;
                             DialogClose();
                             new StyleableToast
                                     .Builder(context)
                                     .text("वॉटरबॉडी सफलतापूर्वक अपलोड किया गया")
                                     .iconStart(R.drawable.right)
                                     .textColor(Color.WHITE)
                                     .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                     .show();
                             try {
                                 Intent intent = new Intent(context, AddWaterbodynew.class);
                                 intent.putExtra("pgCode", Pgcode);
                                 intent.putExtra("pgName", PgName);
                                 intent.putExtra("grpname", AddWaterbodynew.grpname);
                                 context.startActivity(intent);
                                 ((AddWaterbodynew) context).finish();
                             } catch (ClassCastException e) {
                                 e.printStackTrace();
                             }
                         }else if(res == -99){
                             new StyleableToast
                                     .Builder(context)
                                     .text("Upload failed for tblAddWaterBody")
                                     .iconStart(R.drawable.wrong_icon_white)
                                     .textColor(Color.WHITE)
                                     .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                     .show();
                             DialogClose();
                         }
                         else {
                             /*new StyleableToast
                                     .Builder(context)
                                     .text("Upload failed for tblAddWaterBody")
                                     .iconStart(R.drawable.wrong_icon_white)
                                     .textColor(Color.WHITE)
                                     .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                     .show();*/
                             //sunita 12 anjal
                         }
                     }
                 }  catch (JSONException e) {
                     e.printStackTrace();
                 }
        }

        if(tableIndentifier.equals(AppConstant.Upload_tbltracFisheryPlanstatus)){
            try {
                JSONObject jsonObj = new JSONObject(result);
                array2 = jsonObj.getJSONArray("Table");
                if (array2.length() > 0) {
                    JSONObject object = array2.getJSONObject(0);
                    int res = object.optInt("RetValue");
                 //   Toast.makeText(context,"Return value= "+object.optString("RetValue").toString(),Toast.LENGTH_LONG).show();
                    if (res > 0) {
                        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                                .where(Condition.prop("pgcode").eq(Pgcode))
                                .where(Condition.prop("waterbodyid").eq(wbids))
                                .list();

                        if(fisheryPlantbls.size()>0) {
                            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                                fisheryPlantbls.get(i).setIsexported("true");
                                //fisheryPlantbls.get(i).setStatus("0");
                                fisheryPlantbls.get(i).save();
                            }
                        }
                        new StyleableToast
                                .Builder(context)
                                .text("मत्स्य योजना सफलतापूर्वक अपलोड किया गया")
                                .iconStart(R.drawable.right)
                                .textColor(Color.WHITE)
                                .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                .show();
                    }
                } else {
                        new StyleableToast
                                .Builder(context)
                                .text("Upload failed for tbltracFisheryPlans")
                                .iconStart(R.drawable.wrong_icon_white)
                                .textColor(Color.WHITE)
                                .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                .show();
                }

            }  catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(tableIndentifier.equals(AppConstant.Upload_FisherySelectedMembers_DataInsertstatus)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String data = jsonObject.optString("Data");
                if (data.equals("1")) {
                    List<Cropplanningmemberstbl> cropplanningmemberstbls = Select.from(Cropplanningmemberstbl.class)
                            .where(Condition.prop("pgcode").eq(Pgcode))
                            .where(Condition.prop("waterbodyid").eq(wbids))
                            .where(Condition.prop("isexported").eq("0"))
                            .list();
                    if (cropplanningmemberstbls.size() > 0) {
                        for (int i = 0; i < cropplanningmemberstbls.size(); i++) {
                            cropplanningmemberstbls.get(i).setIsexported("1");
                            cropplanningmemberstbls.get(i).save();
                        }
                    }
                    new StyleableToast
                            .Builder(context)
                            .text("क्रॉपप्लानिंगसदस्यों को सफलतापूर्वक अपलोड किया गया")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                            .show();
                    DialogClose();
                }
                if (!data.equals("1")) {
                /*    new StyleableToast
                            .Builder(context)
                            .text("Upload failed for cropplanningmemberstbls")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                            .show();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResponseFailure(String tableIdentifier) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        TextView waterBodyType,area,location;
        ImageView search_people_img,edit,delete,icon;
        TextView search_people,show_cp_count;
        ConstraintLayout container;
        Button textView8,moreDetails;
        FloatingActionButton uploadLandAndCropDetails;

        public MyViewHolder(View view) {
            super(view);
            waterBodyType = (TextView) view.findViewById(R.id.tv_producerGroupName);
            area = view.findViewById(R.id.textView);
            location = view.findViewById(R.id.textView6);
            edit = view.findViewById(R.id.imageView13);
            delete = view.findViewById(R.id.imageView14);
            moreDetails = view.findViewById(R.id.textView7);
            icon= view.findViewById(R.id.icon);
            search_people=view.findViewById(R.id.search_people);
            textView8=view.findViewById(R.id.textView8);
            search_people_img=view.findViewById(R.id.search_people_img);
            show_cp_count=view.findViewById(R.id.show_cp_count);
            container=view.findViewById(R.id.container);
            uploadLandAndCropDetails=view.findViewById(R.id.uploadLandAndCropDetails);
        }
    }

    public AddWaterBodyNewAdapter(Context context, List<AddWaterModel> itemList, String pgcode, String pgName, String versionString) {
        this.context = context;
        this.list = itemList;
        dataprovider = new Dataprovider(context);
        this.Pgcode=pgcode;
        this.PgName=pgName;
        this.versionString=versionString;
    }

    @Override
    public AddWaterBodyNewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_add_water_body_new, parent, false);

        return new AddWaterBodyNewAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBindViewHolder(AddWaterBodyNewAdapter.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);
        AddWaterModel item = list.get(position);
        wbids=list.get(position).getWaterBodyId();
        if(item.getIsExported() != null){
            holder.delete.setVisibility(View.GONE);
            holder.edit.setVisibility(View.GONE);
        }
        mdmSharedPreference = MDMSharedPreference.getInstance(context);
        dataprovider = new Dataprovider(context);
        String query = "Select Name from tblWaterBodyType where  Code='" + item.getWaterBodyCode() + "' ";
        String query1 = "Select Name from tblWaterBodyOwner where  Code='" + item.getWaterBodyOwnerCode() + "' ";
        String query2 = "Select Name from tblSelectedFishery where  Code='" + item.getSelectedFishery() + "' ";

        String waterType =dataprovider.getRecord(query);
        String waterOwner = dataprovider.getRecord(query1);
        String selectedFisheryy = dataprovider.getRecord(query2);

        ArrayList<ProducerGroupMemberActivityModel> listss = new ArrayList<>();
        listss= dataprovider.getProducerGrpMemList(Pgcode);

        if(listss.size()>0){
            for(int i=0;i<listss.size();i++){
                list2.add(listss.get(i).getMemberName());
            }
        }
        String peoples_added=String.valueOf(list2);
        holder.waterBodyType.setText("तालाब का प्रकार   :"+waterType+"("+waterOwner+")");
        holder.area.setText("अनुमानित क्षेत्रफल :"+item.getTotalArea()+"(ड़ीसमिल)");
        holder.location.setText(item.getLocation());
        holder.search_people.setText(peoples_added);

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(item.getPgCode()))
                .where(Condition.prop("waterbodyid").eq(item.getWaterBodyId()))
                .groupBy("fisheryplanid")
                .list();

        List<FisheryPlantbl> fisheryPlantbls3 = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(item.getPgCode()))
                .where(Condition.prop("waterbodyid").eq(item.getWaterBodyId()))
                .where(Condition.prop("isexported").eq("true"))
                .where(Condition.prop("status").notEq("0"))
                .groupBy("fisheryplanid")
                .list();

        if(fisheryPlantbls3.size()>0) {
            int count=0;
            for (int i = 0; i < fisheryPlantbls3.size(); i++) {
                holder.textView8.setBackgroundDrawable(ContextCompat.getDrawable(holder.textView8.getContext(), R.drawable.button_box));
                count++;
                //  if(i>=1) {
                    counter = String.valueOf(count);
                    holder.show_cp_count.setVisibility(View.VISIBLE);
                    holder.show_cp_count.setText("पिछली पूर्ण फसल योजना :" + counter);
               // }
            }
        }

        /*if(!Constant.uploadWaterBodyInProgress) {
            wbids=list.get(position).getWaterBodyId();
            listAddwater = dataprovider.getAddWaterListAll2(Pgcode,wbids);
        }else{
            listAddwater = new ArrayList<>();
        }*/

        wbids=list.get(position).getWaterBodyId();
        listAddwater = dataprovider.getAddWaterListAll2(Pgcode,wbids);
        if(listAddwater.size()>0){
            holder.uploadLandAndCropDetails.setVisibility(View.VISIBLE);
        }else {
            holder.delete.setVisibility(View.GONE);
        }

        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("isexported").eq("false"))
                .list();

        if(fisheryPlantblList.size()>0){
            holder.uploadLandAndCropDetails.setVisibility(View.VISIBLE);
        }else {
           // holder.uploadLandAndCropDetails.setVisibility(View.GONE);
        }

        holder.uploadLandAndCropDetails.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                AlertView alert = new AlertView(context.getString(R.string.notice),context.getString(R.string.notice_msg), AlertStyle.DIALOG);
                alert.addAction(new AlertAction(context.getString(R.string.upload), AlertActionStyle.DEFAULT, action -> {
                    CheckConnectivity checkConnectivity = new CheckConnectivity();
                    if(checkConnectivity.CheckConnection(context)){
                      wbids=item.getWaterBodyId();
                      listAddwater = dataprovider.getAddWaterListAll2(Pgcode,wbids);
                        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                                .where(Condition.prop("pgcode").eq(Pgcode))
                                .where(Condition.prop("waterbodyid").eq(wbids))
                                .where(Condition.prop("isexported").eq("false"))
                                .groupBy("fisheryplanid")
                                .list();

                        if((listAddwater.size()>0)||(fisheryPlantblList.size()>0)){
                            DialogShow();
                            converttblAddWaterBody(wbids,Pgcode);
                            //   new UploadDataToServerTable(context,AppConstant.Upload_tblAddWaterBody,sSaveDateAddWaterBody,"tblAddWaterBody");
                            callUploadWaterBody(sSaveDateAddWaterBody,versionString);
                            converttblFisheryPlan(wbids,Pgcode);
                            callUploadFisheryPlan(tbltracFisheryPlans,versionString);
                            converttblcropplanningmemebrs(wbids,Pgcode);
                            //new UploadDataToServerTable(context,AppConstant.Upload_tbltracFisheryPlan,tbltracFisheryPlans,"tbltracFisheryPlan");
                            callUploadFisherySelectedMembers(saveselectedmembers,versionString);
                            holder.uploadLandAndCropDetails.setVisibility(View.GONE);

                        }
                       else{
                        //       alert(context.getString(R.string.errorr),context.getString(R.string.no_data_found));
                        holder.uploadLandAndCropDetails.setVisibility(View.GONE);
                    }
                    } else{
                        alert(context.getString(R.string.net_problem),context.getString(R.string.net_msg));
                        if(listAddwater.size()>0 || fisheryPlantblList.size()>0){
                            holder.uploadLandAndCropDetails.setVisibility(View.VISIBLE);
                        }
                    }
                 }));
                alert.show((AddWaterbodynew)context);
            }
        });

        holder.show_cp_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = li.inflate(R.layout.view_show_cp_count_list, (ViewGroup) v.findViewById(R.id.show_cp_count_list));
                inexlist = (ListView) layout.findViewById(R.id.inexlist);
                tblCropplanningshowmodels=new ArrayList<>();
                tblCropplanningshowmodel tblCropplanningshowmodel=new tblCropplanningshowmodel();
                tblCropplanningshowmodel.setCropplanningcount("Slno");
                tblCropplanningshowmodel.setSelecteddate("Crpdate");
                tblCropplanningshowmodel.setCropplanningtype("Crptype");
                tblCropplanningshowmodel.setIncomeamount("Inc");
                tblCropplanningshowmodel.setExpenditureamount("EX");
                tblCropplanningshowmodel.setProfitloss("P/L");
                tblCropplanningshowmodels.add(tblCropplanningshowmodel);

                List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                        .where(Condition.prop("pgcode").eq(item.getPgCode()))
                        .where(Condition.prop("waterbodyid").eq(item.getWaterBodyId()))
                        .where(Condition.prop("isexported").eq("true"))
                        .where(Condition.prop("status").notEq("0"))
                        .groupBy("fisheryplanid")
                        .list();

                if(fisheryPlantbls.size() > 0) {
                    int count=0;
                    double totalprofitloss=0.0;
                    double totaltotalincome=0.0;
                    double totaltotalexpenditure=0.0;
                    for (int i = 0; i < fisheryPlantbls.size(); i++) {
                        count++;
                        String getfisheryplanid=fisheryPlantbls.get(i).getFisheryplanid();
                        String getfisheryplandate=fisheryPlantbls.get(i).getFisheryplandate();
                        String getfisheryselectedfishery=fisheryPlantbls.get(i).getSelectedfishery();

                       double totalincome=getIncomeTotal(item.getPgCode(),item.getWaterBodyId(),getfisheryplanid);
                       double totalexpenditure=getExpenditureTotal(item.getPgCode(),item.getWaterBodyId(),getfisheryplanid);
                       double profitloss=getProfitLossTotal(item.getPgCode(),item.getWaterBodyId(),getfisheryplanid);

                        com.jslpsmis.fishery.model.tblCropplanningshowmodel tblCropplanningshowmodelss=new tblCropplanningshowmodel();
                        tblCropplanningshowmodelss.setCropplanningcount(String.valueOf(count));
                        tblCropplanningshowmodelss.setSelecteddate(getfisheryplandate);
                        tblCropplanningshowmodelss.setCropplanningtype(getfisheryselectedfishery);
                        tblCropplanningshowmodelss.setIncomeamount(String.valueOf(totalincome));
                        tblCropplanningshowmodelss.setExpenditureamount(String.valueOf(totalexpenditure));
                        tblCropplanningshowmodelss.setProfitloss(String.valueOf(profitloss));
                        tblCropplanningshowmodels.add(tblCropplanningshowmodelss);
                        totaltotalincome=totaltotalincome+totalincome;
                        totaltotalexpenditure=totaltotalexpenditure+totalexpenditure;
                        totalprofitloss = totalprofitloss+profitloss;
                    }
                    com.jslpsmis.fishery.model.tblCropplanningshowmodel tblCropplanningshowmodelss=new tblCropplanningshowmodel();
                    tblCropplanningshowmodelss.setCropplanningcount("");
                    tblCropplanningshowmodelss.setSelecteddate("");
                    tblCropplanningshowmodelss.setCropplanningtype("Total");
                    tblCropplanningshowmodelss.setIncomeamount(String.valueOf(totaltotalincome));
                    tblCropplanningshowmodelss.setExpenditureamount(String.valueOf(totaltotalexpenditure));
                    tblCropplanningshowmodelss.setProfitloss(String.valueOf(totalprofitloss));
                    tblCropplanningshowmodels.add(tblCropplanningshowmodelss);
                }

                if(tblCropplanningshowmodels.size()>0){
                    completedIncomeExpenditure = new CompletedIncomeExpenditure(tblCropplanningshowmodels, context);
                    inexlist.setAdapter(completedIncomeExpenditure);
                }
                popup = new PopupWindow(layout, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
                popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                View container = popup.getContentView().getRootView();
                if(container != null) {
                    WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
                    WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
                    p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    p.dimAmount = 0.8f;
                    if(wm != null) {
                        wm.updateViewLayout(container, p);
                    }
                }
            }
        });

        if(fisheryPlantbls.size()>0) {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                if(fisheryPlantbls.get(i).getFisheryplanid().length()>0){
                    holder.delete.setVisibility(View.GONE);
                    holder.edit.setVisibility(View.GONE);
                    //Add code for show green button
                    holder.textView8.setBackgroundDrawable(ContextCompat.getDrawable(holder.textView8.getContext(), R.drawable.button_box));

                }
            }
        }

        holder.search_people_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = li.inflate(R.layout.view_producermem_list, (ViewGroup) v.findViewById(R.id.producermemid));
                recyclerView = (ListView) layout.findViewById(R.id.recyclerView);

                ArrayList<ProducerGroupMemberActivityModel> listss = new ArrayList<>();
                listss= dataprovider.getProducerGrpMemList(item.getPgCode());
                grpmemberAdded.clear();
                listAddwater.clear();
                listAddwater = dataprovider.ShowMemberslist(item.getPgCode(),item.getWaterBodyId());
                 for (int j = 0; j < listAddwater.size(); j++) {
                     String gpcode=listAddwater.get(j).getGrpCode();
                     String gpmemcode=listAddwater.get(j).getGrpMemberCode();
                     for (int k = 0; k < listss.size(); k++) {
                         if(gpcode.equals(listss.get(k).getGroupCode())){
                             if(gpmemcode.equals(listss.get(k).getGroupMemberCode())){
                                 String selectedmemebrs=listss.get(k).getMemberName();
                                 ShowMemebrModel showMemebrModel = new ShowMemebrModel();
                                 showMemebrModel.setMembername(selectedmemebrs);
                                 grpmemberAdded.add(showMemebrModel);
                             }
                         }
                     }
                 }
                //need code
                aAdapter = new CustomAdapter(grpmemberAdded,context);
                recyclerView.setAdapter(aAdapter);
                popup = new PopupWindow(layout, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
                popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                View container = popup.getContentView().getRootView();
                if(container != null) {
                    WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
                    WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
                    p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    p.dimAmount = 0.8f;
                    if(wm != null) {
                        wm.updateViewLayout(container, p);
                    }
                }
            }
        });

        holder.icon.setImageDrawable(null);
        holder.icon.setImageBitmap(BitmapFactory.decodeFile(item.getWaterBodyPicPath()));

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater liFarm = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layoutFarm = liFarm.inflate(R.layout.image_zoom_layout, (ViewGroup) v.findViewById(R.id.toastId));
                com.jsibbold .zoomage.ZoomageView imageFarm = (com.jsibbold.zoomage.ZoomageView) layoutFarm.findViewById(R.id.myZoomageView);

                close = (ImageView) layoutFarm.findViewById(R.id.btnClose);
                imageFarm.setImageDrawable(holder.icon.getDrawable());

               close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                popupWindow = new PopupWindow(layoutFarm, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, false);
                popupWindow.showAtLocation(layoutFarm, Gravity.CENTER, 0, 0);
                View container = popupWindow.getContentView().getRootView();
                if(container != null) {
                    WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
                    WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
                    p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                    p.dimAmount = 0.8f;
                    if(wm != null) {  
                        wm.updateViewLayout(container, p);
                    }
                }
            }
        });

        holder.moreDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater liFarm = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layoutFarm = liFarm.inflate(R.layout.view_detail_water_bodies, (ViewGroup) v.findViewById(R.id.toastId));
                TextView pondType = layoutFarm.findViewById(R.id.textView2);
                TextView pondOwner = layoutFarm.findViewById(R.id.textView27 );
                TextView waterAvailable = layoutFarm.findViewById(R.id.textView28);
                TextView selectedFishery = layoutFarm.findViewById(R.id.textView29);
                TextView fishFarming = layoutFarm.findViewById(R.id.textView30);
                TextView fishTraining = layoutFarm.findViewById(R.id.textView31 );
                TextView area = layoutFarm.findViewById(R.id.textView32);
                TextView production = layoutFarm.findViewById(R.id.textView33);
                TextView income = layoutFarm.findViewById(R.id.textView34);

                pondType.setText(waterType);
                pondOwner.setText(waterOwner);
                waterAvailable.setText(item.getNoOfMonthAvailable());
                selectedFishery.setText(selectedFisheryy);
                fishFarming.setText(item.getIsFisheryHappenig());
                fishTraining.setText(item.getIsTrainingTaken());
                area.setText(item.getTotalArea()+"(ड़ीसमिल)");
                production.setText(item.getTotalProduction()+"(किलो में)");
                income.setText(item.getTotalIncome()+"(रुपये)");

                popupWindow = new PopupWindow(layoutFarm, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
                popupWindow.showAtLocation(layoutFarm, Gravity.CENTER, 0, 0);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
                list = dataprovider.getProducerGrpMemList(item.getPgCode());

                grpcode = list.get(position).getGroupCode();
                grpmemcode = list.get(position).getGroupMemberCode();
                wbids = item.getWaterBodyId();
                Log.d("WBid", wbids);

                boolean check = dataprovider.AddWaterBody(wbids, Pgcode, "", "", "", "",
                        "", "", "", "", "", "",
                        "", "", "", "", "", "", item.getGuid(), "delete");
                if (check) {
                    try {
                        Intent intent = new Intent(context, AddWaterbodynew.class);
                        intent.putExtra("pgCode", Pgcode);
                        intent.putExtra("pgName", PgName);
                        intent.putExtra("grpname", AddWaterbodynew.grpname);
                        context.startActivity(intent);
                        ((AddWaterbodynew) context).finish();
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddWaterBodyForm.class);
                intent.putExtra("waterType",waterType);
                intent.putExtra("waterOwner",waterOwner);
                intent.putExtra("waterAvailable",item.getNoOfMonthAvailable());
                intent.putExtra("selectedFishery",selectedFisheryy);
                intent.putExtra("fishFarming",item.getIsFisheryHappenig());
                intent.putExtra("fishTraining",item.getIsTrainingTaken());
                intent.putExtra("fromWhereTraining",item.getFromwheretraing());
                intent.putExtra("area",item.getTotalArea());
                intent.putExtra("production",item.getTotalProduction());
                intent.putExtra("income",item.getTotalIncome());
                intent.putExtra("imageUrl",item.getWaterBodyPicPath());
                intent.putExtra("GUID",item.getGuid());
                intent.putExtra("pgcode",Pgcode);
                intent.putExtra("pgname",PgName);
                Constant.isEdit = "true";
                context.startActivity(intent);
                ((AddWaterbodynew)context).finish();
            }
        });

        holder.textView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wbids=item.getWaterBodyId();
                String totalarea=item.getTotalArea();
                String pgcode=item.getPgCode();
                Selectionpopup(v,item,wbids,totalarea,pgcode);
            }
        });
        getVersion();
        }

    public void Selectionpopup(View view, AddWaterModel item, String wbids, String totalarea, String pgcode){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        if(inflater == null) {
            return;
        }

        View popupView = View.inflate(context, R.layout.popup_window, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                true
        );

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        View container = popupWindow.getContentView().getRootView();
        if(container != null) {
            WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            p.dimAmount = 0.8f;
            if(wm != null) {
                wm.updateViewLayout(container, p);
            }
        }

        Button income_expenditure=(Button)popupView.findViewById(R.id.buttonexp);
        Button cropplanning=(Button)popupView.findViewById(R.id.buttoncrop);
        LinearLayout viewdeletelayout=(LinearLayout)popupView.findViewById(R.id.viewdeletelayout);
        TextView viewfisheryplan=(TextView)popupView.findViewById(R.id.viewfisheryplan);
        ImageView deletefisheryplan=(ImageView)popupView.findViewById(R.id.deletefisheryplan);

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .list();
        try {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("isexported").eq("0"))
                .list();

        List<FisheryPlantbl> fisheryPlantbls1 = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("isexported").eq("false"))
                .list();

        if (tblIncomeExpenditures.size() > 0) {
            for(int i=0;i<tblIncomeExpenditures.size();i++) {
                if (!tblIncomeExpenditures.get(i).getCropplanningid().equals(croppplaningid)) {
                    deletefisheryplan.setVisibility(View.VISIBLE);
                } else {
                    deletefisheryplan.setVisibility(View.GONE);
                }
            }
        } else {
            for (int i = 0; i < fisheryPlantbls1.size(); i++) {
                if (fisheryPlantbls1.get(i).getFisheryplanid().equals(croppplaningid)) {
                    deletestatus = true;
                    deletefisheryplan.setVisibility(View.VISIBLE);
                }else{
                    deletefisheryplan.setVisibility(View.GONE);
                }
            }
        }

        viewfisheryplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Intent intent = new Intent(context, InputIndentCalculationForWaterBody.class);
                intent.putExtra("waterBodyId",wbids);
                intent.putExtra("wbArea",totalarea);
                intent.putExtra("flag","view");
                intent.putExtra("pgcode",pgcode);
                intent.putExtra("grpname", AddWaterbodynew.pgName);
                intent.putExtra("membersarray","");
                context.startActivity(intent);
                ((AddWaterbodynew)context).finish();
            }
        });

        deletefisheryplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FisheryPlantbl.executeQuery("DELETE FROM FISHERY_PLANTBL WHERE FISHERYPLANID = '" + croppplaningid + "'");
                Cropplanningmemberstbl.executeQuery("DELETE FROM CROPPLANNINGMEMBERSTBL WHERE FISHERYPLANID = '" + croppplaningid + "'");

                popupWindow.dismiss();
                new StyleableToast
                        .Builder(context)
                        .text("रिकॉर्ड सफलतापूर्वक हटाया गया")
                        .iconStart(R.drawable.right)
                        .textColor(Color.WHITE)
                        .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                        .show();
                try {
                    Intent intent = new Intent(context, AddWaterbodynew.class);
                    intent.putExtra("pgCode", Pgcode);
                    intent.putExtra("pgName", PgName);
                    intent.putExtra("grpname", AddWaterbodynew.grpname);
                    context.startActivity(intent);
                    ((AddWaterbodynew) context).finish();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
        });

        cropplanning.setEnabled(true);
        viewdeletelayout.setVisibility(View.GONE);
        cropplanning.setBackgroundDrawable(ContextCompat.getDrawable(cropplanning.getContext(), R.drawable.button_box3));

        income_expenditure.setEnabled(false);
        income_expenditure.setBackgroundDrawable(ContextCompat.getDrawable(income_expenditure.getContext(), R.drawable.button_box2));

        for(int i=0;i<fisheryPlantbls.size();i++){
            try {
                String status = fisheryPlantbls.get(i).getStatus();
                if (status.equals("0")) {
                    cropplanning.setEnabled(false);
                    viewdeletelayout.setVisibility(View.VISIBLE);
                    cropplanning.setBackgroundDrawable(ContextCompat.getDrawable(cropplanning.getContext(), R.drawable.button_box2));
                    income_expenditure.setEnabled(true);
                    income_expenditure.setBackgroundDrawable(ContextCompat.getDrawable(income_expenditure.getContext(), R.drawable.button_box3));
                } else {
                    cropplanning.setEnabled(true);
                    viewdeletelayout.setVisibility(View.GONE);
                    cropplanning.setBackgroundDrawable(ContextCompat.getDrawable(cropplanning.getContext(), R.drawable.button_box3));
                    income_expenditure.setEnabled(false);
                    income_expenditure.setBackgroundDrawable(ContextCompat.getDrawable(income_expenditure.getContext(), R.drawable.button_box2));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        cropplanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Cropplanning(v,item,wbids,totalarea,pgcode);
            }
        });

        income_expenditure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                //IncomeExpenditureMembers(v,item);
                Intent intent=new Intent(context, IncomeExpActivity.class);
                intent.putExtra("waterBodyId",wbids);
                intent.putExtra("wbArea",totalarea);
                intent.putExtra("pgcode",pgcode);
                intent.putExtra("grpname",PgName);
                context.startActivity(intent);
                ((AddWaterbodynew)context).finish();
             //   Log.d("wbArea",item.getTotalArea()+item.getWaterBodyId()+Pgcode+PgName);
            }
        });
    }

    public void Cropplanning(View view, AddWaterModel item, String wbid, String totalarea, String pgcode){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        if(inflater == null) {
            return;
        }
        View popupView = View.inflate(context, R.layout.popup_cropplanning, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                true
        );
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        View container = popupWindow.getContentView().getRootView();
        if(container != null) {
            WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            p.dimAmount = 0.8f;
            if(wm != null) {
                wm.updateViewLayout(container, p);
            }
        }
        spinnercheck = (ListView) popupView.findViewById(R.id.spinnercheck);
        spinnerselectedlist=(ListView)popupView.findViewById(R.id.spinnerselectedlist);
        ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
        list= dataprovider.getProducerGrpMemList(pgcode);
        List<AddWaterModel> listAddwater = dataprovider.ShowMemberslist(Pgcode,wbids);
        for (int j = 0; j < listAddwater.size(); j++) {
                String gpcode=listAddwater.get(j).getGrpCode();
                String gpmemcode=listAddwater.get(j).getGrpMemberCode();
                for (int k = 0; k < list.size(); k++) {
                    if(gpcode.equals(list.get(k).getGroupCode())){
                        if(gpmemcode.equals(list.get(k).getGroupMemberCode())){
                            String selectedmemebrs=list.get(k).getMemberName();
                            list.remove(k).getMemberName();
                        }
                    }
                }
            }
        builder = new AlertDialog.Builder(context);
        buttonCreateGroup=(Button)popupView.findViewById(R.id.buttonCreateGroup);
        buttonCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if(CropPlanningMemberModel.size()>0) {
                    Intent intent = new Intent(context, InputIndentCalculationForWaterBody.class);
                    intent.putExtra("waterBodyId", wbid);
                    intent.putExtra("wbArea", totalarea);
                    intent.putExtra("pgcode", pgcode);
                    intent.putExtra("grpname", PgName);
                    intent.putExtra("membersarray", CropPlanningMemberModel);
                    intent.putExtra("flag", "planning");
                    context.startActivity(intent);
                    ((AddWaterbodynew)context).finish();
                    //check again
                }else {
                    new StyleableToast
                            .Builder(context)
                            .text("फसल नियोजन के लिए कम से कम एक सदस्य का चयन करें")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                            .show();
                }
            }
        });

        ArrayList<ProducerGroupMemberActivityModel> listss = new ArrayList<>();
        listss= dataprovider.getProducerGrpMemList(Pgcode);
        listAddwater.clear();
        listAddwater = dataprovider.ShowMemberslist(Pgcode,wbids);
        grpmemberAdded.clear();

        for (int j = 0; j < listAddwater.size(); j++) {
            String gpcode=listAddwater.get(j).getGrpCode();
            String gpmemcode=listAddwater.get(j).getGrpMemberCode();
            for (int k = 0; k < listss.size(); k++) {
                if(gpcode.equals(listss.get(k).getGroupCode())){
                    if(gpmemcode.equals(listss.get(k).getGroupMemberCode())){
                        String selectedmemebrs=listss.get(k).getMemberName();
                        String fathername=listss.get(k).getHusbandName();
                        String GroupName=listss.get(k).getGroupName();
                        ShowMemebrModel showMemebrModel = new ShowMemebrModel();
                        showMemebrModel.setMembername(selectedmemebrs);
                        showMemebrModel.setGrpcode(gpcode);
                        showMemebrModel.setGrpmemcode(gpmemcode);
                        showMemebrModel.setPgcode(Pgcode);
                        showMemebrModel.setWaterbodyid(wbids);
                        showMemebrModel.setFathername(fathername);
                        showMemebrModel.setGroupName(GroupName);
                        grpmemberAdded.add(showMemebrModel);
                        CropPlanningMemberModel.add(selectedmemebrs);
                    }
                }
            }
        }

        if(list.size()>0) {
            showAdapterWithCheckbox=new ShowAdapterWithCheckbox(grpmemberAdded,context);
            spinnerselectedlist.setAdapter(showAdapterWithCheckbox);
            adaptermembers = new CustomAdapterWithCheckbox(list,context);
            spinnercheck.setAdapter(adaptermembers);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomAdapter extends ArrayAdapter<ShowMemebrModel>{

        private ArrayList<ShowMemebrModel> dataSet;
        Context mContext;

        private class ViewHolder {
            TextView membername,pgcode,groupcode,groupmemcode,waterbodyid;
        }

        public CustomAdapter(ArrayList<ShowMemebrModel> data, Context context) {
            super(context, R.layout.row_item, data);
            this.dataSet = data;
            this.mContext=context;
        }

        private int lastPosition = -1;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ShowMemebrModel dataModel = getItem(position);
            ViewHolder viewHolder;
            final View result;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.row_item, parent, false);
                viewHolder.membername = (TextView) convertView.findViewById(R.id.membername);
                viewHolder.pgcode = (TextView) convertView.findViewById(R.id.pgcode);
                viewHolder.groupcode = (TextView) convertView.findViewById(R.id.groupcode);
                viewHolder.groupmemcode = (TextView) convertView.findViewById(R.id.groupmemcode);
                viewHolder.waterbodyid=(TextView)convertView.findViewById(R.id.waterbodyid);

                result=convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result=convertView;
            }
            Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.fadein : R.anim.down_from_top);
            result.startAnimation(animation);
            lastPosition = position;
            viewHolder.membername.setText(dataModel.getMembername());
            viewHolder.membername.setTag(position);
            return convertView;
        }
    }

    public class CustomAdapterWithCheckbox extends ArrayAdapter<ProducerGroupMemberActivityModel>{
        private List<ProducerGroupMemberActivityModel> dataSet;
        Context mContext;
        boolean[] checkBoxState;

        private class ViewHolder {
            TextView pgcode,groupcode,groupmemcode,waterbodyid,farmername,groupname;
            CheckBox membername;
        }

        public CustomAdapterWithCheckbox(List<ProducerGroupMemberActivityModel> data, Context context) {
            super(context, R.layout.checkbox_row_item, data);
            this.dataSet = data;
            this.mContext=context;
            //create the boolean array with
            //initial state as false
            checkBoxState=new boolean[data.size()];
        }

        private int lastPosition = -1;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ProducerGroupMemberActivityModel dataModel = getItem(position);
            ViewHolder viewHolder;
            final View result;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.checkbox_row_item, parent, false);
                viewHolder.membername = (CheckBox) convertView.findViewById(R.id.membername);
                viewHolder.pgcode = (TextView) convertView.findViewById(R.id.pgcode);
                viewHolder.groupcode = (TextView) convertView.findViewById(R.id.groupcode);
                viewHolder.groupmemcode = (TextView) convertView.findViewById(R.id.groupmemcode);
                viewHolder.waterbodyid=(TextView)convertView.findViewById(R.id.waterbodyid);

                viewHolder.farmername = (TextView) convertView.findViewById(R.id.farmername);
                viewHolder.groupname=(TextView)convertView.findViewById(R.id.groupname);

                result=convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result=convertView;
            }
            viewHolder.membername.setText(dataModel.getMemberName());
            viewHolder.membername.setChecked(checkBoxState[position]);
            viewHolder.groupcode.setText(dataModel.getGroupCode());
            viewHolder.groupmemcode.setText(dataModel.getGroupMemberCode());
            viewHolder.pgcode.setText(dataModel.getPgCode());

            viewHolder.waterbodyid.setText(wbid);
            viewHolder.farmername.setText(dataModel.getHusbandName());
            viewHolder.groupname.setText(dataModel.getGroupName());
            viewHolder.membername.setTag(position);

            viewHolder.membername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(((CheckBox)v).isChecked()) {
                    checkBoxState[position] = true;
                    CropPlanningMemberModel.add(viewHolder.membername.getText().toString());
                }
                else {
                    checkBoxState[position] = false;
                    CropPlanningMemberModel.remove(viewHolder.membername.getText().toString());
                }
            }
        });
            return convertView;
        }
    }

    public class ShowAdapterWithCheckbox extends ArrayAdapter<ShowMemebrModel>{
        private List<ShowMemebrModel> dataSet;
        Context mContext;
        boolean[] checkBoxState;

        private class ViewHolder {
            TextView pgcode,groupcode,groupmemcode,waterbodyid,farmername,groupname;
            CheckBox membername;
        }

        public ShowAdapterWithCheckbox(List<ShowMemebrModel> data, Context context) {
            super(context, R.layout.checkbox_row_item2, data);
            this.dataSet = data;
            this.mContext=context;
            checkBoxState=new boolean[data.size()];
        }

        private int lastPosition = -1;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ShowMemebrModel dataModel = getItem(position);
            ViewHolder viewHolder;
            final View result;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.checkbox_row_item2, parent, false);
                viewHolder.membername = (CheckBox) convertView.findViewById(R.id.membername);
                viewHolder.pgcode = (TextView) convertView.findViewById(R.id.pgcode);
                viewHolder.groupcode = (TextView) convertView.findViewById(R.id.groupcode);
                viewHolder.groupmemcode = (TextView) convertView.findViewById(R.id.groupmemcode);
                viewHolder.waterbodyid=(TextView)convertView.findViewById(R.id.waterbodyid);
                viewHolder.farmername = (TextView) convertView.findViewById(R.id.farmername);
                viewHolder.groupname=(TextView)convertView.findViewById(R.id.groupname);

                Arrays.fill(checkBoxState,true);

                result=convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result=convertView;
            }
            viewHolder.groupcode.setText(dataModel.getGrpcode());
            viewHolder.groupmemcode.setText(dataModel.getGrpmemcode());
            viewHolder.pgcode.setText(dataModel.getPgcode());
            viewHolder.farmername.setText(dataModel.getFathername());
            viewHolder.groupname.setText(dataModel.getGroupName());
            viewHolder.membername.setText(dataModel.getMembername());
            viewHolder.membername.setChecked(checkBoxState[position]);

            viewHolder.waterbodyid.setText(wbid);
            viewHolder.membername.setTag(position);
            viewHolder.membername.setOnCheckedChangeListener(null);

            viewHolder.membername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()) {
                        checkBoxState[position] = true;
                        CropPlanningMemberModel.add(viewHolder.membername.getText().toString());
                    }
                    else {
                        checkBoxState[position] = false;
                        CropPlanningMemberModel.remove(viewHolder.membername.getText().toString());
                    }
                }
            });
            return convertView;
        }
    }

    public class CompletedIncomeExpenditure extends ArrayAdapter<tblCropplanningshowmodel>{

        List<tblCropplanningshowmodel> dataSet;
        Context mContext;

        private class ViewHolder {
            TextView sno,Crpdate,Crptype,Inc,EX,PF;
        }
        public CompletedIncomeExpenditure(List<tblCropplanningshowmodel> data, Context context) {
            super(context, R.layout.row_item_for_incomeexpenditure, data);
            this.dataSet = data;
            this.mContext=context;
        }
        private int lastPosition = -1;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            tblCropplanningshowmodel dataModel = getItem(position);
            ViewHolder viewHolder;
            final View result;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.row_item_for_incomeexpenditure, parent, false);
                viewHolder.sno = (TextView) convertView.findViewById(R.id.sno);
                viewHolder.Crpdate = (TextView) convertView.findViewById(R.id.Crpdate);
                viewHolder.Crptype = (TextView) convertView.findViewById(R.id.Crptype);
                viewHolder.Inc = (TextView) convertView.findViewById(R.id.Inc);
                viewHolder.EX = (TextView) convertView.findViewById(R.id.EX);
                viewHolder.PF = (TextView) convertView.findViewById(R.id.PF);
                result=convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result=convertView;
            }
            Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.fadein : R.anim.down_from_top);
            result.startAnimation(animation);
            lastPosition = position;

            viewHolder.sno.setText(dataModel.getCropplanningcount());
            viewHolder.Crpdate.setText(String.valueOf(dataModel.getSelecteddate()));

            viewHolder.Inc.setText(String.valueOf(dataModel.getIncomeamount()));
            viewHolder.EX.setText(String.valueOf(dataModel.getExpenditureamount()));
            viewHolder.PF.setText(String.valueOf(dataModel.getProfitloss()));
            selectedFisheryList = dataprovider.getSelectedFisheryList();

            if(dataModel.getCropplanningtype().equals("Total")) {
                viewHolder.Crptype.setText("Total");
            }
            else if(dataModel.getCropplanningtype().equals("Crptype")) {
                viewHolder.Crptype.setText("Crptype");
            }
            else {
                for (int i = 0; i < selectedFisheryList.size(); i++) {
                    if (dataModel.getCropplanningtype().equals(selectedFisheryList.get(i).getCode())) {
                        viewHolder.Crptype.setText(selectedFisheryList.get(i).getName());
                    }
                }
            }
            viewHolder.sno.setTag(position);
            return convertView;
        }
    }

    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(context.getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show((AddWaterbodynew)context);
    }

    private void converttblAddWaterBody(String wbids,String Pgcode){
        listAddwater = dataprovider.getAddWaterListAll2(Pgcode,wbids);

        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("{");
        lStringBuilder.append("\"tblAddWaterBody\"");
        lStringBuilder.append(":");
        lStringBuilder.append("[");
        for (int i= 0; i<listAddwater.size();i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"PgCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getPgCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpMemCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpMemberCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyOwnerCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyOwnerCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"NoOfMonthWaterAvailable\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getNoOfMonthAvailable()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"SelectedFishery\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getSelectedFishery());
            lStringBuilder.append(",");

            lStringBuilder.append("\"IsFisheryHappening\"");
            lStringBuilder.append(":");
            String yes = listAddwater.get(i).getIsFisheryHappenig();
            if(yes.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }
            lStringBuilder.append(",");

            lStringBuilder.append("\"IsTrainingTaken\"");
            lStringBuilder.append(":");
            String yes1 = listAddwater.get(i).getIsTrainingTaken();
            if(yes1.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }
            lStringBuilder.append(",");

            lStringBuilder.append("\"FromWhereTraining\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getFromwheretraing()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"ExpectedAreaInDecimal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalArea());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalProductionInQuintal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalProduction());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalIncome\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalIncome());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyPicPath\"");
            lStringBuilder.append(":");
            String imagename = removeWords(listAddwater.get(i).getWaterBodyPicPath());
            lStringBuilder.append("\""+imagename+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedBy\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+userName()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Lat\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLat());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Long\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLongg());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Location\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getLocation()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedOn\"");
            lStringBuilder.append(":");
            lStringBuilder.append("null");
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyId\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getWaterBodyId()+"\"");
            lStringBuilder.append("");
            lStringBuilder.append(",");

            lStringBuilder.append("\"status\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getStatus()+"\"");
            lStringBuilder.append("");

            lStringBuilder.append("}");
            if(i<listAddwater.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
        lStringBuilder.append("}");
        sSaveDateAddWaterBody= lStringBuilder.toString();
        System.out.print("");
    }

    private String userName(){
        String user = mdmSharedPreference.getString("userNamee");
        return  user;
    }

    public static String removeWords(String word ) {
        return word.replace("/storage/emulated/0/Pictures/","");
    }

    public void callUploadWaterBody(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(context)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(context);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_tblAddWaterBody,
                    AppConstant.Upload_tblAddWaterBodystatus, this).postWaterBody(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(context)
                    .text(context.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                    .show();
            DialogClose();
        }
    }

    public void callUploadFisheryPlan(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(context)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(context);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_tbltracFisheryPlan,
                    AppConstant.Upload_tbltracFisheryPlanstatus, this).posttracFisheryPlan(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(context)
                    .text(context.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                    .show();
            DialogClose();
        }
    }

    public void callUploadFisherySelectedMembers(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(context)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(context);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_FisherySelectedMembers_DataInsert,
                    AppConstant.Upload_FisherySelectedMembers_DataInsertstatus, this).postFisherySelectedMembers(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(context)
                    .text(context.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                    .show();
            DialogClose();
        }
    }

    public void executeSql(String Sql) {
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            dbcpa_db.execSQL(Sql);
            Log.d("show",listAddwater.get(0).getIsExported());
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in executeSql :: " + exception.getMessage());
        }
    }
    //Income
    public Double getIncomeTotal(String pgcode,String waterbodyid,String fisheryplan ){
        double income=0.0;
        List<tblIncomeExpenditure> tblIncome = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(waterbodyid))
                .where(Condition.prop("cropplanningid").eq(fisheryplan))
                .where(Condition.prop("Inexflag").eq("IN"))
                .where(Condition.prop("isexported").eq("1"))
                .list();

        if(tblIncome.size() > 0) {
            for (int i = 0; i < tblIncome.size(); i++) {
                income += tblIncome.get(i).getTotalamount();
            }
        }
        return income;
    }

    //Expenditure
    public Double getExpenditureTotal(String pgcode,String waterbodyid,String fisheryplan ){
        double expenditure=0.0;
        List<tblIncomeExpenditure> tblExpenditure = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(waterbodyid))
                .where(Condition.prop("cropplanningid").eq(fisheryplan))
                .where(Condition.prop("Inexflag").eq("EX"))
                .where(Condition.prop("isexported").eq("1"))
                .list();

        if(tblExpenditure.size() > 0) {
            for (int i = 0; i < tblExpenditure.size(); i++) {
                expenditure += tblExpenditure.get(i).getTotalamount();
            }
        }
        return expenditure;
    }

    //ProfitLoss
    public Double getProfitLossTotal(String pgcode,String waterbodyid,String fisheryplan ){
        double incomes=0.0,expenditures=0.0,net_total_cal=0.0;
        List<tblIncomeExpenditure> tblIncome = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(waterbodyid))
                .where(Condition.prop("cropplanningid").eq(fisheryplan))
                .where(Condition.prop("Inexflag").eq("IN"))
                .where(Condition.prop("isexported").eq("1"))
                .list();

        if(tblIncome.size() > 0) {
            for (int i = 0; i < tblIncome.size(); i++) {
                incomes += tblIncome.get(i).getTotalamount();
            }
        }

        List<tblIncomeExpenditure> tblExpenditure = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(pgcode))
                .where(Condition.prop("waterbodyid").eq(waterbodyid))
                .where(Condition.prop("cropplanningid").eq(fisheryplan))
                .where(Condition.prop("Inexflag").eq("EX"))
                .where(Condition.prop("isexported").eq("1"))
                .list();

        if(tblExpenditure.size() > 0) {
            for (int i = 0; i < tblExpenditure.size(); i++) {
                expenditures += tblExpenditure.get(i).getTotalamount();
            }
        }
        net_total_cal = incomes - expenditures;
        return net_total_cal;
    }

    private void converttblFisheryPlan(String wbids,String Pgcode){

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("isexported").eq("false"))
                .groupBy("fisheryplanid")
                .list();

        StringBuilder lStringBuilder = new StringBuilder();
       /* lStringBuilder.append("{");
        lStringBuilder.append("\"tbltracFisheryPlan\"");
        lStringBuilder.append(":");*/
        lStringBuilder.append("[");

        for (int i = 0; i< fisheryPlantbls.size(); i++){

            lStringBuilder.append("{");
            lStringBuilder.append("\"fisheryplanid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getFisheryplanid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"ID\"");
            lStringBuilder.append(":");
            lStringBuilder.append(0);
            lStringBuilder.append(",");

            lStringBuilder.append("\"waterbodyid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getWaterbodyid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"fisheryplandate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getFisheryplandate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"selectedfishery\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getSelectedfishery()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"totalamount\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getTotalamount()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"createdby\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getCreatedby()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"isexported\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+"true"+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"individualcost\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getIndividualcost()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"budgetcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getBudgetcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"pgcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getPgcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpmemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getGrpmemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getGrpcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"areadependent\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getAreadependent()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"year\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getYear()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"month\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getMonth()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"itemname\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getItemname()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"itemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getItemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"status\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantbls.get(i).getStatus()+"\"");

            lStringBuilder.append("}");
            if(i<fisheryPlantbls.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
      //  lStringBuilder.append("}");
        tbltracFisheryPlans= lStringBuilder.toString();
        System.out.print("");
        Log.d("tbltracFisheryPlan",tbltracFisheryPlans);
    }

    private void converttblcropplanningmemebrs(String wbids,String Pgcode){
        List<Cropplanningmemberstbl> cropplanningmemberstbls = Select.from(Cropplanningmemberstbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("isexported").eq("0"))
                .list();

        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("[");
        for (int i = 0; i< cropplanningmemberstbls.size(); i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"SelectedMemberUUID\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getSelectedmemberuuid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Membername\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getMembername()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"pgcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getPgcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpmemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getGrpmemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getGrpcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"isexported\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getIsexported()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"waterbodyId\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getWaterbodyId()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"fisheryplanid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getFisheryplanid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createddate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getCreateddate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createdby\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getCreatedby()+"\"");

            lStringBuilder.append("}");
            if(i<cropplanningmemberstbls.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
        saveselectedmembers= lStringBuilder.toString();
        System.out.print("");
        Log.d("saveselectedmembers",saveselectedmembers);
    }

    private void getVersion() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionString = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void DialogShow() {
        progress= new ProgressDialog(context);
        progress.setMessage("अपलोड हो रहा है, कृपया प्रतीक्षा करें...");
        progress.setCancelable(false);
        progress.show();
    }

    private void DialogClose(){
        if(progress!=null){
            progress.dismiss();
        }
    }


}
