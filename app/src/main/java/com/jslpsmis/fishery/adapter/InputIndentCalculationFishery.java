package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.PriceAndRateModel;

import java.util.List;

/**
 * Created by sonu on 4/4/2018.
 */

public class InputIndentCalculationFishery extends RecyclerView.Adapter<InputIndentCalculationFishery.MyViewHolder> {

    Context context;
    List<PriceAndRateModel> list;

    Dataprovider dataprovider;


    String selectFisheryCodeString;

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        TextView name,quantity,price;


        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.nameId);
            quantity = view.findViewById(R.id.quantityId);
            price = view.findViewById(R.id.priceId);

            }

    }

    public InputIndentCalculationFishery(Context context, List<PriceAndRateModel> itemList) {
           this.context = context;
           this.list = itemList;
           dataprovider = new Dataprovider(context);

    }

    @Override
    public InputIndentCalculationFishery.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_input_indent_required, parent, false);

        return new InputIndentCalculationFishery.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InputIndentCalculationFishery.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);

        PriceAndRateModel item = list.get(position);
        holder.name.setText(item.getName());
        holder.quantity.setText(item.getQuantity()+" "+item.getMeasureMentUnit());
        holder.price.setText(item.getPrice());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
