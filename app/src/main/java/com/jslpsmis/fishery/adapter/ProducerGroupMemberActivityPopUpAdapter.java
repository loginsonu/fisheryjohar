    package com.jslpsmis.fishery.adapter;

    import android.content.Context;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.TextView;

    import androidx.recyclerview.widget.RecyclerView;

    import com.jslpsmis.fishery.R;
    import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

    import java.util.ArrayList;

    /**
     * Created by sonu on 4/4/2018.
     */

    public class ProducerGroupMemberActivityPopUpAdapter extends RecyclerView.Adapter<ProducerGroupMemberActivityPopUpAdapter.MyViewHolder>  {

        private Context context;
        private ArrayList<ProducerGroupMemberActivityModel> itemList;


        public class MyViewHolder extends RecyclerView.ViewHolder  {

            TextView farmerName;

            public MyViewHolder(View view) {
                super(view);
                farmerName = (TextView) view.findViewById(R.id.producerGroupMemberName);

            }

        }

        public ProducerGroupMemberActivityPopUpAdapter(Context context, ArrayList<ProducerGroupMemberActivityModel> itemList) {
               this.context = context;
               this.itemList = itemList;

        }

        @Override
        public ProducerGroupMemberActivityPopUpAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_child_producermem_list, parent, false);

            return new ProducerGroupMemberActivityPopUpAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ProducerGroupMemberActivityPopUpAdapter.MyViewHolder holder, int position) {
           // RecyclerViewAnimation.animate(holder, true);

            ProducerGroupMemberActivityModel item = itemList.get(position);

            holder.farmerName.setText(item.getMemberName());


       }

        @Override
        public int getItemCount() {
            return itemList.size();
        }


    }
