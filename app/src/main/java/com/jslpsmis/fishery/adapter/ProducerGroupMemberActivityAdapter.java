    package com.jslpsmis.fishery.adapter;

    import android.content.Context;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.ImageView;
    import android.widget.TextView;

    import androidx.constraintlayout.widget.ConstraintLayout;
    import androidx.recyclerview.widget.RecyclerView;

    import com.jslpsmis.fishery.R;
    import com.jslpsmis.fishery.RecyclerViewAnimation;
    import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

    import java.io.File;
    import java.util.ArrayList;
    import java.util.Locale;

    /**
     * Created by sonu on 4/4/2018.
     */

    public class ProducerGroupMemberActivityAdapter extends RecyclerView.Adapter<ProducerGroupMemberActivityAdapter.MyViewHolder>  {

        private Context context;

        private ArrayList<ProducerGroupMemberActivityModel> newItemList;
        private ArrayList<ProducerGroupMemberActivityModel> itemList;

        String flag="";

        public class MyViewHolder extends RecyclerView.ViewHolder  {

            ConstraintLayout container;
            TextView farmerName,shg,plotCount,cropCount,cropPlanCount;
            ImageView memberImage;

            public MyViewHolder(View view) {
                super(view);

                container = (ConstraintLayout) view.findViewById(R.id.containerActivityProducerGroupMembers);
                farmerName = (TextView) view.findViewById(R.id.textView5);
                shg = (TextView) view.findViewById(R.id.textView3);
//                plotCount = view.findViewById(R.id.textView6);
//                cropCount = view.findViewById(R.id.textView7);
//                cropPlanCount = view.findViewById(R.id.textView8);
//                memberImage = view.findViewById(R.id.imageView11);


            }

        }

        public ProducerGroupMemberActivityAdapter(Context context, ArrayList<ProducerGroupMemberActivityModel> itemList, String flag) {
               this.context = context;
               this.itemList = itemList;
               this.newItemList = new ArrayList<ProducerGroupMemberActivityModel>();
               this.newItemList.addAll(itemList);
               this.flag=flag;

        }

        @Override
        public ProducerGroupMemberActivityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_activity_producer_group_members, parent, false);

            return new ProducerGroupMemberActivityAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ProducerGroupMemberActivityAdapter.MyViewHolder holder, int position) {
            RecyclerViewAnimation.animate(holder, true);
            ProducerGroupMemberActivityModel item = itemList.get(position);
            String guardian;

            if(!item.getHusbandName().equals(null)){
                guardian =item.getHusbandName();
            }else {
                guardian=item.getFatherName();
            }

            holder.farmerName.setText(item.getMemberName()+"("+guardian+")");
            holder.shg.setText(context.getString(R.string.shg)+" "+item.getGroupName());


            /*holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(flag.equals("old")) {
                        Intent intent = new Intent(context, DashboardActivity.class);
                        intent.putExtra("pgCode", item.getPgCode());
                        intent.putExtra("grpCode", item.getGroupCode());
                        intent.putExtra("grpMemberCode", item.getGroupMemberCode());
                        intent.putExtra("memberName", item.getMemberName());
                        intent.putExtra("shg", item.getGroupName());
                        context.startActivity(intent);
                    }else {
                        Toast.makeText(context,"Add new Water body",Toast.LENGTH_SHORT).show();
                    }
                }
            });*/

        }

        @Override
        public int getItemCount() {
            return itemList.size();
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            itemList.clear();
            if (charText.length() == 0) {
                itemList.addAll(newItemList);
            } else {
                for (ProducerGroupMemberActivityModel dd : newItemList) {
                    if (dd.getMemberName().toLowerCase(Locale.getDefault())
                            .contains(charText)||dd.getGroupName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        itemList.add(dd);
                    }
                }
            }
            notifyDataSetChanged();
        }

        public  boolean isFileExist(File path) {
            if( path.exists() ) {
                return true;
            }else{
                return  false;
            }

        }
    }
