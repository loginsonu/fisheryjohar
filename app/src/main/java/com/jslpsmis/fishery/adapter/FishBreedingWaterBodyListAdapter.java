package com.jslpsmis.fishery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AddWaterBodyActivity;
import com.jslpsmis.fishery.AddWaterBodyForm;
import com.jslpsmis.fishery.DashboardActivity;
import com.jslpsmis.fishery.FishBreedingWaterBodyListActivity;
import com.jslpsmis.fishery.InputIndentCalculationForWaterBody;
import com.jslpsmis.fishery.RecyclerViewAnimation;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterBodySpinnerModel;
import com.jslpsmis.fishery.model.AddWaterModel;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by sonu on 4/4/2018.
 */

public class FishBreedingWaterBodyListAdapter extends RecyclerView.Adapter<FishBreedingWaterBodyListAdapter.MyViewHolder> {

    Context context;
    List<AddWaterModel> list;

    Dataprovider dataprovider;
    PopupWindow popupWindow;
    ImageView close;

    List<AddWaterBodySpinnerModel> selectedFisheryList;
    List<String> selectedFisheryName = new ArrayList<>();
    List<String> selectedFisheryCode= new ArrayList<>();
    String selectFisheryCodeString;


    public class MyViewHolder extends RecyclerView.ViewHolder  {

        TextView waterBodyType,area,location, fishBreedingType;
        ImageView icon,edit,delete;


        public MyViewHolder(View view) {
            super(view);

            waterBodyType = (TextView) view.findViewById(R.id.tv_producerGroupName);
            area = view.findViewById(R.id.textView);
            location = view.findViewById(R.id.textView6);

            icon = view.findViewById(R.id.imageView12);
            edit = view.findViewById(R.id.imageView13);
            delete = view.findViewById(R.id.imageView14);
            fishBreedingType = view.findViewById(R.id.textView7);

            }

    }

    public FishBreedingWaterBodyListAdapter(Context context, List<AddWaterModel> itemList) {
           this.context = context;
           this.list = itemList;
           dataprovider = new Dataprovider(context);
          selectedFisheryList = dataprovider.getSelectedFisheryList();

    }

    @Override
    public FishBreedingWaterBodyListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_add_water_body_fish_breeding, parent, false);

        return new FishBreedingWaterBodyListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FishBreedingWaterBodyListAdapter.MyViewHolder holder, int position) {
        RecyclerViewAnimation.animate(holder, true);

        AddWaterModel item = list.get(position);

        String query = "Select Name from tblWaterBodyType where  Code='" + item.getWaterBodyCode() + "' ";
        String query1 = "Select Name from tblWaterBodyOwner where  Code='" + item.getWaterBodyOwnerCode() + "' ";
        String query2 = "Select Name from tblSelectedFishery where  Code='" + item.getSelectedFishery() + "' ";

        String waterType =dataprovider.getRecord(query);
        String waterOwner = dataprovider.getRecord(query1);
        String selectedFisheryy = dataprovider.getRecord(query2);

        holder.waterBodyType.setText("तालाब का प्रकार   :"+waterType);
        holder.area.setText("अनुमानित क्षेत्रफल :"+item.getTotalArea()+"(ड़ीसमिल)");
        holder.location.setText(item.getLocation());
        holder.icon.setImageBitmap(BitmapFactory.decodeFile(item.getWaterBodyPicPath()));


        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater liFarm = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layoutFarm = liFarm.inflate(R.layout.image_zoom_layout, (ViewGroup) v.findViewById(R.id.toastId));
                com.jsibbold .zoomage.ZoomageView imageFarm = (com.jsibbold.zoomage.ZoomageView) layoutFarm.findViewById(R.id.myZoomageView);
                close = (ImageView) layoutFarm.findViewById(R.id.btnClose);
                imageFarm.setImageDrawable(holder.icon.getDrawable());

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                popupWindow = new PopupWindow(layoutFarm, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, false);
                popupWindow.showAtLocation(layoutFarm, Gravity.CENTER, 0, 0);
            }
        });


        holder.fishBreedingType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // alert("मत्स्य पालन का चयन करें","");
                Intent intent = new Intent(context, InputIndentCalculationForWaterBody.class);
                intent.putExtra("wbArea",item.getTotalArea());
               intent.putExtra("waterBodyId",item.getWaterBodyId());
                context.startActivity(intent);

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = dataprovider.AddWaterBody("", DashboardActivity.pgCode, DashboardActivity.grpMemberCode, DashboardActivity.grpCode,"","",
                        "","","","","","",
                        "","","","","","",item.getGuid(),"delete");

                Intent intent = new Intent(context, AddWaterBodyActivity.class);
                context.startActivity(intent);
                ((AddWaterBodyActivity)context).finish();
            }
        });



        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddWaterBodyForm.class);
                intent.putExtra("waterType",waterType);
                intent.putExtra("waterOwner",waterOwner);
                intent.putExtra("waterAvailable",item.getNoOfMonthAvailable());
                intent.putExtra("selectedFishery",selectedFisheryy);
                intent.putExtra("fishFarming",item.getIsFisheryHappenig());
                intent.putExtra("fishTraining",item.getIsTrainingTaken());
                intent.putExtra("fromWhereTraining",item.getFromwheretraing());
                intent.putExtra("area",item.getTotalArea());
                intent.putExtra("production",item.getTotalProduction());
                intent.putExtra("income",item.getTotalIncome());
                intent.putExtra("imageUrl",item.getWaterBodyPicPath());
                intent.putExtra("GUID",item.getGuid());
                Constant.isEdit = "true";
                context.startActivity(intent);
            }
        });

        holder.delete.setVisibility(View.GONE);
        holder.edit.setVisibility(View.GONE);

   }


        private void alert(String heading, String message){
        AlertView alert = new AlertView(heading, message, AlertStyle.DIALOG);

        for(int i =0;i<selectedFisheryList.size();i++){
            String name = selectedFisheryList.get(i).getName();
            String code = selectedFisheryList.get(i).getCode();
            selectedFisheryName.add(name);
            selectedFisheryCode.add(code);
            alert.addAction(new AlertAction(name, AlertActionStyle.DEFAULT, action -> {
                System.out.print("");
                for(int j=0;j<selectedFisheryName.size();j++){
                    String namee= selectedFisheryName.get(j);
                    if(namee.equals(action.getTitle())){
                        selectFisheryCodeString = selectedFisheryCode.get(j);
                    }
                }

            Intent intent = new Intent(context, InputIndentCalculationForWaterBody.class);
            intent.putExtra("selectFisheryName",action.getTitle());
            intent.putExtra("selectFisheryCode",selectFisheryCodeString);
            context.startActivity(intent);
            }));
        }


        alert.show((FishBreedingWaterBodyListActivity)context);
    }





    @Override
    public int getItemCount() {
        return list.size();
    }
}
