    package com.jslpsmis.fishery.seeddata;

    import com.jslpsmis.fishery.R;
    import com.jslpsmis.fishery.model.DashboardModel;

    import java.util.ArrayList;
    import java.util.List;

    /**
     * Created by sonu on 4/5/2018.
     */
    public class SeedDataDashboard {
        //private static final String[] title = new String[] {"आधारभूत भूमि की जानकारी", "आधारभूत भूमि फसल की जानकारी","फसल योजना"};
        private static final String[] title = new String[] {"आधारभूत सर्वेक्षण","मत्स्य पालन योजना"};

        private static final int[] imageIcon= new int[] {
                R.drawable.survey_icon,
                R.drawable.fish_breeding_icon,
        };

        public static List<DashboardModel> getListData(){
            List<DashboardModel> list = new ArrayList<>();
            for (int i = 0; i < title.length; i++)
            {
                DashboardModel dashboardFragmentModel = new DashboardModel(title[i],imageIcon[i]);
                // Binds all strings into an array
                list.add(dashboardFragmentModel);
            }
            return list;
        }
    }
