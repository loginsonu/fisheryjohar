package com.jslpsmis.fishery.model;

/**
 * Created by sonu on 4/4/2018.
 */

public class ProducerGroupMemberActivityModel {
    boolean selected = false;
    private String districtCode,blockCode,clusterCode,pgCode,groupCode,groupMemberCode,memberName,fatherName,husbandName,groupName,cadreId;


    public ProducerGroupMemberActivityModel(){

    }

    public String getDistrictCode() {
        return districtCode;
    }

    public String getCadreId() {
        return cadreId;
    }

    public void setCadreId(String cadreId) {
        this.cadreId = cadreId;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getClusterCode() {
        return clusterCode;
    }

    public void setClusterCode(String clusterCode) {
        this.clusterCode = clusterCode;
    }

    public String getPgCode() {
        return pgCode;
    }

    public void setPgCode(String pgCode) {
        this.pgCode = pgCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupMemberCode() {
        return groupMemberCode;
    }

    public void setGroupMemberCode(String groupMemberCode) {
        this.groupMemberCode = groupMemberCode;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getHusbandName() {
        return husbandName;
    }

    public void setHusbandName(String husbandName) {
        this.husbandName = husbandName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
