package com.jslpsmis.fishery.model;

public class PriceAndRateModel {
    private String name,quantity,price,measureMentUnit;

    public String getMeasureMentUnit() {
        return measureMentUnit;
    }

    public void setMeasureMentUnit(String measureMentUnit) {
        this.measureMentUnit = measureMentUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
