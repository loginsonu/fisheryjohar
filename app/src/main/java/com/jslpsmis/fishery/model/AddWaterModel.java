package com.jslpsmis.fishery.model;

public class AddWaterModel {

    private String pgCode,grpMemberCode,grpCode,guid,waterBodyCode,waterBodyOwnerCode,noOfMonthAvailable,selectedFishery,
    isFisheryHappenig,isTrainingTaken,fromwheretraing,totalArea,totalProduction,totalIncome,waterBodyPicPath,isExported,isDeleted,lat,longg,location,waterBodyId,status;

    public String getWaterBodyId() {
        return waterBodyId;
    }

    public void setWaterBodyId(String waterBodyId) {
        this.waterBodyId = waterBodyId;
    }

    public String getFromwheretraing() {
        return fromwheretraing;
    }

    public void setFromwheretraing(String fromwheretraing) {
        this.fromwheretraing = fromwheretraing;
    }

    public String getPgCode() {
        return pgCode;
    }

    public void setPgCode(String pgCode) {
        this.pgCode = pgCode;
    }

    public String getGrpMemberCode() {
        return grpMemberCode;
    }

    public void setGrpMemberCode(String grpMemberCode) {
        this.grpMemberCode = grpMemberCode;
    }

    public String getGrpCode() {
        return grpCode;
    }

    public void setGrpCode(String grpCode) {
        this.grpCode = grpCode;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getWaterBodyCode() {
        return waterBodyCode;
    }

    public void setWaterBodyCode(String waterBodyCode) {
        this.waterBodyCode = waterBodyCode;
    }

    public String getWaterBodyOwnerCode() {
        return waterBodyOwnerCode;
    }

    public void setWaterBodyOwnerCode(String waterBodyOwnerCode) {
        this.waterBodyOwnerCode = waterBodyOwnerCode;
    }

    public String getNoOfMonthAvailable() {
        return noOfMonthAvailable;
    }

    public void setNoOfMonthAvailable(String noOfMonthAvailable) {
        this.noOfMonthAvailable = noOfMonthAvailable;
    }

    public String getSelectedFishery() {
        return selectedFishery;
    }

    public void setSelectedFishery(String selectedFishery) {
        this.selectedFishery = selectedFishery;
    }

    public String getIsFisheryHappenig() {
        return isFisheryHappenig;
    }

    public void setIsFisheryHappenig(String isFisheryHappenig) {
        this.isFisheryHappenig = isFisheryHappenig;
    }

    public String getIsTrainingTaken() {
        return isTrainingTaken;
    }

    public void setIsTrainingTaken(String isTrainingTaken) {
        this.isTrainingTaken = isTrainingTaken;
    }

    public String getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(String totalArea) {
        this.totalArea = totalArea;
    }

    public String getTotalProduction() {
        return totalProduction;
    }

    public void setTotalProduction(String totalProduction) {
        this.totalProduction = totalProduction;
    }

    public String getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(String totalIncome) {
        this.totalIncome = totalIncome;
    }

    public String getWaterBodyPicPath() {
        return waterBodyPicPath;
    }

    public void setWaterBodyPicPath(String waterBodyPicPath) {
        this.waterBodyPicPath = waterBodyPicPath;
    }

    public String getIsExported() {
        return isExported;
    }

    public void setIsExported(String isExporeted) {
        this.isExported = isExported;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongg() {
        return longg;
    }

    public void setLongg(String longg) {
        this.longg = longg;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
