package com.jslpsmis.fishery.model;

public class tblIncomeExpendituremodel {
    private String incomeexpenditurecode;
    private double incomeamount;
    private String selecteddate;
    private double expenditureamount;

    public tblIncomeExpendituremodel(){

    }

    public tblIncomeExpendituremodel(String incomeexpenditurecode, double incomeamount, String selecteddate, double expenditureamount) {
        this.incomeexpenditurecode = incomeexpenditurecode;
        this.incomeamount = incomeamount;
        this.selecteddate = selecteddate;
        this.expenditureamount = expenditureamount;
    }

    public String getIncomeexpenditurecode() {
        return incomeexpenditurecode;
    }

    public void setIncomeexpenditurecode(String incomeexpenditurecode) {
        this.incomeexpenditurecode = incomeexpenditurecode;
    }

    public double getIncomeamount() {
        return incomeamount;
    }

    public void setIncomeamount(double incomeamount) {
        this.incomeamount = incomeamount;
    }

    public String getSelecteddate() {
        return selecteddate;
    }

    public void setSelecteddate(String selecteddate) {
        this.selecteddate = selecteddate;
    }

    public double getExpenditureamount() {
        return expenditureamount;
    }

    public void setExpenditureamount(double expenditureamount) {
        this.expenditureamount = expenditureamount;
    }
}
