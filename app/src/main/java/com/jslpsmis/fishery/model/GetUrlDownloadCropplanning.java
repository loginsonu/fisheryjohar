package com.jslpsmis.fishery.model;

public class GetUrlDownloadCropplanning {
    private String domain;
    private String method;
    private String PgCode;

    public GetUrlDownloadCropplanning(String domain, String method, String PgCode) {
        this.domain = domain;
        this.method = method;
        this.PgCode = PgCode;
    }

    public String getUrl(){
        String url;
        url = domain + "/" + method;
        return url;
    }
}
