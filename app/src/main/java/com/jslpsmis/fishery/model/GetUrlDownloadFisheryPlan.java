package com.jslpsmis.fishery.model;

public class GetUrlDownloadFisheryPlan {
    private String domain;
    private String method;
    private String whr;
    private String flag;


    public GetUrlDownloadFisheryPlan(String domain, String method, String whr, String flag) {
        this.domain = domain;
        this.method = method;
        this.whr = whr;
        this.flag = flag;
    }

    public String getUrl() {
        String url;
        url = domain + "/" + method;
        return url;
    }
}
