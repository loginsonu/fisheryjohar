package com.jslpsmis.fishery.model;

public class InputIndentModel {
    public String itemCode,itemName,measurementUnit,RatePerUnit,QuantityPerUnit,selectedFisheryCode,areaDependent,budgetCode;

    public String getBudgetCode() {
        return budgetCode;
    }

    public void setBudgetCode(String budgetCode) {
        this.budgetCode = budgetCode;
    }

    public String getAreaDependent() {
        return areaDependent;
    }

    public void setAreaDependent(String areaDependent) {
        this.areaDependent = areaDependent;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public String getRatePerUnit() {
        return RatePerUnit;
    }

    public void setRatePerUnit(String ratePerUnit) {
        RatePerUnit = ratePerUnit;
    }

    public String getQuantityPerUnit() {
        return QuantityPerUnit;
    }

    public void setQuantityPerUnit(String quantityPerUnit) {
        QuantityPerUnit = quantityPerUnit;
    }

    public String getSelectedFisheryCode() {
        return selectedFisheryCode;
    }

    public void setSelectedFisheryCode(String selectedFisheryCode) {
        this.selectedFisheryCode = selectedFisheryCode;
    }
}
