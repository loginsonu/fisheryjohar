package com.jslpsmis.fishery.model;

public class GetUrlDownloadInExMaster {
    private String domain;
    private String method;
    private String flag;

    public GetUrlDownloadInExMaster(String domain, String method) {
        this.domain = domain;
        this.method = method;
        this.flag = flag;
    }

    public String getUrl(){
        String url;
        url = domain+"/"+method;
        return url;
    }
}
