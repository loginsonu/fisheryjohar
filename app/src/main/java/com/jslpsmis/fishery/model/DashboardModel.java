package com.jslpsmis.fishery.model;

/**
 * Created by sonu on 4/5/2018.
 */

public class DashboardModel {
    public String name;
    private int imageIcon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(int imageIcon) {
        this.imageIcon = imageIcon;
    }

    public DashboardModel(String name, int icon){
        this.name = name;
        this.imageIcon = icon;

    }
}
