package com.jslpsmis.fishery.model;

public class BudgetModel {
    private String budgetCode;
    private String benPer;
    private String conPer;
    private String joharBank;

    public BudgetModel(String budgetCode, String benPer, String conPer, String joharBank) {
        this.budgetCode = budgetCode;
        this.benPer = benPer;
        this.conPer = conPer;
        this.joharBank = joharBank;
    }

    public String getBudgetCode() {
        return budgetCode;
    }

    public void setBudgetCode(String budgetCode) {
        this.budgetCode = budgetCode;
    }

    public String getBenPer() {
        return benPer;
    }

    public void setBenPer(String benPer) {
        this.benPer = benPer;
    }

    public String getConPer() {
        return conPer;
    }

    public void setConPer(String conPer) {
        this.conPer = conPer;
    }

    public String getJoharBank() {
        return joharBank;
    }

    public void setJoharBank(String joharBank) {
        this.joharBank = joharBank;
    }
}
