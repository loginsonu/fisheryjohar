package com.jslpsmis.fishery.model;

public class FisheryPlanModel {
    public String fisheryId,waterBodyId,selectedFishey,totalAmount,date,isExported;

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFisheryId() {
        return fisheryId;
    }

    public void setFisheryId(String fisheryId) {
        this.fisheryId = fisheryId;
    }

    public String getWaterBodyId() {
        return waterBodyId;
    }

    public void setWaterBodyId(String waterBodyId) {
        this.waterBodyId = waterBodyId;
    }

    public String getSelectedFishey() {
        return selectedFishey;
    }

    public void setSelectedFishey(String selectedFishey) {
        this.selectedFishey = selectedFishey;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIsExported() {
        return isExported;
    }

    public void setIsExported(String isExported) {
        this.isExported = isExported;
    }
}
