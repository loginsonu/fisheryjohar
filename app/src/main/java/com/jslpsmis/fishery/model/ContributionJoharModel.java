package com.jslpsmis.fishery.model;

public class ContributionJoharModel {
    public String itemname;
    public String unitcost;
    public String johar;
    public String beneficiary;
    public String convergence;

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(String unitcost) {
        this.unitcost = unitcost;
    }

    public String getJohar() {
        return johar;
    }

    public void setJohar(String johar) {
        this.johar = johar;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getConvergence() {
        return convergence;
    }

    public void setConvergence(String convergence) {
        this.convergence = convergence;
    }
}

