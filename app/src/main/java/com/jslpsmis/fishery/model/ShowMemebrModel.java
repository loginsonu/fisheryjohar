package com.jslpsmis.fishery.model;

public class ShowMemebrModel {

    private String membername;
    private String grpmemcode;
    private String grpcode;
    private String pgcode;
    private String waterbodyid;
    private String fathername;
    private String GroupName;

    public ShowMemebrModel(){

    }

    public ShowMemebrModel(String membername, String grpmemcode, String grpcode, String pgcode, String waterbodyid) {
        this.membername = membername;
        this.grpmemcode = grpmemcode;
        this.grpcode = grpcode;
        this.pgcode = pgcode;
        this.waterbodyid = waterbodyid;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getGrpmemcode() {
        return grpmemcode;
    }

    public void setGrpmemcode(String grpmemcode) {
        this.grpmemcode = grpmemcode;
    }

    public String getGrpcode() {
        return grpcode;
    }

    public void setGrpcode(String grpcode) {
        this.grpcode = grpcode;
    }

    public String getPgcode() {
        return pgcode;
    }

    public void setPgcode(String pgcode) {
        this.pgcode = pgcode;
    }

    public String getWaterbodyid() {
        return waterbodyid;
    }

    public void setWaterbodyid(String waterbodyid) {
        this.waterbodyid = waterbodyid;
    }
}
