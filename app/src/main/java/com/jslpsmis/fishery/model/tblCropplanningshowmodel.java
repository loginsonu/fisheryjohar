package com.jslpsmis.fishery.model;

public class tblCropplanningshowmodel {

    private String cropplanningcount;
    private String selecteddate;
    private String cropplanningtype;
    private String incomeamount;
    private String expenditureamount;
    private String profitloss;


    public tblCropplanningshowmodel(){

    }

    public tblCropplanningshowmodel(String cropplanningcount, String selecteddate, String cropplanningtype, String incomeamount, String expenditureamount, String profitloss) {
        this.cropplanningcount = cropplanningcount;
        this.selecteddate = selecteddate;
        this.cropplanningtype = cropplanningtype;
        this.incomeamount = incomeamount;
        this.expenditureamount = expenditureamount;
        this.profitloss = profitloss;
    }

    public String getCropplanningcount() {
        return cropplanningcount;
    }

    public void setCropplanningcount(String cropplanningcount) {
        this.cropplanningcount = cropplanningcount;
    }

    public String getSelecteddate() {
        return selecteddate;
    }

    public void setSelecteddate(String selecteddate) {
        this.selecteddate = selecteddate;
    }

    public String getCropplanningtype() {
        return cropplanningtype;
    }

    public void setCropplanningtype(String cropplanningtype) {
        this.cropplanningtype = cropplanningtype;
    }

    public String getIncomeamount() {
        return incomeamount;
    }

    public void setIncomeamount(String incomeamount) {
        this.incomeamount = incomeamount;
    }

    public String getExpenditureamount() {
        return expenditureamount;
    }

    public void setExpenditureamount(String expenditureamount) {
        this.expenditureamount = expenditureamount;
    }

    public String getProfitloss() {
        return profitloss;
    }

    public void setProfitloss(String profitloss) {
        this.profitloss = profitloss;
    }
}
