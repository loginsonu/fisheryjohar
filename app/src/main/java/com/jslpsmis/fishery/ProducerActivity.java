package com.jslpsmis.fishery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jslpsmis.fishery.adapter.ProducerActivityAdapter;
import com.jslpsmis.fishery.commonclass.CheckConnectivity;
import com.jslpsmis.fishery.commonclass.DownloadDataFromServer;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.Cropplanningmemberstbl;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.GetUrlDownloadCropplanning;
import com.jslpsmis.fishery.model.GetUrlDownloadIIncomeEx;
import com.jslpsmis.fishery.model.GetUrlDownloadInExMaster;
import com.jslpsmis.fishery.model.ProducerActivityModel;
import com.jslpsmis.fishery.service.VolleyString;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProducerActivity extends AppCompatActivity implements VolleyString.VolleyListner{

    RecyclerView recyclerView;
    ProducerActivityAdapter aAdapter;
    ArrayList<ProducerActivityModel> list;
    Dataprovider dataprovider;
    public static String cadreId,firstLogin;
    EditText search;
    ImageView log_out ;
    TextView listCount;
    MDMSharedPreference mdmSharedPreference;
    ProgressDialog progress;
    JSONArray arrayWaterBody = null;
    JSONArray arrayFisheryPlan=null;
    ArrayList<String> getpgname;
    List<String> pgList;
    String pgCSV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_producer);
        
        init();
    }

    private void init() {
        dataprovider = new Dataprovider(this);
        recyclerView = (RecyclerView) findViewById(R.id.list_producer_group_activity_producer);
        listCount = (TextView) findViewById(R.id.listCountProducerActivity);
        search = (EditText) findViewById(R.id.editText);
        log_out = findViewById(R.id.log_out_producer_activity);
        mdmSharedPreference = MDMSharedPreference.getInstance(this);

        Intent intent = getIntent();
        cadreId = intent.getStringExtra("cadreId");
        firstLogin = intent.getStringExtra("firstLogin");

        populatingRecylerView();
        pgList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            pgList.add(list.get(i).getPgCode());
        }

        pgCSV = pgList.toString().replace("[", "").replace("]", "")
                .replace(", ", ",");

        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(ProducerActivity.this)){
            if(firstLogin!=null) {
                DialogShowDownload();
                // download producer members
                new DownloadDataFromServer(ProducerActivity.this, LoginActivity.userName, LoginActivity.password, AppConstant.AMM_M,"producergrpmemtb");

                // download waterbody type
                new DownloadDataFromServer(ProducerActivity.this,"","", AppConstant.tblWaterBodyType,"tblWaterBodyType");

                //WaterBodyOwnerApi
                new DownloadDataFromServer(ProducerActivity.this,"","", AppConstant.tblmstwaterbodyowner,"tblWaterBodyOwner");

                //download selected fishery
                new DownloadDataFromServer(ProducerActivity.this,"","", AppConstant.tblSelectedFishery,"tblSelectedFishery");

                //downloaditem Rrequired
                new DownloadDataFromServer(ProducerActivity.this,"","", AppConstant.tblMstItemRequiredFishBreeding,"tblItemRequiredFishBreeding");

                //download item rate map table
                new DownloadDataFromServer(ProducerActivity.this,"","", AppConstant.tblRateFishBreedingPerUnit,"tblRateFishBreedingPerUnit");

                //download water body data
                new DownloadDataFromServer(ProducerActivity.this,pgCSV,"", AppConstant.tblAddWaterBody,"tblAddWaterBody");

                //download fishery plan
                new DownloadDataFromServer(ProducerActivity.this,pgCSV,"", AppConstant.tbltracFisheryPlan,"tbltracFisheryPlan");

                //Download IncomeExpenditureMaster table
                DownloadIncomeExpenditureMaster();

                //Download IncomeExpenditure table
                DownloadIncomeExpenditure(cadreId);

                //Download CropPlanning Memebers table
                DownloadCropPlanningMemebers(cadreId);

                DialogClose();
            }
        }

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void populatingRecylerView() {
        list= dataprovider.getProducerList(cadreId);
        getpgname=new ArrayList<>();

        for(int i=0;i<list.size();i++) {
            getpgname.add(list.get(i).getPgName());
        }

        if(list.size()>0){
            listCount.setText(getString(R.string.list_count)+ list.size()+"");
            aAdapter = new ProducerActivityAdapter(this,list,getpgname);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);

            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String text = search.getText().toString().toLowerCase(Locale.getDefault());
                    aAdapter.filter(text);
                }
            });
        }
    }

/*
    @Override
    protected void onResume() {
        super.onResume();
        populatingRecylerView();
    }
*/

    @Override
    public void onResponseSuccess(String tableIndentifier, String result) {
        populatingRecylerView();
        if(tableIndentifier.equals(AppConstant.Download_IncomeExpenditureMasterstatus)){
            if(result.equals("[]")){
                new StyleableToast
                        .Builder(this)
                        .text("No data found for IncomeExpenditureMaster table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object=jsonArray.getJSONObject(i);

                        int Incomeexpenditureid=object.getInt("Incomeexpenditureid");
                        String Incomeexpenditurename=object.optString("Incomeexpenditurename");
                        String Incomeexpenditureflag=object.optString("Incomeexpenditureflag");

                        IncomeExpenditureMaster incomeExpenditureMaster=new IncomeExpenditureMaster(Incomeexpenditureid,Incomeexpenditurename,Incomeexpenditureflag,"1");
                        incomeExpenditureMaster.save();
                    }
                    new StyleableToast
                            .Builder(this)
                            .text("Income Expenditure Master Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if(tableIndentifier.equals(AppConstant.PGFisheryIncomeExpenditure_Getdatastatus)){
            if(result.equals("[]")){
                new StyleableToast
                        .Builder(this)
                        .text("No data found for IncomeExpenditure table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object=jsonArray.getJSONObject(i);

                        int incomeexpenditurecode=object.getInt("incomeexpenditurecode");
                        String incomeexpenditureuuid=object.optString("incomeexpenditureuuid");
                        double totalamount=object.optDouble("totalamount");
                        String Pgcode=object.optString("Pgcode");
                        String waterbodyid=object.optString("waterbodyid");
                        String selecteddate=object.optString("selecteddate");
                        String flag=object.getString("flag");
                        double rate=object.getDouble("rate");
                        String unit=object.getString("unit");
                        double quantity=object.getDouble("quantity");
                        String cropplanningid=object.getString("cropplanningid");

                        tblIncomeExpenditure tblIncomeExpenditure=new tblIncomeExpenditure(
                                incomeexpenditureuuid,String.valueOf(incomeexpenditurecode),totalamount,Pgcode,
                                waterbodyid,selecteddate,flag,rate,unit,quantity,cropplanningid,"1","1","","");

                        tblIncomeExpenditure.save();
                    }
                    new StyleableToast
                            .Builder(this)
                            .text("Income Expenditure Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if(tableIndentifier.equals(AppConstant.Download_FisherySelectedMembers_DataInsertstatus)){
            if(result.equals("[]")){
                new StyleableToast
                        .Builder(this)
                        .text("No data found for FisherySelectedMembers table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object=jsonArray.getJSONObject(i);

                        String selectedmemberuuid=object.optString("selectedmemberuuid");
                        String Membername=object.optString("Membername");
                        String pgcode=object.optString("pgcode");
                        String grpmemcode=object.optString("grpmemcode");
                        String grpcode=object.getString("grpcode");
                        String waterbodyid=object.getString("waterbodyid");
                        String fisheryplanid=object.getString("fisheryplanid");

                        Cropplanningmemberstbl cropplanningmemberstbl=new Cropplanningmemberstbl(selectedmemberuuid, Membername, pgcode, grpmemcode, grpcode, "1", waterbodyid, fisheryplanid, "", "");

                        cropplanningmemberstbl.save();
                    }
                    new StyleableToast
                            .Builder(this)
                            .text("FisherySelectedMembers Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onResponseFailure(String tableIdentifier) {
        progress.dismiss();
        new StyleableToast
                .Builder(this)
                .text("server error,Please check internet Connection")
                .iconStart(R.drawable.wrong_icon_white)
                .textColor(Color.WHITE)
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();
    }

    public void DownloadIncomeExpenditureMaster() {
        IncomeExpenditureMaster.deleteAll(IncomeExpenditureMaster.class);
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadInExMaster(AppConstant.domain,
                    AppConstant.DownloadIncomeExpenditureMaster_GetData).getUrl(),
                    AppConstant.Download_IncomeExpenditureMasterstatus,this).postGetIncomeExpenditureMaster("");

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void DownloadIncomeExpenditure(String data) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadIIncomeEx(AppConstant.domain,
                    AppConstant.Upload_PGFisheryIncomeExpenditure_Getdata).getUrl(),
                    AppConstant.PGFisheryIncomeExpenditure_Getdatastatus,this).postGetIncomeExpenditure(data);

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void DownloadCropPlanningMemebers(String data) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadCropplanning(AppConstant.domain,
                    AppConstant.Download_FisherySelectedMembers_GetData,data).getUrl(),
                    AppConstant.Download_FisherySelectedMembers_DataInsertstatus,this).postCropPlanningMemebers(data);

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAffinity(ProducerActivity.this);
        finish();
    }

    private void DialogShowDownload() {
        progress= new ProgressDialog(ProducerActivity.this);
        progress.setMessage("कृपया प्रतीक्षा करें");
        progress.setCancelable(false);
        progress.show();
    }

    private void DialogClose(){
        if(progress!=null){
            progress.dismiss();
        }
    }
}
