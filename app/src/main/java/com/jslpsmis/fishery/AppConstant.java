package com.jslpsmis.fishery;

/**
 * Created by sonu on 4/6/2018.
 */

public class AppConstant {

    public static final String domain="http://johar.swalekha.in/Webservices/WebService.asmx";
    //this is jica url and should be removed in future
    public static final String domainJohar ="http://johar.swalekha.in/Webservices/WebService.asmx";
    public static final String namespace="http://tempuri.org/";

    //Methods download
    public static String DownloadMethodJICA="Download_Johar_TabletData_Service";
    //this should be removed since it is in jica
    public static String DownloadMethodJOHAR="Download_Johar_TabletData_Service";
    public static String DownloadIncomeExpenditureMaster_GetData="IncomeExpenditureMaster_GetData";
    public static String Download_FisherySelectedMembers_GetData="FisherySelectedMembers_GetData";

    //Methods Upload
     public static String Upload_tblAddWaterBody = "Upload_tblAddWaterBody";
     public static String Upload_tbltracFisheryPlan ="fisherytrackplan_UploadData";//"Upload_tbltracFisheryPlan";
     public static String Upload_PGFisheryIncomeExpenditure_DataInsert="PGFisheryIncomeExpenditure_DataInsert";
     public static String Upload_PGFisheryIncomeExpenditure_Getdata="PGFisheryIncomeExpenditure_Getdata";
     public static String Upload_FisherySelectedMembers_DataInsert="FisherySelectedMembers_DataInsert";

    //Flags
    public static String AMM_G = "AMM_G";
    public static String AMM_M = "AMM_M";
    public static String tblWaterBodyType = "tblWaterBodyType";
    public static String tblmstwaterbodyowner = "tblmstwaterbodyowner";
    public static String tblSelectedFishery = "tblSelectedFishery";
    public static String tblMstItemRequiredFishBreeding = "tblMstItemRequiredFishBreeding";
    public static String tblRateFishBreedingPerUnit = "tblRateFishBreedingPerUnit";
    public static String tblAddWaterBody = "tblAddWaterBody";
    public static String tbltracFisheryPlan ="tbltracFisheryPlan";

    //table indentifier
    public static final String Upload_tblAddWaterBodystatus ="1";
    public static final String UploadIncomeExpendituresatus="2";
    public static final String Download_IncomeExpenditureMasterstatus="3";
    public static String PGFisheryIncomeExpenditure_Getdatastatus="4";
    public static String Upload_FisherySelectedMembers_DataInsertstatus="5";
    public static String Download_FisherySelectedMembers_DataInsertstatus="6";
    public static final String Upload_tbltracFisheryPlanstatus ="7";
    public static final String Download_tblAddWaterBody ="8";
    public static final String Download_tbltracFisheryPlan ="9";
}
