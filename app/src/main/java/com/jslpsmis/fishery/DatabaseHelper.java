package com.jslpsmis.fishery;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabaseHelper extends SQLiteOpenHelper
{
	private static SQLiteDatabase sqliteDb;
    private static DatabaseHelper instance;
    private static final int DATABASE_VERSION = 1;

	// the default database path is : /data/data/pkgNameOfYourApplication/databases/
    private static String DB_PATH = "";
    private static String DB_PATH_PREFIX = "/data/data/";
    private static String DB_PATH_SUFFIX = "/databases/";
    private static final String TAG = "com.example.sonu.farmernew.DatabaseHelper";
    private Context context;

	/**
     * Constructor
     * @param aContext : application context
     * @param name : database name
     * @param factory : Cursor Factory
     * @param version : DB version
     */

	public DatabaseHelper(Context aContext, String name, CursorFactory factory, int version) {
		super(aContext, name, factory, version);
		// TODO Auto-generated constructor stub
		this.context = aContext;
		//Log.i(TAG, "Create or Open Database : " + name);
	}
 
	/**
	 * Initialize method
	 * @param aContext : application context
	 * @param databaseName : database name
	 */
	private static void initialize(Context aContext, String databaseName){
		if (instance == null){
			//Check there is an original copy of the database in the assets directory of the application
			if(!checkDatabase(aContext, databaseName)){
				// if not then copy database from assets directory
				try{
					copyDatabase(aContext, databaseName);
				}catch(Exception e){
					//Log.e(TAG, "Database does not exists and there is no copy of database in asset directory");
				}
			}
			// Create instance of database
			//Log.i(TAG, "Creating database instance ....");
			instance = new DatabaseHelper(aContext, databaseName, null, DATABASE_VERSION);
			sqliteDb = instance.getWritableDatabase();
			//Log.i(TAG, "Database instance created!");
		}
	}
	
	/**
	 * static method for getting singleton instance
	 * @param aContext : application context
	 * @param databaseName : database name
	 * @return singleton instance
	 */
	public static final DatabaseHelper getInstance(Context aContext, String databaseName){
		initialize(aContext, databaseName);
		return instance;
	}
	
	/**
	 * Method to get database instance
	 * @return SQLiteDatabase instance
	 */
	public SQLiteDatabase getDatabase(){
		return sqliteDb;
	}
	
    /**
     * Method for Copy the database from asset directory to application's data directory
     * @param databaseName : database name
     * @throws IOException : exception if file does not exists
     */
    public void copyDatabase(String databaseName) throws IOException {
    	copyDatabase(context, databaseName);
    }
	
	/**
	 * Copy database from assets directory to application's data directory
	 * @param aContext : application context
	 * @param databaseName : database name
	 * @throws : IOException, if file does not exists
	 */
	private static void copyDatabase(Context aContext, String databaseName)throws IOException {
		// Open your local database as the input stream
        InputStream myInput = aContext.getAssets().open(databaseName);
        // Path to the just created empty db
        String outFileName = getDatabasePath(aContext, databaseName);
        // Check if the path exists or not, if not then create
        String dataDir = DB_PATH_PREFIX + aContext.getPackageName() + DB_PATH_SUFFIX;
        File dbFile = new File(dataDir);
        if(!dbFile.exists()){
        	dbFile.mkdir();
        }
        // Copy the file into data directory
        //Log.i(TAG, "Copying local database into : " + outFileName );
        
        // Open the empty database as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        
        // transfer bytes from the input file to the output file
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
        //Log.i(TAG, "Database (" + databaseName + ") copied successfully!");
	}
	
    /**
     * Method to check if database exists in application's data directory
     * @param databaseName : database name
     * @return : boolean (true if exists)
     */
    public boolean checkDatabase(String databaseName) {
            return checkDatabase(context, databaseName);
    }

	/**
	 * check original copy of the database 
	 * @param aContext : application context
	 * @param databaseName : database name
	 * @return : true ? false
	 */
	private static boolean checkDatabase(Context aContext, String databaseName) {
		SQLiteDatabase checkDB = null;

        try {
        	String myPath = getDatabasePath(aContext, databaseName);
        	//Log.i(TAG, "Trying to conntect to : " + myPath);
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            //Log.i(TAG, "Database " + databaseName + " found!");
            checkDB.close();
        } catch (SQLiteException e) {
            //Log.i(TAG, "Database " + databaseName + " does not exists!");
        }

        return checkDB != null ? true : false;
	}
	
	/***
     * Method that returns database path in the application's data directory
     * @param databaseName : database name
     * @return : complete path
     */
    public String getDatabasePath(String databaseName) {
            return getDatabasePath(context, databaseName);
    }

    /***
     * Static Method that returns database path in the application's data directory
     * @param aContext : application context
     * @param databaseName : database name
     * @return : complete path
     */
    private static String getDatabasePath(Context aContext, String databaseName) {
    		DB_PATH = DB_PATH_PREFIX + aContext.getPackageName() + DB_PATH_SUFFIX
            + databaseName;
            return DB_PATH;
    }

    @Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//Log.i(TAG, "OnCreate :: Called ");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//Log.i(TAG, "OnCreate :: Upgrade ");
		
	}
}
