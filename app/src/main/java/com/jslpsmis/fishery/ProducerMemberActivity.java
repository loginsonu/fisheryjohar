package com.jslpsmis.fishery;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.adapter.ProducerGroupMemberActivityAdapter;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import java.util.ArrayList;
import java.util.Locale;

public class ProducerMemberActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProducerGroupMemberActivityAdapter aAdapter;
    ArrayList<ProducerGroupMemberActivityModel> list;
    Dataprovider dataprovider;
    public static String pgName,pgCode,flag,grpname;
    EditText search;
    ImageView backButton;
    TextView producerGroupName;
    TextView listCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_producer_member);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        pgCode = intent.getStringExtra("pgCode");
        pgName = intent.getStringExtra("pgName");
        flag = intent.getStringExtra("flag");
        grpname=intent.getStringExtra("grpname");

        backButton = (ImageView) findViewById(R.id.backButtonProducerGroupMemberActivity);
        dataprovider = new Dataprovider(this);
        recyclerView = (RecyclerView) findViewById(R.id.list_producer_group_member_activity_producerGroup);
        search = (EditText) findViewById(R.id.editText);
        producerGroupName = findViewById(R.id.producerGroupName);
        listCount = (TextView) findViewById(R.id.listCountProducerMemeberActivity);

        producerGroupName.setText(pgName);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        populatingRecyclerView();
     }

    private void populatingRecyclerView() {
        ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
        list= dataprovider.getProducerGrpMemList(pgCode);

        if(list.size()>0){
            listCount.setText(getString(R.string.list_count)+ list.size()+"");
            aAdapter = new ProducerGroupMemberActivityAdapter(this,list,flag);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {
                    String text = search.getText().toString().toLowerCase(Locale.getDefault());
                    aAdapter.filter(text);
                }
            });
        }
    }
}
