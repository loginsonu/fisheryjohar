package com.jslpsmis.fishery;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.commonclass.CheckConnectivity;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.DownloadDataFromServer;
import com.jslpsmis.fishery.commonclass.Loader;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.Cropplanningmemberstbl;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.GetUrlDownloadCropplanning;
import com.jslpsmis.fishery.model.GetUrlDownloadIIncomeEx;
import com.jslpsmis.fishery.model.GetUrlDownloadInExMaster;
import com.jslpsmis.fishery.model.LoginModel;
import com.jslpsmis.fishery.model.ProducerActivityModel;
import com.jslpsmis.fishery.service.VolleyString;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

import se.simbio.encryption.Encryption;

public class LoginActivity extends AppCompatActivity implements VolleyString.VolleyListner {
    EditText etUserName;
    EditText etPassword;
    Button btnLogin;
    CheckBox checkBox;
    Button refresh;
    Boolean isPasswordshowing=false;
    Dataprovider dataprovider;
    String userNameFromDatabase;
    String cadreIdFromDatabase;
    MDMSharedPreference mdmSharedPreference;
    public static View view;
    String  cadreID;
    public static String userName,password;
    int PERMISSION_ALL = 1;
    SchemaGenerator schemaGenerator;

    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA, Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

    TextView tvVersion;
    JSONArray arrayWaterBody = null;
    JSONArray arrayFisheryPlan=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        /******asking for permission for devices >= marsmellow ********/
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        init();
    }

    private void init() {
        dataprovider = new Dataprovider(this);
        mdmSharedPreference = MDMSharedPreference.getInstance(this);

        SugarContext.init(getApplicationContext());
        schemaGenerator = new SchemaGenerator(getApplicationContext());
        schemaGenerator.createDatabase(new SugarDb(getApplicationContext()).getDB());

        tvVersion = findViewById(R.id.textView42);
        etUserName = (EditText) findViewById(R.id.et_username_activityLogin);
        etPassword = (EditText) findViewById(R.id.et_password_activityLogin);
        btnLogin = (Button) findViewById(R.id.btn_activityLogin);
        refresh = (Button) findViewById(R.id.btn_refreshLogin);
        view = findViewById(android.R.id.content);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        refresh.setVisibility(View.GONE);
        etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.password_view_icon, 0);

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //  etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        if(!isPasswordshowing){
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.password_hide_icon, 0);
                            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            etPassword.setSelection(etPassword.length());
                            isPasswordshowing = true;
                        }else{
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.password_view_icon, 0);
                            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            etPassword.setSelection(etPassword.length());
                            isPasswordshowing=false;
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            try {
                tvVersion.setText("Version:" + version);
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(!mdmSharedPreference.getString("username").equals("")){
            etUserName.setText(mdmSharedPreference.getString("username"));
            etPassword.setText(mdmSharedPreference.getString("password"));
            checkBox.setChecked(true);
        }

        if(!mdmSharedPreference.getString("firstTimeRun").equals("")){
            refresh.setVisibility(View.VISIBLE);
        }

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation("update");
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation("insert");
            }
        });
    }

    private void validation(String action) {
        if(etUserName.getText().toString().equals("")||etPassword.getText().toString().equals("")){
            if(etUserName.getText().toString().equals("")){
                String msg = getString(R.string.username_blank);
                alert(getString(R.string.no_user_name),msg);
            }
            else if(etPassword.getText().toString().equals("")){
                String msg = getString(R.string.password_blank);
                alert(getString(R.string.no_password),msg);
            }
        }else{
            boolean check = checkBox.isChecked();
            if(check){
                mdmSharedPreference.putString("username",etUserName.getText().toString());
                mdmSharedPreference.putString("password",etPassword.getText().toString());
            }
            else {
                mdmSharedPreference.putString("username","");
                mdmSharedPreference.putString("password","");
            }
            String normalPassword = etPassword.getText().toString();
            userName = etUserName.getText().toString();
            password = normalPassword;
            String passwordEncrypted = encrptionMethod(normalPassword,"encrypt");
            String loginquery = "Select contactno from logintb where  contactno='" + etUserName.getText().toString() + "' and password='" + passwordEncrypted + "'";
            userNameFromDatabase = dataprovider.getRecord(loginquery);

            String loginqueryCadreId = "Select cadreid from logintb where contactno='" + etUserName.getText().toString() + "' and password='" + passwordEncrypted + "'";
            cadreIdFromDatabase = dataprovider.getRecord(loginqueryCadreId);
            if(!userNameFromDatabase.equals("")){
                if(action.equals("insert")) {
                    Intent intent = new Intent(LoginActivity.this, ProducerActivity.class);
                    intent.putExtra("cadreId", cadreIdFromDatabase);
                    startActivity(intent);
                    finish();
                }
                else{
                    CheckConnectivity checkConnectivity = new CheckConnectivity();
                    if(checkConnectivity.CheckConnection(LoginActivity.this)){
                       new LoginApi(action).execute();
                        Constant.action = "update";
//                        startService(new Intent(LoginActivity.this, DownloadMasterData.class));
                        new DownloadDataFromServer(LoginActivity.this, LoginActivity.userName, LoginActivity.password, AppConstant.AMM_M,"producergrpmemtb");

                        // download waterbody type
                        new DownloadDataFromServer(LoginActivity.this,"","", AppConstant.tblWaterBodyType,"tblWaterBodyType");

                        //WaterBodyOwnerApi
                        new DownloadDataFromServer(LoginActivity.this,"","", AppConstant.tblmstwaterbodyowner,"tblWaterBodyOwner");

                        //download selected fishery
                        new DownloadDataFromServer(LoginActivity.this,"","", AppConstant.tblSelectedFishery,"tblSelectedFishery");

                        //downloaditem Rrequired
                        new DownloadDataFromServer(LoginActivity.this,"","", AppConstant.tblMstItemRequiredFishBreeding,"tblItemRequiredFishBreeding");

                        //download item rate map table
                        new DownloadDataFromServer(LoginActivity.this,"","", AppConstant.tblRateFishBreedingPerUnit,"tblRateFishBreedingPerUnit");

                        //download water body data
                        new DownloadDataFromServer(LoginActivity.this,LoginActivity.userName,"", AppConstant.tblAddWaterBody,"tblAddWaterBody");

                        FisheryPlantbl.deleteAll(FisheryPlantbl.class);
                        //download fishery plan
                        new DownloadDataFromServer(LoginActivity.this,LoginActivity.userName,"", AppConstant.tbltracFisheryPlan,"tbltracFisheryPlan");

                        //Download IncomeExpenditureMaster table
                        DownloadIncomeExpenditureMaster();

                        tblIncomeExpenditure.deleteAll(tblIncomeExpenditure.class);
                        //Download IncomeExpenditure table
                        DownloadIncomeExpenditure(LoginActivity.userName);

                        Cropplanningmemberstbl.deleteAll(Cropplanningmemberstbl.class);
                        //Download CropPlanningMemebers table
                        DownloadCropPlanningMemebers(LoginActivity.userName);

                    }else{
                        alert(getString(R.string.internet_error),getString(R.string.internet_msg));
                    }
                }
            }
            else{
                CheckConnectivity checkConnectivity = new CheckConnectivity();
                if(checkConnectivity.CheckConnection(LoginActivity.this)){
                   new LoginApi(action).execute();
                    //startService(new Intent(LoginActivity.this, DownloadMasterData.class));
                }else{
                    alert(getString(R.string.internet_error),getString(R.string.internet_msg));
                }
            }
        }
    }

    @Override
    public void onResponseSuccess(String tableIndentifier, String result) {

        if(tableIndentifier.equals(AppConstant.Download_IncomeExpenditureMasterstatus)){
            if(result.equals("[]")){
                new StyleableToast
                        .Builder(this)
                        .text("No data found for IncomeExpenditureMaster table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(result);
                     for(int i=0;i<jsonArray.length();i++){
                         JSONObject object=jsonArray.getJSONObject(i);

                         int Incomeexpenditureid=object.getInt("Incomeexpenditureid");
                         String Incomeexpenditurename=object.optString("Incomeexpenditurename");
                         String Incomeexpenditureflag=object.optString("Incomeexpenditureflag");

                         IncomeExpenditureMaster incomeExpenditureMaster=new IncomeExpenditureMaster(Incomeexpenditureid,Incomeexpenditurename,Incomeexpenditureflag,"1");
                           incomeExpenditureMaster.save();
                     }

                    new StyleableToast
                            .Builder(this)
                            .text("Income Expenditure Master Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if(tableIndentifier.equals(AppConstant.PGFisheryIncomeExpenditure_Getdatastatus)) {
            if (result.equals("[]")) {
                new StyleableToast
                        .Builder(this)
                        .text("No data found for IncomeExpenditure table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            } else {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);

                        int incomeexpenditurecode = object.getInt("incomeexpenditurecode");
                        String incomeexpenditureuuid = object.optString("incomeexpenditureuuid");
                        double totalamount = object.optDouble("totalamount");
                        String Pgcode = object.optString("Pgcode");
                        String waterbodyid = object.optString("waterbodyid");
                        String selecteddate = object.optString("selecteddate");
                        String flag = object.getString("flag");
                        double rate = object.getDouble("rate");
                        String unit = object.getString("unit");
                        double quantity = object.getDouble("quantity");
                        String cropplanningid = object.getString("cropplanningid");

                        tblIncomeExpenditure tblIncomeExpenditure = new tblIncomeExpenditure(
                                incomeexpenditureuuid, String.valueOf(incomeexpenditurecode), totalamount, Pgcode,
                                waterbodyid, selecteddate, flag, rate, unit, quantity, cropplanningid, "1", "1", "", "");

                        tblIncomeExpenditure.save();
                    }
                    new StyleableToast
                            .Builder(this)
                            .text("Income Expenditure Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
           if(tableIndentifier.equals(AppConstant.Download_FisherySelectedMembers_DataInsertstatus)){
            if(result.equals("[]")){
                new StyleableToast
                        .Builder(this)
                        .text("No data found for FisherySelectedMembers table download")
                        .iconStart(R.drawable.wrong_icon_white)
                        .textColor(Color.WHITE)
                        .backgroundColor(getResources().getColor(R.color.colorPrimary))
                        .show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object=jsonArray.getJSONObject(i);

                        String selectedmemberuuid=object.optString("selectedmemberuuid");
                        String Membername=object.optString("Membername");
                        String pgcode=object.optString("pgcode");
                        String grpmemcode=object.optString("grpmemcode");
                        String grpcode=object.getString("grpcode");
                        String waterbodyid=object.getString("waterbodyid");
                        String fisheryplanid=object.getString("fisheryplanid");

                        Cropplanningmemberstbl cropplanningmemberstbl=new Cropplanningmemberstbl(selectedmemberuuid, Membername, pgcode, grpmemcode, grpcode, "1", waterbodyid, fisheryplanid, "", "");

                        cropplanningmemberstbl.save();
                    }
                    new StyleableToast
                            .Builder(this)
                            .text("FisherySelectedMembers Downloaded")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onResponseFailure(String tableIdentifier) {
        new StyleableToast
                .Builder(this)
                .text("server error,Please check internet Connection")
                .iconStart(R.drawable.wrong_icon_white)
                .textColor(Color.WHITE)
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();
    }

    public void DownloadIncomeExpenditureMaster() {
        IncomeExpenditureMaster.deleteAll(IncomeExpenditureMaster.class);
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadInExMaster(AppConstant.domain,
                    AppConstant.DownloadIncomeExpenditureMaster_GetData).getUrl(),
                    AppConstant.Download_IncomeExpenditureMasterstatus,this).postGetIncomeExpenditureMaster("");

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void DownloadIncomeExpenditure(String data) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadIIncomeEx(AppConstant.domain,
                    AppConstant.Upload_PGFisheryIncomeExpenditure_Getdata).getUrl(),
                    AppConstant.PGFisheryIncomeExpenditure_Getdatastatus,this).postGetIncomeExpenditure(data);

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void DownloadCropPlanningMemebers(String data) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            RequestQueue mRequestQueue;
            StringRequest mStringRequest;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(new GetUrlDownloadCropplanning(AppConstant.domain,
                    AppConstant.Download_FisherySelectedMembers_GetData,data).getUrl(),
                    AppConstant.Download_FisherySelectedMembers_DataInsertstatus,this).postCropPlanningMemebers(data);

            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }


    /*****************Login api ************************/
    public class LoginApi extends AsyncTask<String,Void,String> {
        SoapPrimitive resultString;
        JSONArray array = null;
        String action;
        ArrayList<LoginModel> listLogin = new ArrayList<>();
        String resultt = "";
        String pgName,pgMemName,shg;
        ArrayList<ProducerActivityModel> listProducer = new ArrayList<>();

        public LoginApi(String action){
            this.action = action;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            new Loader(LoginActivity.this, view);

            }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                JSONObject jsonObj = new JSONObject(result);
                array = jsonObj.getJSONArray("Master");

                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        if(i == 0) {
                            //deleting pld data when new user logged In
                            dataprovider.deleteData("logintb");
                            dataprovider.deleteData("producergrpmemtb");
                            dataprovider.deleteData("producergrptb");
                            dataprovider.deleteData("tblAddWaterBody");
                            dataprovider.deleteData("tblFisheryPlan");
                            dataprovider.deleteData("tblItemRequiredFishBreeding");
                            dataprovider.deleteData("tblRateFishBreedingPerUnit");
                            dataprovider.deleteData("tblSelectedFishery");
                            dataprovider.deleteData("tblWaterBodyOwner");
                            dataprovider.deleteData("tblWaterBodyType");

                            LoginModel model = new LoginModel();
                            model.setDistrictCode(object.optString("DistrictCode"));
                            model.setBlockCode(object.optString("BlockCode"));
                            model.setCadreId(object.optString("CadreId"));
                            cadreID = object.optString("CadreId");
                            mdmSharedPreference.putString("cadre",cadreID);
                            model.setCadreType(object.optString("CadreType"));
                            model.setUserId(object.optString("UserID"));
                            model.setPassword(object.optString("Password"));
                            model.setContactNumber(object.optString("Conact_No"));
                            model.setCadreName(object.optString("CadreName"));
                            model.setClusterCode(object.optString("ClusterCode"));
                            listLogin.add(model);
                            String passwordNormal = object.optString("Password");
                            String passwordEncrypted = encrptionMethod(passwordNormal,"encrypt");

                            if(action.equals("insert")) {

                                boolean insert = dataprovider.saveLoginData(object.optString("Conact_No"), passwordEncrypted
                                        , object.optString("ClusterCode"), object.optString("DistrictCode"), object.optString("BlockCode")
                                        , object.optString("CadreId")
                                        , object.optString("CadreName"), object.optString("CadreType"), object.optString("UserID"), action);
                            }else{
                                boolean delete = dataprovider.saveLoginData(object.optString("Conact_No"), object.optString("Password")
                                        , object.optString("ClusterCode"), object.optString("DistrictCode"), object.optString("BlockCode")
                                        , object.optString("CadreId")
                                        , object.optString("CadreName"), object.optString("CadreType"), object.optString("UserID"), action);
                                boolean insert1 = dataprovider.saveLoginData(object.optString("Conact_No"),passwordEncrypted
                                        , object.optString("ClusterCode"), object.optString("DistrictCode"), object.optString("BlockCode")
                                        , object.optString("CadreId")
                                        , object.optString("CadreName"), object.optString("CadreType"), object.optString("UserID"), "insert");
                            }
                        }

                        ProducerActivityModel model1 = new ProducerActivityModel();
                        model1.setDistrictCode(object.optString("DistrictCode"));
                        model1.setBlockCode(object.optString("BlockCode"));
                        model1.setClusterCode(object.optString("ClusterCode"));
                        model1.setPgCode(object.optString("PGCode"));
                        pgName = object.optString("PGName");

                        model1.setCadreId(object.optString("CadreId"));
                        listProducer.add(model1);
                        if(action.equals("insert")) {
                            boolean check1 = dataprovider.saveProducerGroupData(object.optString("PGCode")
                                    , pgName, object.optString("ClusterCode"), object.optString("DistrictCode")
                                    , object.optString("BlockCode"), object.optString("CadreId"), action);
                        }else{
                            if(i==0){
                                boolean check1 = dataprovider.saveProducerGroupData(object.optString("PGCode")
                                        , pgName, object.optString("ClusterCode"), object.optString("DistrictCode")
                                        , object.optString("BlockCode"), object.optString("CadreId"), action);
                            }
                            boolean check1 = dataprovider.saveProducerGroupData(object.optString("PGCode")
                                    , pgName, object.optString("ClusterCode"), object.optString("DistrictCode")
                                    , object.optString("BlockCode"), object.optString("CadreId"), "insert");
                        }
                    }
                    if(listLogin.size()>0) {
                        if(action.equals("insert")){
                            mdmSharedPreference.putString("firstTimeRun", "no");
                            Intent intent = new Intent(LoginActivity.this, ProducerActivity.class);
                            intent.putExtra("cadreId", cadreID);
                            intent.putExtra("firstLogin","true");
                            startActivity(intent);
                            finish();
                        }
                        else{
                            alert1(getString(R.string.success),getString(R.string.success_msg));
                            Loader.popup.dismiss();
                        }
                    }else{
                        String msg = getString(R.string.username_wrong);
                        alert(msg,getString(R.string.correct_username));
                        Loader.popup.dismiss();
                    }
                }else{
                    String msg = getString(R.string.username_wrong);
                    alert(msg,getString(R.string.correct_username));
                    Loader.popup.dismiss();
                }
            }catch (Exception e){

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            SoapObject request = new SoapObject(AppConstant.namespace, AppConstant.DownloadMethodJICA);

            request.addProperty("whr", etUserName.getText().toString());
            request.addProperty("flag", AppConstant.AMM_G);
            request.addProperty("whr1", etPassword.getText().toString());

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);

            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(AppConstant.domain, 320000);

            try{
                httpTransport.call(AppConstant.namespace+ AppConstant.DownloadMethodJICA, envelope);
                resultString = (SoapPrimitive) envelope.getResponse();
                resultt = resultString.toString();

            }catch (Exception e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this,"Server Error and internet not working,please try again",Toast.LENGTH_SHORT).show();
                        Loader.popup.dismiss();
                    }
                });
            }
            return resultt;
        }
    }


    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show(LoginActivity.this);
    }

    private void alert1(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.close), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show(LoginActivity.this);
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private String encrptionMethod(String textToEncrypt,String method){
        String key = "FISHERY";
        String salt = "MICROWARE";
        byte[] iv = new byte[16];
        String encrypted = null;
        Encryption encryption = Encryption.getDefault(key, salt, iv);

        if(method.equals("encrypt")){
            encrypted = encryption.encryptOrNull(textToEncrypt);
            return encrypted;
        }else{
            String decrypted = encryption.decryptOrNull(encrypted);
            return decrypted;
        }

    }
}
