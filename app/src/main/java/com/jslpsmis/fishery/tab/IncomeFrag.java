package com.jslpsmis.fishery.tab;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.newmodule.IncomeExpActivity;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class IncomeFrag extends Fragment implements DatePickerDialog.OnDateSetListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    TextView payment_date,Unit_label;
    ImageView call_calender;
    Spinner questions_list,inputitem_unit;
    String questions_selected,selected_units,questions_selecteds;
    TextInputEditText enter_total_duck_meat,enter_quantity,enter_rate;
    private OnFragmentInteractionListener mListener;
    RecyclerView recyclerView;
    IncomeAdapter aAdapter;
    Button buttonSubmit;
    String croppplaningid="";
    public ArrayAdapter<IncomeExpenditureMaster> headSpinAdapter;
    List<IncomeExpenditureMaster> incomeExpenditureMasters;

    public IncomeFrag() {

    }

    public static IncomeFrag newInstance(String param1, String param2) {
        IncomeFrag fragment = new IncomeFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.incomefrag_input, container, false);
        payment_date=(TextView)view.findViewById(R.id.payment_date);
        call_calender=(ImageView)view.findViewById(R.id.call_calender);
        enter_total_duck_meat=(TextInputEditText)view.findViewById(R.id.enter_total);
        questions_list=(Spinner)view.findViewById(R.id.questions_list);
        Unit_label=(TextView)view.findViewById(R.id.add_unit_label);
        inputitem_unit=(Spinner)view.findViewById(R.id.inputitem_unit);
        enter_quantity=(TextInputEditText)view.findViewById(R.id.enter_quantity);
        enter_rate=(TextInputEditText)view.findViewById(R.id.enter_rate);
        recyclerView=(RecyclerView)view.findViewById(R.id.income_recycler);

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .list();
        try {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        inputitem_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!inputitem_unit.getSelectedItem().toString().equals("इकाई को चुने")) {
                    Unit_label.setText(" दर प्रति "+ inputitem_unit.getSelectedItem().toString() +"");
                   selected_units= inputitem_unit.getSelectedItem().toString();
                  }
                }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        enter_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ss = enter_quantity.getText().toString();
                String rate=enter_rate.getText().toString();
                try{
                    if(!ss.equals("") && !rate.equals("")){
                        String total= String.valueOf(Integer.parseInt(ss)*Integer.parseInt(rate));
                        enter_total_duck_meat.setText(total);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        questions_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!questions_list.getSelectedItem().toString().equals("आय का प्रकार चुनें(*)")) {
                    questions_selected = questions_list.getSelectedItem().toString();
                    questions_selecteds = String.valueOf(incomeExpenditureMasters.get(position).getIncomeexpenditureid());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonSubmit=(Button)view.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    String entered_value = enter_total_duck_meat.getText().toString();
                    String date = payment_date.getText().toString();
                    String ss = enter_quantity.getText().toString();
                    String rate = enter_rate.getText().toString();

                    if(!date.equals("पहले आय तिथि चुनें(*)")) {
                    if (!entered_value.equals("")) {
                        double getvalue = Double.parseDouble(entered_value);
                        tblIncomeExpenditure tblIncomeExpenditure = new tblIncomeExpenditure(UUID.randomUUID().toString(),questions_selecteds, getvalue, IncomeExpActivity.Pgcode, IncomeExpActivity.waterBodyId, date, "IN", Double.parseDouble(rate), selected_units, Double.parseDouble(ss), croppplaningid, "0", "0", "", "");
                        tblIncomeExpenditure.save();

                        new StyleableToast
                                .Builder(getActivity())
                                .text("डेटा सफलतापूर्वक सहेजा गया")
                                .iconStart(R.drawable.right)
                                .textColor(Color.WHITE)
                                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                .show();
                    }
                    populatingRecyclerView();
                    enter_total_duck_meat.setText("");
                    enter_quantity.setText("");
                    enter_rate.setText("");
                    payment_date.setText("पहले आय तिथि चुनें(*)");
                    questions_list.setSelection(0);
                    inputitem_unit.setSelection(0);
                }
                    }else {
                        new StyleableToast
                                .Builder(getActivity())
                                .text("मान्य तिथि चुनें")
                                .iconStart(R.drawable.wrong_icon_white)
                                .textColor(Color.WHITE)
                                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                .show();
                    }
            }
        });

        call_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        IncomeFrag.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        incomeExpenditureMasters= Select.from(IncomeExpenditureMaster.class)
                .where(Condition.prop("Incomeexpenditureflag").eq("IN"))
                .list();
        IncomeExpenditureMaster model = new IncomeExpenditureMaster(0,"आय का प्रकार चुनें(*)","0","");
        incomeExpenditureMasters.add(model);
        Collections.reverse(incomeExpenditureMasters);
        headSpinAdapter = new ArrayAdapter<IncomeExpenditureMaster>(getActivity(),android.R.layout.simple_spinner_dropdown_item, incomeExpenditureMasters) {};
        questions_list.setAdapter(headSpinAdapter);

        populatingRecyclerView();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        String newDay = dayOfMonth + "";
        String newMonth = (monthOfYear + 1) + "";
        if ((monthOfYear + 1) < 10) {
            newMonth = "0" + newMonth;
        }
        if (dayOfMonth < 10) {
            newDay = "0" + dayOfMonth;
        }
        String newDate =  year+ "/" + newMonth + "/" + newDay;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(date);
            date2 = sdf.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1 != null && date1.compareTo(date2) > 0) {
            Toast.makeText(getContext(), "मान्य तिथि चुनें", Toast.LENGTH_LONG).show();
        }
        else {
            payment_date.setText(newDate);
            payment_date.setHint("");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void populatingRecyclerView() {
        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("IN"))
                .list();

        if(tblIncomeExpenditures.size()>0){
            aAdapter = new IncomeAdapter(getActivity(), tblIncomeExpenditures, IncomeFrag.this);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }

    private boolean validation() {
        boolean result = false;
        int pos = questions_list.getSelectedItemPosition();
        int pos2 = inputitem_unit.getSelectedItemPosition();

        if(enter_total_duck_meat.getText().toString().equals("")){
            new StyleableToast
                    .Builder(getActivity())
                    .text("कुल राशि दर्ज करें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else if (pos == 0) {
            new StyleableToast
                    .Builder(getActivity())
                    .text("विकल्प से आय प्रकार चुनें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else if(enter_quantity.getText().toString().equals("")){
            new StyleableToast
                    .Builder(getActivity())
                    .text("पहले मात्रा दर्ज करें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else if(enter_rate.getText().toString().equals("")){
            new StyleableToast
                    .Builder(getActivity())
                    .text("पहले दर दर्ज करें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else if(payment_date.getText().toString().equals("पहले आय तिथि चुनें(*)")){
            new StyleableToast
                    .Builder(getActivity())
                    .text("मान्य तिथि चुनें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else if (pos2 == 0) {
            new StyleableToast
                    .Builder(getActivity())
                    .text("विकल्प से इकाई का चयन करें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        }
        else {
            result = true;
        }
        return result;
    }

    public  void reload(){
        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("IN"))
                .list();

        if(tblIncomeExpenditures.size()>0){
            aAdapter = new IncomeAdapter(getActivity(), tblIncomeExpenditures, IncomeFrag.this);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }
}
