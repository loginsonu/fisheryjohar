package com.jslpsmis.fishery.tab;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;
import com.jslpsmis.fishery.newmodule.IncomeExpActivity;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpenditureAdapter extends RecyclerView.Adapter<ExpenditureAdapter.MyViewHolder> {

    private List<tblIncomeExpenditure> list;
    private Context context;
    private  Fragment fragment;

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.inputoutput_date)
        TextView inputoutput_date;
        @BindView(R.id.inputoutput_type)
        TextView inputoutput_type;
        @BindView(R.id.inputoutput_unit)
        TextView inputoutput_unit;
        @BindView(R.id.inputoutput_item)
        TextView inputoutput_item;
        @BindView(R.id.delete)
        ImageView imgDelete;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ExpenditureAdapter(Context context, List<tblIncomeExpenditure> itemList, Fragment fragment) {
        this.context = context;
        this.list = itemList;
        this.fragment=fragment;
    }

    @NotNull
    @Override
    public ExpenditureAdapter.MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_exp, parent, false);

        return new ExpenditureAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ExpenditureAdapter.MyViewHolder holder, int position) {
        tblIncomeExpenditure item = list.get(position);
        holder.inputoutput_date.setText(item.getSelecteddate());
        holder.inputoutput_type.setText(String.valueOf(item.getTotalamount()));
        List<IncomeExpenditureMaster> incomeExpenditureMasters= Select.from(IncomeExpenditureMaster.class)
                .where(Condition.prop("Incomeexpenditureflag").eq("EX"))
                .list();
        for(int i=0;i<incomeExpenditureMasters.size();i++){
            String value=item.getIncomeexpenditurecode();
            if(value.equals(String.valueOf(incomeExpenditureMasters.get(i).getIncomeexpenditureid()))){
                holder.inputoutput_item.setText(incomeExpenditureMasters.get(i).getIncomeexpenditurename());
            }
        }

        //condition to make edit and delete visible
        if(item.getIsexported().equals("0")){
            //  holder.imgEdit.setVisibility(View.VISIBLE);
            holder.imgDelete.setVisibility(View.VISIBLE);
        }else{
            //  holder.imgEdit.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.GONE);
        }

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert("Delete","Are you sure, you want to delete this record",item);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    private void alert(String title, String message, tblIncomeExpenditure item){
        com.irozon.alertview.AlertView alert = new com.irozon.alertview.AlertView(title, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction("Delete", AlertActionStyle.DEFAULT, action -> {
            item.delete();
           // presenter.setRecyclerView();
            try {
                ((ExpenditureFrag) fragment).reload();
                if (list.size() == 1) {
                    Intent intent = new Intent(context, AddWaterbodynew.class);
                    intent.putExtra("pgCode", AddWaterbodynew.pgCode);
                    intent.putExtra("pgName", AddWaterbodynew.pgName);
                    intent.putExtra("grpname", AddWaterbodynew.grpname);
                    context.startActivity(intent);
                    ((IncomeExpActivity)context).finish();
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }));
        alert.show((IncomeExpActivity)context);
    }
}
