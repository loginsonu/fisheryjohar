package com.jslpsmis.fishery.tab;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.model.tblIncomeExpendituremodel;
import com.jslpsmis.fishery.newmodule.IncomeExpActivity;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class ProfitFrag extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private IncomeFrag.OnFragmentInteractionListener mListener;
    RecyclerView recyclerView;
    ProfitAdapter aAdapter;
    List<com.jslpsmis.fishery.model.tblIncomeExpendituremodel> tblIncomeExpendituremodel;
    Double income=0.0,expenditure=0.0,net_total_cal=0.0;
    TextView total_in,total_ex,net_total,net_amt;
    String croppplaningid="";
    ProgressDialog progress;
    SwipeRefreshLayout refresh;
    private boolean mLayoutMovementEnabled = true;

    public ProfitFrag() {

    }

    public static ProfitFrag newInstance(String param1, String param2) {
        ProfitFrag fragment = new ProfitFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.profitfrag, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.profit_recycler);
        total_ex=(TextView)view.findViewById(R.id.total_ex);
        total_in=(TextView)view.findViewById(R.id.total_in);
        net_total=(TextView)view.findViewById(R.id.net_total);
        net_amt=(TextView)view.findViewById(R.id.net_amt);
        tblIncomeExpendituremodel=new ArrayList<>();

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .list();
        try {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        List<tblIncomeExpenditure> tblIncome = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("IN"))
                .list();

        if(tblIncome.size() > 0) {
            income=0.0;
            for (int i = 0; i < tblIncome.size(); i++) {
                income += tblIncome.get(i).getTotalamount();
                com.jslpsmis.fishery.model.tblIncomeExpendituremodel tblIncomeExpendituremodels=new tblIncomeExpendituremodel();
                tblIncomeExpendituremodels.setIncomeexpenditurecode(tblIncome.get(i).getIncomeexpenditurecode());
                tblIncomeExpendituremodels.setIncomeamount(tblIncome.get(i).getTotalamount());
                tblIncomeExpendituremodels.setSelecteddate(tblIncome.get(i).getSelecteddate());
                tblIncomeExpendituremodel.add(tblIncomeExpendituremodels);
            }
        }

        List<tblIncomeExpenditure> tblExpenditure = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("EX"))
                .list();

        if(tblExpenditure.size() > 0) {
            expenditure=0.0;
            for (int i = 0; i < tblExpenditure.size(); i++) {
                expenditure += tblExpenditure.get(i).getTotalamount();
                com.jslpsmis.fishery.model.tblIncomeExpendituremodel tblIncomeExpendituremodels=new tblIncomeExpendituremodel();
                tblIncomeExpendituremodels.setIncomeexpenditurecode(tblExpenditure.get(i).getIncomeexpenditurecode());
                tblIncomeExpendituremodels.setExpenditureamount(tblExpenditure.get(i).getTotalamount());
                tblIncomeExpendituremodels.setSelecteddate(tblExpenditure.get(i).getSelecteddate());
                tblIncomeExpendituremodel.add(tblIncomeExpendituremodels);
            }
        }
            populatingRecyclerView();
            net_total_cal=0.0;
            net_total_cal = income - expenditure;
            total_in.setText(String.valueOf(income));
            total_ex.setText(String.valueOf(expenditure));
            net_total.setText(String.valueOf("कुल :"));
            net_amt.setText(String.valueOf(net_total_cal));

        refresh=(SwipeRefreshLayout)view.findViewById(R.id.refresh);
        refresh.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.RED, Color.YELLOW);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                        .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                        .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                        .list();
                try {
                    for (int i = 0; i < fisheryPlantbls.size(); i++) {
                        croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                tblIncomeExpendituremodel=new ArrayList<>();
                List<tblIncomeExpenditure> tblIncome = Select.from(tblIncomeExpenditure.class)
                        .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                        .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                        .where(Condition.prop("cropplanningid").eq(croppplaningid))
                        .where(Condition.prop("Inexflag").eq("IN"))
                        .list();

                if(tblIncome.size() > 0) {
                    income=0.0;
                    for (int i = 0; i < tblIncome.size(); i++) {
                        income += tblIncome.get(i).getTotalamount();
                        com.jslpsmis.fishery.model.tblIncomeExpendituremodel tblIncomeExpendituremodels=new tblIncomeExpendituremodel();
                        tblIncomeExpendituremodels.setIncomeexpenditurecode(tblIncome.get(i).getIncomeexpenditurecode());
                        tblIncomeExpendituremodels.setIncomeamount(tblIncome.get(i).getTotalamount());
                        tblIncomeExpendituremodels.setSelecteddate(tblIncome.get(i).getSelecteddate());
                        tblIncomeExpendituremodel.add(tblIncomeExpendituremodels);
                    }
                }

                List<tblIncomeExpenditure> tblExpenditure = Select.from(tblIncomeExpenditure.class)
                        .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                        .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                        .where(Condition.prop("cropplanningid").eq(croppplaningid))
                        .where(Condition.prop("Inexflag").eq("EX"))
                        .list();

                if(tblExpenditure.size() > 0) {
                    expenditure=0.0;
                    for (int i = 0; i < tblExpenditure.size(); i++) {
                        expenditure += tblExpenditure.get(i).getTotalamount();
                        com.jslpsmis.fishery.model.tblIncomeExpendituremodel tblIncomeExpendituremodels=new tblIncomeExpendituremodel();
                        tblIncomeExpendituremodels.setIncomeexpenditurecode(tblExpenditure.get(i).getIncomeexpenditurecode());
                        tblIncomeExpendituremodels.setExpenditureamount(tblExpenditure.get(i).getTotalamount());
                        tblIncomeExpendituremodels.setSelecteddate(tblExpenditure.get(i).getSelecteddate());
                        tblIncomeExpendituremodel.add(tblIncomeExpendituremodels);
                    }
                }
                if(tblIncomeExpendituremodel.size()>0){
                    aAdapter = new ProfitAdapter(getActivity(), tblIncomeExpendituremodel, ProfitFrag.this);
                    LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(verticalLayoutmanager);
                    recyclerView.setAdapter(aAdapter);
                }
                net_total_cal=0.0;
                net_total_cal = income - expenditure;
                total_in.setText(String.valueOf(income));
                total_ex.setText(String.valueOf(expenditure));
                net_total.setText(String.valueOf("कुल :"));
                net_amt.setText(String.valueOf(net_total_cal));
                mLayoutMovementEnabled=false;
                closerefresh(mLayoutMovementEnabled);
            }
        });

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            FragmentTransaction ftr = getFragmentManager().beginTransaction();
            ftr.detach(this).attach(this).commit();
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void populatingRecyclerView() {
        if(tblIncomeExpendituremodel.size()>0){
            aAdapter = new ProfitAdapter(getActivity(), tblIncomeExpendituremodel, ProfitFrag.this);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
            aAdapter.notifyDataSetChanged();
        }
    }

    private void closerefresh(Boolean mLayoutMovementEnabled){
        refresh.setRefreshing(mLayoutMovementEnabled);
        refresh.setEnabled(mLayoutMovementEnabled);
    }

}