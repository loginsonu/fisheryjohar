package com.jslpsmis.fishery.tab;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.newmodule.IncomeExpActivity;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class ExpenditureFrag extends Fragment implements DatePickerDialog.OnDateSetListener,AdapterView.OnItemSelectedListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private IncomeFrag.OnFragmentInteractionListener mListener;
    TextView payment_date;
    ImageView call_calender;
    Spinner questions_list_ex;
    String questions_selected,questions_selecteds;
    TextInputEditText enter_total_duck_meat;

    RecyclerView recyclerView;
    ExpenditureAdapter aAdapter;
    Button buttonSubmit;
    String croppplaningid = "";
    public ArrayAdapter<IncomeExpenditureMaster> headSpinAdapter;
    List<IncomeExpenditureMaster> incomeExpenditureMasters;

    public ExpenditureFrag() {

    }

    public static ExpenditureFrag newInstance(String param1, String param2) {
        ExpenditureFrag fragment = new ExpenditureFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.expenditurefrag_input, container, false);

        payment_date = (TextView) view.findViewById(R.id.payment_date);
        call_calender = (ImageView) view.findViewById(R.id.call_calender);
        recyclerView = (RecyclerView) view.findViewById(R.id.exp_list);
        buttonSubmit = (Button) view.findViewById(R.id.buttonSubmit);
        questions_list_ex = (Spinner) view.findViewById(R.id.questions_list_ex);
        enter_total_duck_meat = (TextInputEditText) view.findViewById(R.id.enter_total);

        incomeExpenditureMasters= Select.from(IncomeExpenditureMaster.class)
                .where(Condition.prop("Incomeexpenditureflag").eq("EX"))
                .list();
        IncomeExpenditureMaster model = new IncomeExpenditureMaster(0,"व्यय का प्रकार चुनें(*)","0","");
        incomeExpenditureMasters.add(model);

        Collections.reverse(incomeExpenditureMasters);
        headSpinAdapter = new ArrayAdapter<IncomeExpenditureMaster>(getActivity(),android.R.layout.simple_spinner_dropdown_item, incomeExpenditureMasters) {};
        questions_list_ex.setAdapter(headSpinAdapter);

        call_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ExpenditureFrag.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        questions_list_ex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!questions_list_ex.getSelectedItem().toString().equals("व्यय का प्रकार चुनें(*)")) {
                    questions_selected = questions_list_ex.getSelectedItem().toString();
                    questions_selecteds = String.valueOf(incomeExpenditureMasters.get(position).getIncomeexpenditureid());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .list();
        try {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        buttonSubmit = (Button) view.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    String entered_value = enter_total_duck_meat.getText().toString();
                    String date = payment_date.getText().toString();

                    if (!date.equals("पहले व्यय तिथि चुनें(*)")) {
                        if (!entered_value.equals("")) {
                            double getvalue = Double.parseDouble(entered_value);
                            tblIncomeExpenditure tblIncomeExpenditure =
                                    new tblIncomeExpenditure(UUID.randomUUID().toString(),questions_selecteds,
                                            getvalue, IncomeExpActivity.Pgcode,
                                            IncomeExpActivity.waterBodyId, date, "EX", 0.00, "", 0.00,
                                            croppplaningid, "0", "0", "", "");

                            tblIncomeExpenditure.save();

                            new StyleableToast
                                    .Builder(getActivity())
                                    .text("डेटा सफलतापूर्वक सहेजा गया")
                                    .iconStart(R.drawable.right)
                                    .textColor(Color.WHITE)
                                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                    .show();
                        }
                        populatingRecyclerView();
                        enter_total_duck_meat.setText("");
                        payment_date.setText("पहले व्यय तिथि चुनें(*)");
                        questions_list_ex.setSelection(0);

                    } else {
                        new StyleableToast
                                .Builder(getActivity())
                                .text("मान्य तिथि चुनें")
                                .iconStart(R.drawable.wrong_icon_white)
                                .textColor(Color.WHITE)
                                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                .show();
                    }
                }
            }
        });
        populatingRecyclerView();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        String newDay = dayOfMonth + "";
        String newMonth = (monthOfYear + 1) + "";
        if ((monthOfYear + 1) < 10) {
            newMonth = "0" + newMonth;
        }
        if (dayOfMonth < 10) {
            newDay = "0" + dayOfMonth;
        }
        String newDate = year + "/" + newMonth + "/" + newDay;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(date);
            date2 = sdf.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1 != null && date1.compareTo(date2) > 0) {
            Toast.makeText(getContext(), "मान्य तिथि चुनें", Toast.LENGTH_LONG).show();
        } else {
            payment_date.setText(newDate);
            payment_date.setHint("");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void populatingRecyclerView() {
        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("EX"))
                .list();

        if (tblIncomeExpenditures.size() > 0) {
            aAdapter = new ExpenditureAdapter(getActivity(), tblIncomeExpenditures, ExpenditureFrag.this);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }

    private boolean validation() {
        boolean result = false;
        int pos = questions_list_ex.getSelectedItemPosition();

        if (enter_total_duck_meat.getText().toString().equals("")) {
            new StyleableToast
                    .Builder(getActivity())
                    .text("कुल राशि दर्ज करें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        } else if (pos == 0) {
            new StyleableToast
                    .Builder(getActivity())
                    .text("विकल्प से व्यय प्रकार चुनें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        } else if (payment_date.getText().toString().equals("SELECT DATE FIRST(*)")) {
            new StyleableToast
                    .Builder(getActivity())
                    .text("मान्य तिथि चुनें")
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
        } else {
            result = true;
        }
        return result;
    }

    public void reload() {
        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("Inexflag").eq("EX"))
                .list();

        if (tblIncomeExpenditures.size() > 0) {
            aAdapter = new ExpenditureAdapter(getActivity(), tblIncomeExpenditures, ExpenditureFrag.this);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }
}