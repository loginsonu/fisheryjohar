package com.jslpsmis.fishery.tab;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.database.IncomeExpenditureMaster;
import com.jslpsmis.fishery.model.tblIncomeExpendituremodel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfitAdapter extends RecyclerView.Adapter<ProfitAdapter.MyViewHolder> {

    private List<tblIncomeExpendituremodel> list;
    private Context context;
    private Fragment fragment;

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.inputoutput_date)
        TextView inputoutput_date;
        @BindView(R.id.inputoutput_type)
        TextView inputoutput_type;
        @BindView(R.id.inputoutput_unit)
        TextView inputoutput_unit;
        @BindView(R.id.inputoutput_item)
        TextView inputoutput_item;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ProfitAdapter(Context context, List<tblIncomeExpendituremodel> itemList, Fragment fragment) {
        this.context = context;
        this.list = itemList;
        this.fragment = fragment;
    }

    @NotNull
    @Override
    public ProfitAdapter.MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_profit, parent, false);

        return new ProfitAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ProfitAdapter.MyViewHolder holder, int position) {
        tblIncomeExpendituremodel item = list.get(position);
        holder.inputoutput_date.setText(item.getSelecteddate());
        holder.inputoutput_type.setText(String.valueOf(item.getIncomeamount()));
        holder.inputoutput_unit.setText(String.valueOf(item.getExpenditureamount()));
        List<IncomeExpenditureMaster> incomeExpenditureMasters= IncomeExpenditureMaster.listAll(IncomeExpenditureMaster.class);

        for(int i=0;i<incomeExpenditureMasters.size();i++){
            String value=item.getIncomeexpenditurecode();
            if(value.equals(String.valueOf(incomeExpenditureMasters.get(i).getIncomeexpenditureid()))){
                holder.inputoutput_item.setText(incomeExpenditureMasters.get(i).getIncomeexpenditurename());
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

}
