package com.jslpsmis.fishery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.adapter.AddWaterBodyAdapter;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterModel;

import java.util.ArrayList;

public class AddWaterBodyActivity extends AppCompatActivity {

    TextView tvFarmerName,pgName,shgName,addwaterResource;
    RecyclerView recyclerView;
    Dataprovider dataprovider;
    ArrayList<AddWaterModel> list;
    AddWaterBodyAdapter aAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_water_body);
        init();
    }

    private void init() {
        dataprovider = new Dataprovider(this);
        tvFarmerName  = (TextView) findViewById(R.id.headingCropPlanLandInfo);
        pgName = (TextView) findViewById(R.id.textView10);
        shgName = (TextView) findViewById(R.id.textView40);
        addwaterResource = findViewById(R.id.addPlotActivityBaseLineLandInfo2);
        recyclerView = findViewById(R.id.recyclerViewBaseLineLandInfo);

        tvFarmerName.setText("जल श्रोत"+"("+ DashboardActivity.memberName+")");
        pgName.setText(ProducerMemberActivity.pgName);
        shgName.setText(DashboardActivity.grpName);

        addwaterResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWaterBodyActivity.this, AddWaterBodyForm.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        list = dataprovider.getAddWaterList(DashboardActivity.pgCode);
        if(list.size()>0){
            aAdapter = new AddWaterBodyAdapter(this,list);
            LinearLayoutManager verticalLayoutmanager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }
}
