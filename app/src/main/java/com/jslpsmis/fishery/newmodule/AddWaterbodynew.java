package com.jslpsmis.fishery.newmodule;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AddWaterBodyForm;
import com.jslpsmis.fishery.adapter.AddWaterBodyNewAdapter;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class AddWaterbodynew extends AppCompatActivity {

    TextView addPlotActivityBaseLineLandInfo2;
    Dataprovider dataprovider;
    public static String pgName,pgCode,flag,grpname;
    TextView producerGroupName;
    TextView listCount;
    RecyclerView recyclerView;
    ArrayList<AddWaterModel> list;
    public static FloatingActionButton btnUpload;
    AddWaterBodyNewAdapter aAdapter;

    public static List<AddWaterModel> listAddwater;
    public static List<FisheryPlantbl> fisheryPlantblList;
    public static String sDataFisheryPlantbl;
    MDMSharedPreference mdmSharedPreference;
    ImageView backButtonCropPlanForm;
    private static String versionString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_waterbodynew);
        Intent intent = getIntent();
        pgCode = intent.getStringExtra("pgCode");
        pgName = intent.getStringExtra("pgName");
        grpname=intent.getStringExtra("grpname");
        init();
    }

    public void init(){
        mdmSharedPreference = MDMSharedPreference.getInstance(this);
        dataprovider = new Dataprovider(this);
        recyclerView = findViewById(R.id.recyclerViewBaseLineLandInfo);

        producerGroupName = findViewById(R.id.textView10);
        listCount = (TextView) findViewById(R.id.listCountProducerMemeberActivity);

        producerGroupName.setText(ProducerMemberNewActivity.pgName);
        getVersion();
        addPlotActivityBaseLineLandInfo2=(TextView)findViewById(R.id.addPlotActivityBaseLineLandInfo2);
        addPlotActivityBaseLineLandInfo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWaterbodynew.this, AddWaterBodyForm.class);
                intent.putExtra("pgcode",pgCode);
                intent.putExtra("pgname",pgName);
                intent.putExtra("grpname",grpname);
                startActivity(intent);
            }
        });

        backButtonCropPlanForm=(ImageView)findViewById(R.id.backButtonCropPlanForm);
        backButtonCropPlanForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWaterbodynew.this, ProducerMemberNewActivity.class);
                intent.putExtra("pgCode",pgCode);
                intent.putExtra("pgName",pgName);
                intent.putExtra("flag","old");
                intent.putExtra("grpname",grpname);
                startActivity(intent);
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        dataprovider = new Dataprovider(this);
        getVersion();
        populatingRecyclerView();

        if(!Constant.uploadWaterBodyInProgress) {
            listAddwater = dataprovider.getAddWaterListAll(pgCode);
        }else{
            listAddwater = new ArrayList<>();
        }

        if(!Constant.fisheryplanuploadinprogress){
            fisheryPlantblList = Select.from(FisheryPlantbl.class)
                    .where(Condition.prop("pgcode").eq(pgCode))
                    .where(Condition.prop("isexported").eq("false"))
                    .list();

            System.out.print("");

        }else{
            fisheryPlantblList = new ArrayList<>();
        }
    }

    private void populatingRecyclerView() {
        list = dataprovider.getAddWaterList(pgCode);
        if(list.size()>0){
            aAdapter = new AddWaterBodyNewAdapter(AddWaterbodynew.this,list,pgCode,pgName,versionString);
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
            aAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AddWaterbodynew.this, ProducerMemberNewActivity.class);
        intent.putExtra("pgCode",pgCode);
        intent.putExtra("pgName",pgName);
        intent.putExtra("flag","old");
        intent.putExtra("grpname",grpname);
        startActivity(intent);
        finish();
    }

    private void getVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionString = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}