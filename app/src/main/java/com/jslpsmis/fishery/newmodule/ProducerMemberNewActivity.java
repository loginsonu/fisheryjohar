package com.jslpsmis.fishery.newmodule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.ProducerMemberActivity;
import com.jslpsmis.fishery.adapter.ProducerGroupMemberActivityPopUpAdapter;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import java.util.ArrayList;

public class ProducerMemberNewActivity extends AppCompatActivity {

    LinearLayout first,second;
    ArrayList<ProducerGroupMemberActivityModel> list;
    Dataprovider dataprovider;
    public static String pgName,pgCode,flag,grpname;
    EditText search;
    ImageView backButton;
    TextView producerGroupName;
    TextView listCount;

    PopupWindow popup;
    RecyclerView recyclerView;
    ProducerGroupMemberActivityPopUpAdapter aAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_producer_member_new);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        pgCode = intent.getStringExtra("pgCode");
        pgName = intent.getStringExtra("pgName");
        flag = intent.getStringExtra("flag");
        grpname=intent.getStringExtra("grpname");

        backButton = (ImageView) findViewById(R.id.backButtonmodule);
        dataprovider = new Dataprovider(this);
        first = (LinearLayout) findViewById(R.id.first);
        second=(LinearLayout)findViewById(R.id.second);
        search = (EditText) findViewById(R.id.editText);
        producerGroupName = findViewById(R.id.producerGroupName);
        listCount = (TextView) findViewById(R.id.listCountProducerMemeberActivity);

        producerGroupName.setText(pgName);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProducerMemberNewActivity.this, AddWaterbodynew.class);
                intent.putExtra("pgCode",pgCode);
                intent.putExtra("pgName",pgName);
                intent.putExtra("flag","old");
                intent.putExtra("grpname",grpname);
                startActivity(intent);
                finish();
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProducerMemberNewActivity.this, ProducerMemberActivity.class);
                intent.putExtra("pgCode",pgCode);
                intent.putExtra("pgName",pgName);
                intent.putExtra("flag","old");
                intent.putExtra("grpname",grpname);
                startActivity(intent);
                finish();
            }
        });

        populatingRecyclerView();
    }

    private void populatingRecyclerView() {
        ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
        list= dataprovider.getProducerGrpMemList(pgCode);

        if(list.size()>0){
            listCount.setText(getString(R.string.list_count)+ list.size()+"");
            //newItemList.addAll(itemList);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
