package com.jslpsmis.fishery.newmodule;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.orm.SugarApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class MyAppStartClass extends SugarApp {
    private static String DB_NAME = "Fishey.db";

    @Override
    public final void onCreate() {
        init();
        super.onCreate();
    }

    private void init() {
        initDB();
    }

    private void initDB() {
        try {
                File folder = new File(Environment.getExternalStorageDirectory().toString() + "/FisheyDATABASE");
                folder.mkdirs();

                //Save the path as a string value
                String extStorageDirectory = folder.toString();
                File outputFile = new File(extStorageDirectory, "Fishey");
                if (!outputFile.exists())
                    outputFile.createNewFile();
                File data = Environment.getDataDirectory();
                File inputFile = new File(data, "data/" + getPackageName() + "/databases/" + DB_NAME);

                Context context = getApplicationContext();
                SQLiteDatabase db = context.openOrCreateDatabase("Fishey.db", context.MODE_PRIVATE, null);
                db.close();
                InputStream dbInput = getApplicationContext().getAssets().open("Fishey.db");
                OutputStream dbOutput = new FileOutputStream(outputFile);
                try {
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = dbInput.read(buffer)) > 0) {
                        dbOutput.write(buffer, 0, length);
                    }
                } finally {
                    dbOutput.flush();
                    dbOutput.close();
                    dbInput.close();
                }
        } catch (Exception e) {
            e.toString();
        }
    }

    private boolean doesDatabaseExist(MyAppStartClass context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);

        return dbFile.exists();
    }
}
