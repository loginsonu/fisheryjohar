package com.jslpsmis.fishery.newmodule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.AppConstant;
import com.jslpsmis.fishery.DatabaseHelper;
import com.jslpsmis.fishery.commonclass.CheckConnectivity;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.database.Cropplanningmemberstbl;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.database.tblIncomeExpenditure;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.jslpsmis.fishery.service.VolleyString;
import com.jslpsmis.fishery.tab.ExpenditureFrag;
import com.jslpsmis.fishery.tab.IncomeFrag;
import com.jslpsmis.fishery.tab.ProfitFrag;
import com.jslpsmis.fishery.tab.TabAdapter;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class IncomeExpActivity extends AppCompatActivity implements VolleyString.VolleyListner{

    TabAdapter adapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    public static String waterBodyId;
    public static String waterBodyArea;
    public static String Pgcode,PgName;
    Button buttonFinal;
    TextView listCountProducerMemeberActivity,producerGroupName;
    String saveIncomeExpenditure;
    MDMSharedPreference mdmSharedPreference;
    ProgressDialog progress;
    ImageView backButtonProducerGroupMemberActivity;
    String saveselectedmembers="",tbltracFisheryPlans="";
    List<AddWaterModel> listAddwater;
    List<FisheryPlantbl> fisheryPlantblList;
    String sSaveDateAddWaterBody;
    Dataprovider dataprovider;
    JSONArray array = null,array2=null;
    public SQLiteDatabase dbcpa_db;
    private Cursor cursor;
    private DatabaseHelper dbHelper;
    String croppplaningid="";
    private static String versionString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_exp);

        Intent intent = getIntent();
        waterBodyId = intent.getStringExtra("waterBodyId");
        waterBodyArea = intent.getStringExtra("wbArea");
        Pgcode= intent.getStringExtra("pgcode");
        PgName= intent.getStringExtra("grpname");

        dataprovider = new Dataprovider(IncomeExpActivity.this);
        getVersion();
        Log.d("versionString",versionString);
        listAddwater = dataprovider.getAddWaterListAll2(Pgcode,waterBodyId);

        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("Pgcode").eq(IncomeExpActivity.Pgcode))
                .where(Condition.prop("waterbodyid").eq(IncomeExpActivity.waterBodyId))
                .list();
        try {
            for (int i = 0; i < fisheryPlantbls.size(); i++) {
                croppplaningid = fisheryPlantbls.get(i).getFisheryplanid();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(waterBodyId))
                .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                .list();

        if(fisheryPlantblList.get(0).getIsexported().equals("false")){
            //for not uploaded
            fisheryPlantblList = Select.from(FisheryPlantbl.class)
                    .where(Condition.prop("pgcode").eq(Pgcode))
                    .where(Condition.prop("waterbodyid").eq(waterBodyId))
                    .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                    .where(Condition.prop("isexported").eq("false"))
                    .groupBy("fisheryplanid")
                    .list();
        }else if(fisheryPlantblList.get(0).getStatus().equals("0")){
            //for update
            fisheryPlantblList = Select.from(FisheryPlantbl.class)
                    .where(Condition.prop("pgcode").eq(Pgcode))
                    .where(Condition.prop("waterbodyid").eq(waterBodyId))
                    .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                    .where(Condition.prop("status").eq("0"))
                    .groupBy("fisheryplanid")
                    .list();
        }else{
            fisheryPlantblList=new ArrayList<>();
        }

        mdmSharedPreference = MDMSharedPreference.getInstance(this);
        listCountProducerMemeberActivity=(TextView)findViewById(R.id.listCountProducerMemeberActivity);
        listCountProducerMemeberActivity.setText("Total Area "+ waterBodyArea);

        producerGroupName=(TextView)findViewById(R.id.producerGroupName);
        producerGroupName.setText(PgName);

        viewPager = (ViewPager)findViewById(R.id.viewPager);
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        adapter = new TabAdapter(this.getSupportFragmentManager());
        adapter.addFragment(new IncomeFrag(), "आय");
        adapter.addFragment(new ExpenditureFrag(), "व्यय");
        adapter.addFragment(new ProfitFrag(), "लाभ/हानि");

        backButtonProducerGroupMemberActivity=(ImageView)findViewById(R.id.backButtonProducerGroupMemberActivity);
        backButtonProducerGroupMemberActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IncomeExpActivity.this, AddWaterbodynew.class);
                intent.putExtra("pgCode",Pgcode);
                intent.putExtra("pgName",PgName);
                intent.putExtra("grpname",PgName);
                startActivity(intent);
                finish();
            }
        });

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        buttonFinal=(Button)findViewById(R.id.final_submit);
        buttonFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                        .where(Condition.prop("Pgcode").eq(Pgcode))
                        .where(Condition.prop("waterbodyid").eq(waterBodyId))
                        .where(Condition.prop("cropplanningid").eq(croppplaningid))
                        .where(Condition.prop("isexported").eq("0"))
                        .list();
                if(tblIncomeExpenditures.size()>0) {
                    CheckConnectivity checkConnectivity = new CheckConnectivity();
                    if (checkConnectivity.CheckConnection(IncomeExpActivity.this)) {
                        Selectionpopup(v);
                    }else {
                        alert(IncomeExpActivity.this.getString(R.string.net_problem), IncomeExpActivity.this.getString(R.string.net_msg));
                    }
                }else{
                    new StyleableToast
                            .Builder(IncomeExpActivity.this)
                            .text("अपलोड करने के लिए कोई डेटा नहीं मिला।")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();
                }
             }
        });
    }

    private String userName(){
        String user = mdmSharedPreference.getString("userNamee");
        return  user;
    }


    public void Selectionpopup(View view){
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
        if(inflater == null) {
            return;
        }
        View popupView = View.inflate(this, R.layout.popup_window_incomeex, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                true
        );
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        View container = popupWindow.getContentView().getRootView();
        if(container != null) {
            WindowManager wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams p = (WindowManager.LayoutParams)container.getLayoutParams();
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            p.dimAmount = 0.8f;
            if(wm != null) {
                wm.updateViewLayout(container, p);
            }
        }

        Button buttonYes=(Button)popupView.findViewById(R.id.buttonYes);
        Button buttonno=(Button)popupView.findViewById(R.id.buttonno);
        buttonno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                DialogShow();
                 if(listAddwater.size()>0||fisheryPlantblList.size()>0) {
                         converttblAddWaterBody();
                         //   new UploadDataToServerTable(context,AppConstant.Upload_tblAddWaterBody,sSaveDateAddWaterBody,"tblAddWaterBody");
                         callUploadWaterBody(sSaveDateAddWaterBody,versionString);
                         /*fisheryPlantblList = Select.from(FisheryPlantbl.class)
                                 .where(Condition.prop("pgcode").eq(Pgcode))
                                 .where(Condition.prop("waterbodyid").eq(waterBodyId))
                                 .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                                 .where(Condition.prop("isexported").eq("false"))
                                 .groupBy("fisheryplanid")
                                 .list();*/

                         converttblFisheryPlan(waterBodyId,Pgcode);
                         converttblcropplanningmemebrs(waterBodyId,Pgcode);
                         //new UploadDataToServerTable(context,AppConstant.Upload_tbltracFisheryPlan,tbltracFisheryPlans,"tbltracFisheryPlan");
                         callUploadFisheryPlan(tbltracFisheryPlans,versionString);
                         callUploadFisherySelectedMembers(saveselectedmembers,versionString);
                         converttblIncomeExpenditure();
                         callUploadIncomeExpenditure(saveIncomeExpenditure,versionString);
                 }else {
                     converttblIncomeExpenditure();
                     callUploadIncomeExpenditure(saveIncomeExpenditure,versionString);
                 }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(IncomeExpActivity.this, AddWaterbodynew.class);
        intent.putExtra("pgCode", AddWaterbodynew.pgCode);
        intent.putExtra("pgName", AddWaterbodynew.pgName);
        intent.putExtra("grpname", AddWaterbodynew.grpname);
        startActivity(intent);
        finish();
    }

    private void converttblIncomeExpenditure(){
        List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                .where(Condition.prop("Pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(waterBodyId))
                .where(Condition.prop("cropplanningid").eq(croppplaningid))
                .where(Condition.prop("isexported").eq("0"))
                .list();

        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("[");
        for (int i = 0; i< tblIncomeExpenditures.size(); i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"incomeexpenditureuuid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getIncomeexpenditureuuid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"incomeexpenditurecode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(tblIncomeExpenditures.get(i).getIncomeexpenditurecode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"totalamount\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getTotalamount()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Pgcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getPgcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"waterbodyid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getWaterbodyid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"selecteddate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getSelecteddate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"flag\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getFlag()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"rate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getRate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"unit\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getUnit()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"quantity\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getQuantity()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"cropplanningid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getCropplanningid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"status\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getStatus()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"isexported\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+tblIncomeExpenditures.get(i).getIsexported()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createdby\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+userName()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createddate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("null");

            lStringBuilder.append("}");
            if(i<tblIncomeExpenditures.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
        saveIncomeExpenditure= lStringBuilder.toString();
        System.out.print("");
        Log.d("saveIncomeExpenditure",saveIncomeExpenditure);
    }

    public void callUploadIncomeExpenditure(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(this)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(this);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_PGFisheryIncomeExpenditure_DataInsert,
                    AppConstant.UploadIncomeExpendituresatus, this).postIncomeExpenditure(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(this)
                    .text(getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(getResources().getColor(R.color.colorPrimary))
                    .show();
            progress.dismiss();
        }
    }

    @Override
    public void onResponseSuccess(String tableIndentifier, String result) {
          if(tableIndentifier.equals(AppConstant.Upload_tblAddWaterBodystatus)){
                 try {
                     JSONObject jsonObj = new JSONObject(result);
                     array = jsonObj.getJSONArray("Table");
                     if (array.length() > 0) {
                         JSONObject object = array.getJSONObject(0);
                         int res = object.optInt("RetValue");
                         if (res == 1) {
                             for (int i = 0; i < listAddwater.size(); i++) {
                                 dataprovider.updateAddWaterBody(Pgcode,waterBodyId);
                                 String sql = "";
                                 String isExported = "true";
                                 sql = "Update tblAddWaterBody set IsExported='" + isExported + "'  where PgCode='" + Pgcode + "' and  WaterBodyId='" + waterBodyId + "'";
                                 try {
                                     executeSql(sql);
                                 } catch (Exception e) {
                                     // TODO: handle exception
                                 }
                             }
                            // listAddwater.clear();
                            // Constant.uploadWaterBodyInProgress = false;
                             new StyleableToast
                                     .Builder(IncomeExpActivity.this)
                                     .text("वॉटरबॉडी सफलतापूर्वक अपलोड किया गया")
                                     .iconStart(R.drawable.right)
                                     .textColor(Color.WHITE)
                                     .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                                     .show();

                         } else {
                             /*new StyleableToast
                                     .Builder(context)
                                     .text("Upload failed for tblAddWaterBody")
                                     .iconStart(R.drawable.wrong_icon_white)
                                     .textColor(Color.WHITE)
                                     .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                     .show();*/
                         }
                     }
                 }  catch (JSONException e) {
                     e.printStackTrace();
                 }
        }

        if(tableIndentifier.equals(AppConstant.Upload_tbltracFisheryPlanstatus)){
            try {
                JSONObject jsonObj = new JSONObject(result);
                 array2 = jsonObj.getJSONArray("Table");
                if (array2.length() > 0) {
                    JSONObject object = array2.getJSONObject(0);
                 //   Toast.makeText(IncomeExpActivity.this,"Return value= "+object.optString("RetValue").toString(),Toast.LENGTH_LONG).show();
                    try {
                        if (!object.getString("RetValue").equals("")) {
                            int res = object.optInt("RetValue");
                            if (res > 0) {
                                List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                                        .where(Condition.prop("pgcode").eq(Pgcode))
                                        .where(Condition.prop("waterbodyid").eq(waterBodyId))
                                        .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                                        .list();

                                if (fisheryPlantbls.size() > 0) {
                                    for (int i = 0; i < fisheryPlantbls.size(); i++) {
                                        fisheryPlantbls.get(i).setIsexported("true");
                                        //fisheryPlantbls.get(i).setStatus("1");
                                        fisheryPlantbls.get(i).save();
                                    }
                                }
                                new StyleableToast
                                        .Builder(IncomeExpActivity.this)
                                        .text("मत्स्य योजना सफलतापूर्वक अपलोड किया गया")
                                        .iconStart(R.drawable.right)
                                        .textColor(Color.WHITE)
                                        .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                                        .show();
                            }
                        }
                    }catch (JSONException e) {
                    e.printStackTrace();
                            }
                        } else {
                      /*  new StyleableToast
                                .Builder(context)
                                .text("Upload failed for tbltracFisheryPlans")
                                .iconStart(R.drawable.wrong_icon_white)
                                .textColor(Color.WHITE)
                                .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                                .show();*/
                        }

            }  catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(tableIndentifier.equals(AppConstant.Upload_FisherySelectedMembers_DataInsertstatus)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String data = jsonObject.optString("Data");
                if (data.equals("1")) {
                    List<Cropplanningmemberstbl> cropplanningmemberstbls = Select.from(Cropplanningmemberstbl.class)
                            .where(Condition.prop("pgcode").eq(Pgcode))
                            .where(Condition.prop("waterbodyid").eq(waterBodyId))
                            .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                            .where(Condition.prop("isexported").eq("0"))
                            .list();
                    if (cropplanningmemberstbls.size() > 0) {
                        for (int i = 0; i < cropplanningmemberstbls.size(); i++) {
                            cropplanningmemberstbls.get(i).setIsexported("1");
                            cropplanningmemberstbls.get(i).save();
                        }
                    }
                    new StyleableToast
                            .Builder(IncomeExpActivity.this)
                            .text("क्रॉपप्लानिंगसदस्यों को सफलतापूर्वक अपलोड किया गया")
                            .iconStart(R.drawable.right)
                            .textColor(Color.WHITE)
                            .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                            .show();
                }
                if (!data.equals("1")) {
                /*    new StyleableToast
                            .Builder(context)
                            .text("Upload failed for cropplanningmemberstbls")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(context.getResources().getColor(R.color.colorPrimary))
                            .show();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(tableIndentifier.equals(AppConstant.UploadIncomeExpendituresatus)){
            try {
                JSONObject jsonObject = new JSONObject(result);
                String data = jsonObject.optString("Data");
                if(data.equals("1")){
                    List<tblIncomeExpenditure> tblIncomeExpenditures = Select.from(tblIncomeExpenditure.class)
                            .where(Condition.prop("Pgcode").eq(Pgcode))
                            .where(Condition.prop("waterbodyid").eq(waterBodyId))
                            .where(Condition.prop("cropplanningid").eq(croppplaningid))
                            .where(Condition.prop("isexported").eq("0"))
                            .list();
                    if(tblIncomeExpenditures.size()>0){
                        for(int i=0;i<tblIncomeExpenditures.size();i++){
                            tblIncomeExpenditures.get(i).setIsexported("1");
                            tblIncomeExpenditures.get(i).setStatus("1");
                            tblIncomeExpenditures.get(i).save();
                        }
                        List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                                .where(Condition.prop("Pgcode").eq(Pgcode))
                                .where(Condition.prop("waterbodyid").eq(waterBodyId))
                                .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                                .list();

                         if(fisheryPlantbls.size()>0) {
                             for (int i = 0; i < fisheryPlantbls.size(); i++) {
                                 fisheryPlantbls.get(i).setStatus("1");
                                 fisheryPlantbls.get(i).save();
                             }
                         }

                        DialogClose();

                         Intent intent=new Intent(IncomeExpActivity.this, AddWaterbodynew.class);
                         intent.putExtra("pgCode",Pgcode);
                         intent.putExtra("pgName",PgName);
                         intent.putExtra("grpname", AddWaterbodynew.grpname);
                         startActivity(intent);
                         finish();

                        new StyleableToast
                                .Builder(this)
                                .text("आय व्यय सफलतापूर्वक अपलोड किया गया")
                                .iconStart(R.drawable.right)
                                .textColor(Color.WHITE)
                                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                                .show();
                    }
                }else if (data.equals("-2")){
                    new StyleableToast
                            .Builder(this)
                            .text("Old Version के लिए अपलोड विफल रहा! कृपया नए वर्शन से अपडेट करें ")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();
                }else {
                    new StyleableToast
                            .Builder(this)
                            .text("tblIncomeExpenditures के लिए अपलोड विफल रहा")
                            .iconStart(R.drawable.wrong_icon_white)
                            .textColor(Color.WHITE)
                            .backgroundColor(getResources().getColor(R.color.colorPrimary))
                            .show();
                }
                DialogClose();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResponseFailure(String tableIdentifier) {
        progress.dismiss();
        new StyleableToast
                .Builder(this)
                .text("सर्वर त्रुटि, इंटरनेट कनेक्शन जांचें")
                .iconStart(R.drawable.wrong_icon_white)
                .textColor(Color.WHITE)
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();
        DialogClose();
    }

    private void DialogShow() {
        progress= new ProgressDialog(IncomeExpActivity.this);
        progress.setMessage("अपलोड हो रहा है, कृपया प्रतीक्षा करें...");
        progress.setCancelable(false);
        progress.show();
    }

    private void DialogClose(){
        if(progress!=null){
            progress.dismiss();
        }
    }

    private void converttblAddWaterBody(){
        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("{");
        lStringBuilder.append("\"tblAddWaterBody\"");
        lStringBuilder.append(":");
        lStringBuilder.append("[");
        for (int i= 0; i<listAddwater.size();i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"PgCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getPgCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpMemCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpMemberCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyOwnerCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyOwnerCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"NoOfMonthWaterAvailable\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getNoOfMonthAvailable()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"SelectedFishery\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getSelectedFishery());
            lStringBuilder.append(",");

            lStringBuilder.append("\"IsFisheryHappening\"");
            lStringBuilder.append(":");
            String yes = listAddwater.get(i).getIsFisheryHappenig();
            if(yes.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }
            lStringBuilder.append(",");

            lStringBuilder.append("\"IsTrainingTaken\"");
            lStringBuilder.append(":");
            String yes1 = listAddwater.get(i).getIsTrainingTaken();
            if(yes1.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }
            lStringBuilder.append(",");

            lStringBuilder.append("\"FromWhereTraining\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getFromwheretraing()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"ExpectedAreaInDecimal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalArea());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalProductionInQuintal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalProduction());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalIncome\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalIncome());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyPicPath\"");
            lStringBuilder.append(":");
            String imagename = removeWords(listAddwater.get(i).getWaterBodyPicPath());
            lStringBuilder.append("\""+imagename+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedBy\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+userName()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Lat\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLat());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Long\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLongg());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Location\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getLocation()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedOn\"");
            lStringBuilder.append(":");
            lStringBuilder.append("null");
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyId\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getWaterBodyId()+"\"");
            lStringBuilder.append("");
            lStringBuilder.append(",");

            lStringBuilder.append("\"status\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getStatus()+"\"");
            lStringBuilder.append("");

            lStringBuilder.append("}");
            if(i<listAddwater.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
        lStringBuilder.append("}");
        sSaveDateAddWaterBody= lStringBuilder.toString();
        System.out.print("");
        Log.d("sSaveDateAddWaterBody",sSaveDateAddWaterBody);
    }
    
    public static String removeWords(String word ) {
        return word.replace("/storage/emulated/0/Pictures/","");
    }

    private void converttblFisheryPlan(String wbids,String Pgcode){

      /*  List<FisheryPlantbl> fisheryPlantbls = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                .where(Condition.prop("isexported").eq("false"))
                .groupBy("fisheryplanid")
                .list();*/

        StringBuilder lStringBuilder = new StringBuilder();
      /*  lStringBuilder.append("{");
        lStringBuilder.append("\"tbltracFisheryPlan\"");
        lStringBuilder.append(":");*/
        lStringBuilder.append("[");

        for (int i = 0; i< fisheryPlantblList.size(); i++){

            lStringBuilder.append("{");
            lStringBuilder.append("\"fisheryplanid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getFisheryplanid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"ID\"");
            lStringBuilder.append(":");
            lStringBuilder.append(0);
            lStringBuilder.append(",");

            lStringBuilder.append("\"waterbodyid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getWaterbodyid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"fisheryplandate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getFisheryplandate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"selectedfishery\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getSelectedfishery()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"totalamount\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getTotalamount()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"createdby\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getCreatedby()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"isexported\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+"true"+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"individualcost\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getIndividualcost()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"budgetcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getBudgetcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"pgcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getPgcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpmemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getGrpmemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getGrpcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"areadependent\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getAreadependent()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"year\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getYear()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"month\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getMonth()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"itemname\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getItemname()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"itemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+fisheryPlantblList.get(i).getItemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"status\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+"1"+"\"");

            lStringBuilder.append("}");
            if(i<fisheryPlantblList.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
     //   lStringBuilder.append("}");
        tbltracFisheryPlans= lStringBuilder.toString();
        System.out.print("");
        Log.d("tbltracFisheryPlan",tbltracFisheryPlans);
    }

    private void converttblcropplanningmemebrs(String wbids,String Pgcode){
        List<Cropplanningmemberstbl> cropplanningmemberstbls = Select.from(Cropplanningmemberstbl.class)
                .where(Condition.prop("pgcode").eq(Pgcode))
                .where(Condition.prop("waterbodyid").eq(wbids))
                .where(Condition.prop("fisheryplanid").eq(croppplaningid))
                .where(Condition.prop("isexported").eq("0"))
                .list();

        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("[");
        for (int i = 0; i< cropplanningmemberstbls.size(); i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"SelectedMemberUUID\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getSelectedmemberuuid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Membername\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getMembername()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"pgcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getPgcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpmemcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getGrpmemcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"grpcode\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getGrpcode()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"isexported\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getIsexported()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"waterbodyId\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getWaterbodyId()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"fisheryplanid\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getFisheryplanid()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createddate\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getCreateddate()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Createdby\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+cropplanningmemberstbls.get(i).getCreatedby()+"\"");

            lStringBuilder.append("}");
            if(i<cropplanningmemberstbls.size()-1) {
                lStringBuilder.append(",");
            }
        }
        lStringBuilder.append("]");
        saveselectedmembers= lStringBuilder.toString();
        System.out.print("");
        Log.d("saveselectedmembers",saveselectedmembers);
    }


    public void callUploadWaterBody(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(IncomeExpActivity.this)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(IncomeExpActivity.this);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_tblAddWaterBody,
                    AppConstant.Upload_tblAddWaterBodystatus, this).postWaterBody(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(IncomeExpActivity.this)
                    .text(IncomeExpActivity.this.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void callUploadFisheryPlan(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(IncomeExpActivity.this)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(IncomeExpActivity.this);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_tbltracFisheryPlan,
                    AppConstant.Upload_tbltracFisheryPlanstatus, this).posttracFisheryPlan(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(IncomeExpActivity.this)
                    .text(IncomeExpActivity.this.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void callUploadFisherySelectedMembers(String jsonData,String versionString) {
        CheckConnectivity checkConnectivity = new CheckConnectivity();
        if(checkConnectivity.CheckConnection(IncomeExpActivity.this)){
            StringRequest mStringRequest;
            RequestQueue mRequestQueue;
            mRequestQueue = Volley.newRequestQueue(IncomeExpActivity.this);
            mStringRequest = new VolleyString(AppConstant.domain+"/"+ AppConstant.Upload_FisherySelectedMembers_DataInsert,
                    AppConstant.Upload_FisherySelectedMembers_DataInsertstatus, this).postFisherySelectedMembers(jsonData,versionString);
            mRequestQueue.add(mStringRequest);
        }
        else{
            new StyleableToast
                    .Builder(IncomeExpActivity.this)
                    .text(IncomeExpActivity.this.getString(R.string.internet_error))
                    .iconStart(R.drawable.wrong_icon_white)
                    .textColor(Color.WHITE)
                    .backgroundColor(IncomeExpActivity.this.getResources().getColor(R.color.colorPrimary))
                    .show();
        }
    }

    public void executeSql(String Sql) {
        try {
            if (dbcpa_db == null) {
                dbcpa_db = dbHelper.getDatabase();
            }
            dbcpa_db.execSQL(Sql);
            Log.d("show",listAddwater.get(0).getIsExported());
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in executeSql :: " + exception.getMessage());
        }
    }

    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(IncomeExpActivity.this.getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show(IncomeExpActivity.this);
    }

    private void getVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionString = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}