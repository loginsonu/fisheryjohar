package com.jslpsmis.fishery.newmodule;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.R;
import com.jslpsmis.fishery.DashboardActivity;
import com.jslpsmis.fishery.MultiSpinner;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.LocationClass;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterBodySpinnerModel;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AddWaterBodyFormNew extends AppCompatActivity  implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private MultiSpinner spinnerMulti;
    private ArrayAdapter<String> adapter;
    TextView memberName,shg;
    private static final int LOCATION_PERMISSION_ID = 1001;

    String[] permissions = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    LocationClass locationClass;
    double latitute,longititude;
    String waterBodyLocation,waterBodyLocationAccurate;
    MagicalCamera magicalCamera;
    MagicalPermissions magicalPermissions;
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 20;
    PopupWindow popupWindow;
    boolean imageCheckZoomFarm = false;
    String membersselected="";
    //String unique = DashboardActivity.pgCode + DashboardActivity.grpMemberCode + DashboardActivity.grpCode;

    Spinner spinner,spinner2,spinner3,spinner4,spinner5,spinner6;
    EditText questiontwodependent,areaId,fishAverageProduction,fishAverageValue;
    ImageView imageFarmPIcture;
    TextView tvFarmPic;
    boolean imageCheck;
    ImageView close;
    String pathWaterBodyPic = "";
    Dataprovider dataprovider;
    int count=0;

    List<AddWaterBodySpinnerModel> waterTypeList;
    List<AddWaterBodySpinnerModel> waterOwnerList;

    List<AddWaterBodySpinnerModel> selectedFisheryList;
    List<String> waterTypeBodyName = new ArrayList<>();
    List<String> waterTypeBodyCode = new ArrayList<>();
    public ArrayAdapter<String> waterTypeAdapter;

    List<String> waterOwnerName = new ArrayList<>();
    List<String> waterOwnerCode = new ArrayList<>();
    public ArrayAdapter<String> waterOwnerAdapter;

    List<String> selectedFisheryName = new ArrayList<>();
    List<String> selectedFisheryCode= new ArrayList<>();
    public ArrayAdapter<String> selectedFisheryAdapter;
    Button save,update;

    String waterTypeString="";
    String waterOwnerString="";
    String waterAvailabilityString="";
    String fisheryString="";
    String isFisheryHappening="";
    String isfishKeeperTakenTraining="";
    String GUID;

    List<String> pgCodeToBeAdded = new ArrayList<>();
    List<String> grpMemCodeToBeAdded = new ArrayList<>();
    List<String> grpCodeToBeAdded = new ArrayList<>();
    List<String> grpmemToBeAdded = new ArrayList<>();

    List<String> pgCodeRes = new ArrayList<>();
    List<String> grpMemCode = new ArrayList<>();
    List<String> grpCode = new ArrayList<>();
    List<String> memberNameslist=new ArrayList<>();

    private static String waterBodyId;
    Uri fileUri;
    String photoPath = "";
    public static String pgName,pgCode,flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_water_body_form_new);

        // Location permission not granted
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_ID);
            return;
        }
        init();
    }

    private void init() {
        Intent intent = getIntent();
        pgCode = intent.getStringExtra("pgcode");
        pgName = intent.getStringExtra("pgname");
        memberName = findViewById(R.id.textView52);
        shg = findViewById(R.id.textView53);
        magicalPermissions = new MagicalPermissions(this, permissions);
        magicalCamera = new MagicalCamera(this,RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);

        spinner = findViewById(R.id.spinner);
        spinner2 = findViewById(R.id.spinner2);
        spinner3 = findViewById(R.id.spinner3);
        spinner4 = findViewById(R.id.spinner4);
        spinner5 = findViewById(R.id.spinner5);
        spinner6 = findViewById(R.id.spinner6);
        save = findViewById(R.id.btnnSaveBaseLineLandInfoForm);
        update = findViewById(R.id.btnUpdateBaseLineLandInfoForm);

        save.setVisibility(View.VISIBLE);
        update.setVisibility(View.GONE);

        save.setOnClickListener(this);
        update.setOnClickListener(this);

        questiontwodependent = findViewById(R.id.questiontwodependent);
        areaId = findViewById(R.id.areaId);
        fishAverageProduction = findViewById(R.id.fishAverageProduction);
        fishAverageValue= findViewById(R.id.fishAverageValue);

        dataprovider = new Dataprovider(this);
        imageFarmPIcture = findViewById(R.id.imageFarmPIcture);
        tvFarmPic = findViewById(R.id.tvFarmPic);

        imageFarmPIcture.setOnClickListener(this);
        tvFarmPic.setOnClickListener(this);

        memberName.setText(DashboardActivity.memberName);
        shg.setText(DashboardActivity.grpName);

        locationClass = new LocationClass(AddWaterBodyFormNew.this);
        latitute = LocationClass.lat;
        longititude = LocationClass.longi;
        waterBodyLocation = locationClass.getLocation();
        waterBodyLocationAccurate = LocationClass.currentLocation;

        waterTypeList = dataprovider.getwaterBodyTypeList();
        waterOwnerList = dataprovider.getwaterBodyOwnerList();
        selectedFisheryList = dataprovider.getSelectedFisheryList();

        waterBodyId = random()+pgCode;

        if(waterTypeList.size()>0){
            waterTypeBodyName.add("तालाब का प्रकार चुने");
            waterTypeBodyCode.add("");
            for(int i =0;i<waterTypeList.size();i++){
                String name = waterTypeList.get(i).getName();
                String code = waterTypeList.get(i).getCode();
                waterTypeBodyName.add(name);
                waterTypeBodyCode.add(code);
            }
            waterTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, waterTypeBodyName);
            spinner.setAdapter(waterTypeAdapter);
        }

        if(waterOwnerList.size()>0){
            waterOwnerName.add("तालाब किसका है?");
            waterOwnerCode.add("");
            for(int i =0;i<waterOwnerList.size();i++){
                String name = waterOwnerList.get(i).getName();
                String code = waterOwnerList.get(i).getCode();
                waterOwnerName.add(name);
                waterOwnerCode.add(code);
            }
            waterOwnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, waterOwnerName);
            spinner2.setAdapter(waterOwnerAdapter);
        }

        if(selectedFisheryList.size()>0){
            selectedFisheryName.add("चयनित मत्स्यकी");
            selectedFisheryCode.add("");
            for(int i =0;i<selectedFisheryList.size();i++){
                String name = selectedFisheryList.get(i).getName();
                String code = selectedFisheryList.get(i).getCode();
                selectedFisheryName.add(name);
                selectedFisheryCode.add(code);
            }
            selectedFisheryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, selectedFisheryName);
            spinner4.setAdapter(selectedFisheryAdapter);
        }

        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);
        spinner3.setOnItemSelectedListener(this);
        spinner4.setOnItemSelectedListener(this);
        spinner5.setOnItemSelectedListener(this);
        spinner6.setOnItemSelectedListener(this);

        ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
        list= dataprovider.getProducerGrpMemList(pgCode);

        for(int k =0;k<list.size();k++){
            if(!list.get(k).getMemberName().equals("")) {
                adapter.add(list.get(k).getMemberName());
            }
        }
        spinnerMulti.setAdapter(adapter, false, onSelectedListener);
    }

    MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            grpCodeToBeAdded.clear();
            grpMemCodeToBeAdded.clear();
            memberNameslist.clear();
            ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
            list= dataprovider.getProducerGrpMemList(pgCode);
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    boolean b = selected[i];
                    if (b) {
                        count++;
                        grpMemCodeToBeAdded.add(list.get(i).getGroupMemberCode());
                        grpCodeToBeAdded.add(list.get(i).getGroupCode());
                        memberNameslist.add(list.get(i).getMemberName());
                        if(membersselected.equals(""))
                        {
                            membersselected=list.get(i).getMemberName();
                        }else {
                            membersselected=membersselected+","+list.get(i).getMemberName();
                        }
                    }
                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.tvFarmPic:
                startingCameraIntent();
                imageCheck = true;
                break;
            case R.id.imageFarmPIcture:
                if(imageCheckZoomFarm) {
                    LayoutInflater liFarm = (LayoutInflater) AddWaterBodyFormNew.this.getSystemService(LAYOUT_INFLATER_SERVICE);
                    View layoutFarm = liFarm.inflate(R.layout.image_zoom_layout, (ViewGroup) findViewById(R.id.toastId));
                    com.jsibbold .zoomage.ZoomageView imageFarm = (com.jsibbold.zoomage.ZoomageView) layoutFarm.findViewById(R.id.myZoomageView);
                    close = (ImageView) layoutFarm.findViewById(R.id.btnClose);
                    imageFarm.setImageDrawable(imageFarmPIcture.getDrawable());

                    close.setOnClickListener(this);
                    popupWindow = new PopupWindow(layoutFarm, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, false);
                    popupWindow.showAtLocation(layoutFarm, Gravity.CENTER, 0, 0);
                }
                break;
            case R.id.btnClose:
                popupWindow.dismiss();
                break;

            case R.id.btnnSaveBaseLineLandInfoForm:
                validation();
                break;

            case  R.id.btnUpdateBaseLineLandInfoForm:
                validation1();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //new camera code
        if (resultCode == Activity.RESULT_OK)
        {
            try
            {
                photoPath = getPath(fileUri);
                Bitmap b = decodeUri(fileUri);
                imageCheckZoomFarm = true;
                imageFarmPIcture.setImageBitmap(b);
                pathWaterBodyPic =photoPath;
                System.out.print("");
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //new code for camera working in all devices
    private void startingCameraIntent()
    {
        String fileName = System.currentTimeMillis()+".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, 1);
    }

    @SuppressWarnings("deprecation")
    private String getPath(Uri selectedImaeUri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(selectedImaeUri, projection, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }
        return selectedImaeUri.getPath();
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
    {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver()
                .openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 400;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true)
        {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
            {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver()
                .openInputStream(selectedImage), null, o2);

        return bitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationClass = new LocationClass(AddWaterBodyFormNew.this);
        latitute = LocationClass.lat;
        longititude = LocationClass.longi;
        waterBodyLocation = locationClass.getLocation();
        waterBodyLocationAccurate = LocationClass.currentLocation;

        if(Constant.isEdit.equals("true")){
            spinnerMulti.setEnabled(false);
            Constant.isEdit = "false";
            save.setVisibility(View.GONE);
            update.setVisibility(View.VISIBLE);
            Intent intent = getIntent();
            waterTypeString = intent.getStringExtra("waterType");
            waterOwnerString = intent.getStringExtra("waterOwner");
            waterAvailabilityString = intent.getStringExtra("waterAvailable");
            fisheryString = intent.getStringExtra("selectedFishery");
            isFisheryHappening = intent.getStringExtra("fishFarming");
            isfishKeeperTakenTraining = intent.getStringExtra("fishTraining");
            String questiontwodependentt = intent.getStringExtra("fromWhereTraining");
            String area = intent.getStringExtra("area");
            String production = intent.getStringExtra("production");
            String income = intent.getStringExtra("income");
            GUID = intent.getStringExtra("GUID");
            pathWaterBodyPic = intent.getStringExtra("imageUrl");

            setSpinText(spinner,waterTypeString);
            setSpinText(spinner2,waterOwnerString);
            setSpinText(spinner3,waterAvailabilityString);
            setSpinText(spinner4,fisheryString);
            setSpinText(spinner5,isFisheryHappening);
            setSpinText(spinner6,isfishKeeperTakenTraining);

            questiontwodependent.setText(questiontwodependentt);
            areaId.setText(area);
            fishAverageProduction.setText(production);
            fishAverageValue.setText(income);
            imageFarmPIcture.setImageBitmap(BitmapFactory.decodeFile(pathWaterBodyPic));
            System.out.print("");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long id) {
        switch (adapterView.getId()){
            case R.id.spinner:
                if(waterTypeBodyCode.size()>0) {
                    waterTypeString = waterTypeBodyCode.get(i);
                }
                break;
            case R.id.spinner2:
                if(waterOwnerCode.size()>0){
                    waterOwnerString = waterOwnerCode.get(i);
                }
                break;
            case R.id.spinner3:
                waterAvailabilityString = adapterView.getItemAtPosition(i).toString();
                break;
            case R.id.spinner4:
                if(selectedFisheryCode.size()>0){
                    fisheryString = selectedFisheryCode.get(i);
                }
                break;
            case R.id.spinner5:
                isFisheryHappening = adapterView.getItemAtPosition(i).toString();
                break;
            case R.id.spinner6:
                isfishKeeperTakenTraining = adapterView.getItemAtPosition(i).toString();
                if(isfishKeeperTakenTraining.equals("हां")){
                    questiontwodependent.setVisibility(View.VISIBLE);
                }else{
                    questiontwodependent.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void validation() {
        if(waterTypeString.equals("")){
            alert("गलती","तालाब का प्रकार चुने");
        }
        else if(waterOwnerString.equals("")){
            alert("गलती","तालाब किसका है?");
        }
        else if (waterAvailabilityString.equals("कितने महीने पानी उपलब्ध रहता है चुने")){
            alert("गलती","कितने महीने पानी उपलब्ध रहता है चुने");
        }
        else if(fisheryString.equals("")){
            alert("गलती","चयनित मत्स्यकी");
        }
        else if(isFisheryHappening.equals("क्या इस तालाब मै मत्स्य पालन हो रही है?")){
            alert("गलती","क्या इस तालाब मै मत्स्य पालन हो रही है?");
        }
        else if(isfishKeeperTakenTraining.equals("क्या मत्स्य पालक इसके लिए प्रशीक्षण प्राप्त किए हुए है?")){
            alert("गलती","क्या मत्स्य पालक इसके लिए प्रशीक्षण प्राप्त किए हुए है?");
        }
        else if(areaId.getText().toString().equals("")){
            alert("गलती","अनुमानित क्षेत्रफल(ड़ीसमिल मे)");
        }
        else if (fishAverageProduction.getText().toString().equals("")){
            alert("गलती","माछली का औसत उत्पादन(क्विंटल)(पिछले साल)");
        }
        else if (fishAverageValue.getText().toString().equals("")){
            alert("गलती","माछली का औसात विक्रय मूल्य(रुपये)(पिछले साल)");
        }else if(latitute==0){
            alert("गलती","लोकेशन नहीं है, पुनः प्रयास करें");
        }
        else{
            if(pgCodeToBeAdded.size()>0){
                for(int i=0;i<pgCodeToBeAdded.size();i++){
                    boolean check = dataprovider.AddWaterBody(waterBodyId,pgCodeToBeAdded.get(i),grpMemCodeToBeAdded.get(i),grpCodeToBeAdded.get(i),waterTypeString,waterOwnerString,waterAvailabilityString,
                            fisheryString,isFisheryHappening,isfishKeeperTakenTraining,questiontwodependent.getText().toString(),areaId.getText().toString(),fishAverageProduction.getText().toString(),
                            fishAverageValue.getText().toString(),pathWaterBodyPic,
                            latitute+"",longititude+"",waterBodyLocation,"","insert");
                }
            }else{
                boolean check = dataprovider.AddWaterBody(waterBodyId,pgCode, DashboardActivity.grpMemberCode, DashboardActivity.grpCode,waterTypeString,waterOwnerString,waterAvailabilityString,
                        fisheryString,isFisheryHappening,isfishKeeperTakenTraining,questiontwodependent.getText().toString(),areaId.getText().toString(),fishAverageProduction.getText().toString(),
                        fishAverageValue.getText().toString(),pathWaterBodyPic,
                        latitute+"",longititude+"",waterBodyLocation,"","insert");
            }
            alert1(getString(R.string.success),getString(R.string.success_msg));
        }
    }

    private void validation1() {
        if(waterTypeString.equals("")){
            alert("गलती","तालाब का प्रकार चुने");
        }
        else if(waterOwnerString.equals("")){
            alert("गलती","तालाब किसका है?");
        }
        else if (waterAvailabilityString.equals("कितने महीने पानी उपलब्ध रहता है चुने")){
            alert("गलती","कितने महीने पानी उपलब्ध रहता है चुने");
        }
        else if(fisheryString.equals("")){
            alert("गलती","चयनित मत्स्यकी");
        }
        else if(isFisheryHappening.equals("क्या इस तालाब मै मत्स्य पालन हो रही है?")){
            alert("गलती","क्या इस तालाब मै मत्स्य पालन हो रही है?");
        }
        else if(isfishKeeperTakenTraining.equals("क्या मत्स्य पालक इसके लिए प्रशीक्षण प्राप्त किए हुए है?")){
            alert("गलती","क्या मत्स्य पालक इसके लिए प्रशीक्षण प्राप्त किए हुए है?");
        }
        else if(questiontwodependent.getText().toString().equals("")){
            alert("गलती","यदि हा तो कहा से प्राप्त हुऐ है?");
        }
        else if(areaId.getText().toString().equals("")){
            alert("गलती","अनुमानित क्षेत्रफल(ड़ीसमिल मे)");
        }
        else if (fishAverageProduction.getText().toString().equals("")){
            alert("गलती","माछली का औसत उत्पादन(क्विंटल)(पिछले साल)");
        }
        else if (fishAverageValue.getText().toString().equals("")){
            alert("गलती","माछली का औसात विक्रय मूल्य(रुपये)(पिछले साल)");
        }
        else{
            boolean check = dataprovider.AddWaterBody(waterBodyId,pgCode, DashboardActivity.grpMemberCode, DashboardActivity.grpCode,waterTypeString,waterOwnerString,waterAvailabilityString,
                    fisheryString,isFisheryHappening,isfishKeeperTakenTraining,questiontwodependent.getText().toString(),areaId.getText().toString(),fishAverageProduction.getText().toString(),
                    fishAverageValue.getText().toString(),pathWaterBodyPic,
                    latitute+"",longititude+"",waterBodyLocation,GUID,"update");
            if(check){
                alert1(getString(R.string.success),getString(R.string.update_msg));
            }
        }
    }

    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {

        }));
        alert.show(AddWaterBodyFormNew.this);
    }

    private void alert1(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.close), AlertActionStyle.DEFAULT, action -> {
            finish();
        }));
        alert.show(AddWaterBodyFormNew.this);
    }

    public void setSpinText(Spinner spin, String text)
    {
        for(int i= 0; i < spin.getAdapter().getCount(); i++)
        {
            if(spin.getAdapter().getItem(i).toString().contains(text))
            {
                spin.setSelection(i);
            }
        }
    }

    public String random() {
        char[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                .toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 30; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

}
