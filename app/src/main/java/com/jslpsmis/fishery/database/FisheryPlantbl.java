package com.jslpsmis.fishery.database;

import com.orm.SugarRecord;

public class FisheryPlantbl extends SugarRecord {

    // in this table areaindenpent items should be saved only one time because farmer will get it one time only
    private String fisheryplanid;
    private String waterbodyid;
    private String fisheryplandate;
    private String selectedfishery;
    private String totalamount;
    private String createdby;
    private String isexported;
    private String individualcost;
    private String budgetcode;
    private String pgcode;
    private String grpmemcode;
    private String grpcode;
    private String areadependent;
    private String year;
    private String month;
    private String itemname;
    private String itemcode;
    private String status;

    public FisheryPlantbl() {
    }

    public FisheryPlantbl(String fisheryplanid, String waterbodyid, String fisheryplandate, String selectedfishery, String totalamount, String createdby, String isexported, String individualcost, String budgetcode, String pgcode, String grpmemcode, String grpcode, String areadependent, String year, String month, String itemname, String itemcode, String status) {
        this.fisheryplanid = fisheryplanid;
        this.waterbodyid = waterbodyid;
        this.fisheryplandate = fisheryplandate;
        this.selectedfishery = selectedfishery;
        this.totalamount = totalamount;
        this.createdby = createdby;
        this.isexported = isexported;
        this.individualcost = individualcost;
        this.budgetcode = budgetcode;
        this.pgcode = pgcode;
        this.grpmemcode = grpmemcode;
        this.grpcode = grpcode;
        this.areadependent = areadependent;
        this.year = year;
        this.month = month;
        this.itemname = itemname;
        this.itemcode = itemcode;
        this.status = status;
    }

    public String getFisheryplanid() {
        return fisheryplanid;
    }

    public void setFisheryplanid(String fisheryplanid) {
        this.fisheryplanid = fisheryplanid;
    }

    public String getWaterbodyid() {
        return waterbodyid;
    }

    public void setWaterbodyid(String waterbodyid) {
        this.waterbodyid = waterbodyid;
    }

    public String getFisheryplandate() {
        return fisheryplandate;
    }

    public void setFisheryplandate(String fisheryplandate) {
        this.fisheryplandate = fisheryplandate;
    }

    public String getSelectedfishery() {
        return selectedfishery;
    }

    public void setSelectedfishery(String selectedfishery) {
        this.selectedfishery = selectedfishery;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getIsexported() {
        return isexported;
    }

    public void setIsexported(String isexported) {
        this.isexported = isexported;
    }

    public String getIndividualcost() {
        return individualcost;
    }

    public void setIndividualcost(String individualcost) {
        this.individualcost = individualcost;
    }

    public String getBudgetcode() {
        return budgetcode;
    }

    public void setBudgetcode(String budgetcode) {
        this.budgetcode = budgetcode;
    }

    public String getPgcode() {
        return pgcode;
    }

    public void setPgcode(String pgcode) {
        this.pgcode = pgcode;
    }

    public String getGrpmemcode() {
        return grpmemcode;
    }

    public void setGrpmemcode(String grpmemcode) {
        this.grpmemcode = grpmemcode;
    }

    public String getGrpcode() {
        return grpcode;
    }

    public void setGrpcode(String grpcode) {
        this.grpcode = grpcode;
    }

    public String getAreadependent() {
        return areadependent;
    }

    public void setAreadependent(String areadependent) {
        this.areadependent = areadependent;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
