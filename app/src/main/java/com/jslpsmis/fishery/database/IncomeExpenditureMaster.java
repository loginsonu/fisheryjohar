package com.jslpsmis.fishery.database;

import com.orm.SugarRecord;

public class IncomeExpenditureMaster extends SugarRecord {

    private int Incomeexpenditureid;
    private String Incomeexpenditurename;
    private String Incomeexpenditureflag;
    private String isexported;

    public IncomeExpenditureMaster(){

    }

    public IncomeExpenditureMaster(int incomeexpenditureid, String incomeexpenditurename, String incomeexpenditureflag, String isexported) {
        Incomeexpenditureid = incomeexpenditureid;
        Incomeexpenditurename = incomeexpenditurename;
        Incomeexpenditureflag = incomeexpenditureflag;
        this.isexported = isexported;
    }

    public int getIncomeexpenditureid() {
        return Incomeexpenditureid;
    }

    public void setIncomeexpenditureid(int incomeexpenditureid) {
        Incomeexpenditureid = incomeexpenditureid;
    }

    public String getIncomeexpenditurename() {
        return Incomeexpenditurename;
    }

    public void setIncomeexpenditurename(String incomeexpenditurename) {
        Incomeexpenditurename = incomeexpenditurename;
    }

    public String getIncomeexpenditureflag() {
        return Incomeexpenditureflag;
    }

    public void setIncomeexpenditureflag(String incomeexpenditureflag) {
        Incomeexpenditureflag = incomeexpenditureflag;
    }

    public String getIsexported() {
        return isexported;
    }

    public void setIsexported(String isexported) {
        this.isexported = isexported;
    }

    @Override
    public String toString() {
        return Incomeexpenditurename;
    }
}
