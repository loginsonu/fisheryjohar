package com.jslpsmis.fishery.database;

import com.orm.SugarRecord;

public class tblIncomeExpenditure extends SugarRecord {

    private String incomeexpenditureuuid;
    private String incomeexpenditurecode;
    private double totalamount;
    private String Pgcode;
    private String waterbodyid;
    private String selecteddate;
    private String Inexflag;
    private double rate;
    private String unit;
    private double quantity;
    private String cropplanningid;
    private String finalsubmitstatus;
    private String isexported;
    private String Createdby;
    private String Createddate;


    public tblIncomeExpenditure(){

    }

    public tblIncomeExpenditure(String incomeexpenditureuuid, String incomeexpenditurecode, double totalamount, String pgcode, String waterbodyid, String selecteddate, String flag, double rate, String unit, double quantity, String cropplanningid, String status, String isexported, String createdby, String createddate) {
        this.incomeexpenditureuuid = incomeexpenditureuuid;
        this.incomeexpenditurecode = incomeexpenditurecode;
        this.totalamount = totalamount;
        Pgcode = pgcode;
        this.waterbodyid = waterbodyid;
        this.selecteddate = selecteddate;
        this.Inexflag = flag;
        this.rate = rate;
        this.unit = unit;
        this.quantity = quantity;
        this.cropplanningid = cropplanningid;
        this.finalsubmitstatus = status;
        this.isexported = isexported;
        Createdby = createdby;
        Createddate = createddate;
    }

    public String getIncomeexpenditureuuid() {
        return incomeexpenditureuuid;
    }

    public void setIncomeexpenditureuuid(String incomeexpenditureuuid) {
        this.incomeexpenditureuuid = incomeexpenditureuuid;
    }

    public String getIncomeexpenditurecode() {
        return incomeexpenditurecode;
    }

    public void setIncomeexpenditurecode(String incomeexpenditurecode) {
        this.incomeexpenditurecode = incomeexpenditurecode;
    }

    public double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(double totalamount) {
        this.totalamount = totalamount;
    }

    public String getPgcode() {
        return Pgcode;
    }

    public void setPgcode(String pgcode) {
        Pgcode = pgcode;
    }

    public String getWaterbodyid() {
        return waterbodyid;
    }

    public void setWaterbodyid(String waterbodyid) {
        this.waterbodyid = waterbodyid;
    }

    public String getSelecteddate() {
        return selecteddate;
    }

    public void setSelecteddate(String selecteddate) {
        this.selecteddate = selecteddate;
    }

    public String getFlag() {
        return Inexflag;
    }

    public void setFlag(String flag) {
        this.Inexflag = flag;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getCropplanningid() {
        return cropplanningid;
    }

    public void setCropplanningid(String cropplanningid) {
        this.cropplanningid = cropplanningid;
    }

    public String getStatus() {
        return finalsubmitstatus;
    }

    public void setStatus(String status) {
        this.finalsubmitstatus = status;
    }

    public String getIsexported() {
        return isexported;
    }

    public void setIsexported(String isexported) {
        this.isexported = isexported;
    }

    public String getCreatedby() {
        return Createdby;
    }

    public void setCreatedby(String createdby) {
        Createdby = createdby;
    }

    public String getCreateddate() {
        return Createddate;
    }

    public void setCreateddate(String createddate) {
        Createddate = createddate;
    }
}
