package com.jslpsmis.fishery.database;

import com.orm.SugarRecord;

public class tblwaterbodyselectedmembers extends SugarRecord {

    private String wbselectedmemberuuid;
    private String Membername;
    private String pgcode;
    private String grpmemcode;
    private String grpcode;
    private String isexported;
    private String waterbodyid;
    private String Createddate;
    private String Createdby;

    public tblwaterbodyselectedmembers(){

    }

    public String getWbselectedmemberuuid() {
        return wbselectedmemberuuid;
    }

    public void setWbselectedmemberuuid(String wbselectedmemberuuid) {
        this.wbselectedmemberuuid = wbselectedmemberuuid;
    }

    public String getMembername() {
        return Membername;
    }

    public void setMembername(String membername) {
        Membername = membername;
    }

    public String getPgcode() {
        return pgcode;
    }

    public void setPgcode(String pgcode) {
        this.pgcode = pgcode;
    }

    public String getGrpmemcode() {
        return grpmemcode;
    }

    public void setGrpmemcode(String grpmemcode) {
        this.grpmemcode = grpmemcode;
    }

    public String getGrpcode() {
        return grpcode;
    }

    public void setGrpcode(String grpcode) {
        this.grpcode = grpcode;
    }

    public String getIsexported() {
        return isexported;
    }

    public void setIsexported(String isexported) {
        this.isexported = isexported;
    }

    public String getWaterbodyid() {
        return waterbodyid;
    }

    public void setWaterbodyid(String waterbodyid) {
        this.waterbodyid = waterbodyid;
    }

    public String getCreateddate() {
        return Createddate;
    }

    public void setCreateddate(String createddate) {
        Createddate = createddate;
    }

    public String getCreatedby() {
        return Createdby;
    }

    public void setCreatedby(String createdby) {
        Createdby = createdby;
    }
}
