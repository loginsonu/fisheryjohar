package com.jslpsmis.fishery.database;

import com.orm.SugarRecord;

public class Cropplanningmemberstbl extends SugarRecord {

    private String selectedmemberuuid;
    private String Membername;
    private String pgcode;
    private String grpmemcode;
    private String grpcode;
    private String isexported;
    private String waterbodyid;
    private String fisheryplanid;
    private String Createddate;
    private String Createdby;

    public Cropplanningmemberstbl(){

     }

    public Cropplanningmemberstbl(String selectedmemberuuid, String membername, String pgcode, String grpmemcode, String grpcode, String isexported, String waterbodyId, String fisheryplanid, String createddate, String createdby) {
        this.selectedmemberuuid = selectedmemberuuid;
        Membername = membername;
        this.pgcode = pgcode;
        this.grpmemcode = grpmemcode;
        this.grpcode = grpcode;
        this.isexported = isexported;
        this.waterbodyid = waterbodyId;
        this.fisheryplanid = fisheryplanid;
        Createddate = createddate;
        Createdby = createdby;
    }

    public String getSelectedmemberuuid() {
        return selectedmemberuuid;
    }

    public void setSelectedmemberuuid(String selectedmemberuuid) {
        this.selectedmemberuuid = selectedmemberuuid;
    }

    public String getMembername() {
        return Membername;
    }

    public void setMembername(String membername) {
        Membername = membername;
    }

    public String getPgcode() {
        return pgcode;
    }

    public void setPgcode(String pgcode) {
        this.pgcode = pgcode;
    }

    public String getGrpmemcode() {
        return grpmemcode;
    }

    public void setGrpmemcode(String grpmemcode) {
        this.grpmemcode = grpmemcode;
    }

    public String getGrpcode() {
        return grpcode;
    }

    public void setGrpcode(String grpcode) {
        this.grpcode = grpcode;
    }

    public String getIsexported() {
        return isexported;
    }

    public void setIsexported(String isexported) {
        this.isexported = isexported;
    }

    public String getWaterbodyId() {
        return waterbodyid;
    }

    public void setWaterbodyId(String waterbodyId) {
        this.waterbodyid = waterbodyId;
    }

    public String getFisheryplanid() {
        return fisheryplanid;
    }

    public void setFisheryplanid(String fisheryplanid) {
        this.fisheryplanid = fisheryplanid;
    }

    public String getCreateddate() {
        return Createddate;
    }

    public void setCreateddate(String createddate) {
        Createddate = createddate;
    }

    public String getCreatedby() {
        return Createdby;
    }

    public void setCreatedby(String createdby) {
        Createdby = createdby;
    }
}
