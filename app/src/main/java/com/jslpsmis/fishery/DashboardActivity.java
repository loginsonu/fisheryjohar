package com.jslpsmis.fishery;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.jslpsmis.fishery.adapter.DashboardAdapter;
import com.jslpsmis.fishery.commonclass.CheckConnectivity;
import com.jslpsmis.fishery.commonclass.Constant;
import com.jslpsmis.fishery.commonclass.CreateJsonString;
import com.jslpsmis.fishery.commonclass.MDMSharedPreference;
import com.jslpsmis.fishery.commonclass.UploadDataToServerTable;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.AddWaterModel;
import com.jslpsmis.fishery.seeddata.SeedDataDashboard;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    public static String pgCode,grpCode,grpMemberCode,memberName,grpName;
    TextView tvFarmerName,pgName,shg;
    public static FloatingActionButton btnUpload;
    RecyclerView recyclerView;
    ImageView backBtn;
    DashboardAdapter aAdapter;
    public static String sSaveDateAddWaterBody;

    public static List<AddWaterModel> listAddwater;
    public static List<FisheryPlantbl> fisheryPlantblList;
    public static String sDataFisheryPlantbl;

    Dataprovider dataprovider;
    MDMSharedPreference mdmSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);
        init();
    }

    private void init() {
        mdmSharedPreference = MDMSharedPreference.getInstance(this);
        Intent intent = getIntent();
        pgCode = intent.getStringExtra("pgCode");
        grpCode = intent.getStringExtra("grpCode");
        grpMemberCode = intent.getStringExtra("grpMemberCode");
        memberName = intent.getStringExtra("memberName");
        grpName = intent.getStringExtra("shg");

        tvFarmerName =(TextView) findViewById(R.id.farmerNameActivityDashboard);
        backBtn = (ImageView) findViewById(R.id.backButtonDashboard);
        recyclerView = (RecyclerView) findViewById(R.id.list_dashboard);
        btnUpload = (FloatingActionButton) findViewById(R.id.uploadLandAndCropDetails);
        pgName = findViewById(R.id.textView9);
        shg = findViewById(R.id.textView37);
        btnUpload.setVisibility(View.GONE);

        tvFarmerName.setText(getString(R.string.dashboard)+"("+memberName+")");
        pgName.setText(ProducerMemberActivity.pgName);
        shg.setText(grpName);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert2(getString(R.string.notice),getString(R.string.notice_msg));
            }
        });

    }

    private void alert(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.try_again), AlertActionStyle.DEFAULT, action -> {
        }));
        alert.show(DashboardActivity.this);
    }


    private void alert2(String error,String message){
        AlertView alert = new AlertView(error, message, AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.upload), AlertActionStyle.DEFAULT, action -> {
            if(listAddwater.size()>0||fisheryPlantblList.size()>0){
                btnUpload.setVisibility(View.GONE);

                CheckConnectivity checkConnectivity = new CheckConnectivity();
                if(checkConnectivity.CheckConnection(DashboardActivity.this)){

                    if(listAddwater.size()>0){
                        converttblAddWaterBody();
                        new UploadDataToServerTable(DashboardActivity.this, AppConstant.Upload_tblAddWaterBody,sSaveDateAddWaterBody,"tblAddWaterBody");
                       // startService(new Intent(DashboardActivity.this, UploadWaterBody.class));
                        Constant.uploadWaterBodyInProgress = true;
                    }

                    if(fisheryPlantblList.size()>0){
                        CreateJsonString.CreateJson("tbltracFisheryPlan");
                        new UploadDataToServerTable(DashboardActivity.this, AppConstant.Upload_tbltracFisheryPlan,sDataFisheryPlantbl,"tbltracFisheryPlan");
                    }

                }else{

                    alert(getString(R.string.net_problem),getString(R.string.net_msg));
                    if(listAddwater.size()>0||fisheryPlantblList.size()>0){
                        btnUpload.setVisibility(View.VISIBLE);
                    }
                }

            }
            else{
                alert(getString(R.string.errorr),getString(R.string.no_data_found));
                btnUpload.setVisibility(View.GONE);
            }
        }));
        alert.show(DashboardActivity.this);
    }

    private void converttblAddWaterBody(){
        StringBuilder lStringBuilder = new StringBuilder();
        lStringBuilder.append("{");
        lStringBuilder.append("\"tblAddWaterBody\"");
        lStringBuilder.append(":");
        lStringBuilder.append("[");
        for (int i= 0; i<listAddwater.size();i++){
            lStringBuilder.append("{");

            lStringBuilder.append("\"PgCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getPgCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpMemCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpMemberCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"GrpCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getGrpCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyOwnerCode\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getWaterBodyOwnerCode());
            lStringBuilder.append(",");

            lStringBuilder.append("\"NoOfMonthWaterAvailable\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getNoOfMonthAvailable()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"SelectedFishery\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getSelectedFishery());
            lStringBuilder.append(",");

            lStringBuilder.append("\"IsFisheryHappening\"");
            lStringBuilder.append(":");
            String yes = listAddwater.get(i).getIsFisheryHappenig();
            if(yes.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }

            lStringBuilder.append(",");

            lStringBuilder.append("\"IsTrainingTaken\"");
            lStringBuilder.append(":");
            String yes1 = listAddwater.get(i).getIsTrainingTaken();
            if(yes.equals("हां")){
                lStringBuilder.append(1);
            }else{
                lStringBuilder.append(0);
            }
            lStringBuilder.append(",");

            lStringBuilder.append("\"FromWhereTraining\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getFromwheretraing()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"ExpectedAreaInDecimal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalArea());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalProductionInQuintal\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalProduction());
            lStringBuilder.append(",");

            lStringBuilder.append("\"TotalIncome\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getTotalIncome());
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyPicPath\"");
            lStringBuilder.append(":");
            String imagename = removeWords(listAddwater.get(i).getWaterBodyPicPath());
            lStringBuilder.append("\""+imagename+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedBy\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+userName()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"Lat\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLat());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Long\"");
            lStringBuilder.append(":");
            lStringBuilder.append(listAddwater.get(i).getLongg());
            lStringBuilder.append(",");

            lStringBuilder.append("\"Location\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getLocation()+"\"");
            lStringBuilder.append(",");

            lStringBuilder.append("\"CreatedOn\"");
            lStringBuilder.append(":");
            lStringBuilder.append("null");
            lStringBuilder.append(",");

            lStringBuilder.append("\"WaterBodyId\"");
            lStringBuilder.append(":");
            lStringBuilder.append("\""+listAddwater.get(i).getWaterBodyId()+"\"");
            lStringBuilder.append("");

            lStringBuilder.append("}");
            if(i<listAddwater.size()-1) {
                lStringBuilder.append(",");
            }

        }
        lStringBuilder.append("]");
        lStringBuilder.append("}");
        sSaveDateAddWaterBody= lStringBuilder.toString();
        System.out.print("");
    }

    private void populatingRecyclerView() {
        aAdapter = new DashboardAdapter(this, SeedDataDashboard.getListData());
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(verticalLayoutmanager);
        recyclerView.setAdapter(aAdapter);
    }

    private String userName(){
        String user = mdmSharedPreference.getString("userNamee");
        return  user;
    }

    public static String removeWords(String word ) {
        return word.replace("/storage/emulated/0/Pictures/","");
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        dataprovider = new Dataprovider(this);
        populatingRecyclerView();

        if(!Constant.uploadWaterBodyInProgress) {
            listAddwater = dataprovider.getAddWaterListAll(DashboardActivity.pgCode);
        }else{
            listAddwater = new ArrayList<>();
        }

        if(!Constant.fisheryplanuploadinprogress){
            fisheryPlantblList = Select.from(FisheryPlantbl.class)
                    .where(Condition.prop("pgcode").eq(DashboardActivity.pgCode))
                    .where(Condition.prop("grpmemcode").eq( DashboardActivity.grpMemberCode))
                    .where(Condition.prop("grpcode").eq(DashboardActivity.grpCode))
                    .where(Condition.prop("isexported").eq("false"))
                    .list();

            System.out.print("");

            }else{
            fisheryPlantblList = new ArrayList<>();
        }

        if(listAddwater.size()>0||fisheryPlantblList.size()>0){
            btnUpload.setVisibility(View.VISIBLE);
        }
    }
}
