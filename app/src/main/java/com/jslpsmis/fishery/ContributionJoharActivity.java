package com.jslpsmis.fishery;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jslpsmis.fishery.adapter.ContributionJoharAdapter;
import com.jslpsmis.fishery.commonclass.BudgetCodeClass;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.model.ContributionJoharModel;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContributionJoharActivity extends AppCompatActivity {
    @BindView(R.id.spinner2)
    Spinner spinner2;
    private String currentYear;
    List<FisheryPlantbl> fisheryPlantblList;
    @BindView(R.id.backButtonCropPlanLandInfoActivity)
    ImageView backButtonCropPlanLandInfoActivity;
    List<ContributionJoharModel> contributionJoharModelList;
    @BindView(R.id.list)
    RecyclerView list;
    ContributionJoharAdapter aAdapter;

    float totalcost, totalcostjohar, totalcostben, totalcostcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribution_johar);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        currentYear = date();
        setSpinText(spinner2,currentYear);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currentYear = adapterView.getItemAtPosition(i).toString();
                populateRecycler();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setSpinText(Spinner spin, String text)
    {
        for(int i= 0; i < spin.getAdapter().getCount(); i++)
        {
            if(spin.getAdapter().getItem(i).toString().contains(text))
            {
                spin.setSelection(i);
            }
        }

    }

    private void populateRecycler() {
        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("pgcode").eq(InputIndentCalculationForWaterBody.Pgcode))
                .where(Condition.prop("waterbodyid").eq(InputIndentCalculationForWaterBody.waterBodyId))
                .where(Condition.prop("year").eq(currentYear))
                .list();

        totalcost = 0;
        totalcostjohar = 0;
        totalcostben = 0;
        totalcostcon = 0;

        contributionJoharModelList = new ArrayList<>();
        for (int i = 0; i < fisheryPlantblList.size(); i++) {
            String budgetCode = fisheryPlantblList.get(i).getBudgetcode();
            for (int j = 0; j < BudgetCodeClass.getListData().size(); j++) {
                String budgetcodej = BudgetCodeClass.getListData().get(j).getBudgetCode();
                if (budgetCode.equals(budgetcodej)) {
                    NumberFormat formatter = NumberFormat.getNumberInstance();
                    formatter.setMinimumFractionDigits(2);
                    formatter.setMaximumFractionDigits(2);

                    ContributionJoharModel model = new ContributionJoharModel();
                    model.setItemname(fisheryPlantblList.get(i).getItemname());
                    String unitcost = fisheryPlantblList.get(i).getIndividualcost();
                    model.setUnitcost(formatter.format(Float.parseFloat(unitcost)));

                    String joharPercentage = BudgetCodeClass.getListData().get(j).getJoharBank();
                    String BeneficiaryPer = BudgetCodeClass.getListData().get(j).getBenPer();
                    String convergencePer = BudgetCodeClass.getListData().get(j).getConPer();
                    Float johar = Float.parseFloat(joharPercentage);
                    Float ben = Float.parseFloat(BeneficiaryPer);
                    Float conv = Float.parseFloat(convergencePer);
                    Float cost = Float.parseFloat(unitcost);

                    Float joharFund = (johar / 100) * cost;
                    model.setJohar(formatter.format(joharFund));

                    Float benFund = (ben / 100) * cost;
                    model.setBeneficiary(formatter.format(benFund));

                    Float conFund = (conv / 100) * cost;
                    model.setConvergence(formatter.format(conFund));

                    contributionJoharModelList.add(model);

                    totalcost = totalcost + Float.parseFloat(unitcost);
                    totalcostcon = totalcostcon + conFund;
                    totalcostben = totalcostben + benFund;
                    totalcostjohar = totalcostjohar + joharFund;
                }
            }
        }
        ContributionJoharModel model = new ContributionJoharModel();
        model.setItemname("Total");
        model.setUnitcost(totalcost + "");
        model.setJohar(totalcostjohar + "");
        model.setBeneficiary(totalcostben + "");
        model.setConvergence(totalcostcon + "");
        contributionJoharModelList.add(model);


        aAdapter = new ContributionJoharAdapter(this, contributionJoharModelList);

        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(verticalLayoutmanager);
        list.setAdapter(aAdapter);
    }

    @OnClick(R.id.backButtonCropPlanLandInfoActivity)
    public void onViewClicked() {
        finish();
    }

    private String date() {
        Calendar date = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
        return dateFormat.format(date.getTime());

    }
}
