package com.jslpsmis.fishery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jslpsmis.fishery.adapter.FisheryPlanAdapter;
import com.jslpsmis.fishery.database.FisheryPlantbl;
import com.jslpsmis.fishery.dataprovider.Dataprovider;
import com.jslpsmis.fishery.model.ProducerGroupMemberActivityModel;
import com.jslpsmis.fishery.newmodule.AddWaterbodynew;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputIndentCalculationForWaterBody extends AppCompatActivity {

    public static String waterBodyId;
    String waterBodyAreas;
    TextView pgName, shg, farmerName;
    TextView addFisheryPlan;
    Dataprovider dataprovider;

    //  ArrayList<FisheryPlanModel> fisheryPlanModelArrayList;
    FisheryPlanAdapter aAdapter;
    RecyclerView recyclerView;
    List<FisheryPlantbl> fisheryPlantblList;
    List<FisheryPlantbl> fisheryPlantblList1;
    List<String> fisheryPlanIdUniquelist;

    public static String Pgcode;
    public static String PgName;
    @BindView(R.id.contributionButton)
    FloatingActionButton contributionButton;
    String flag;
    ArrayList<String> CropPlanningMemberModel;
    ArrayList<String> grpMemCodeToBeAdded=new ArrayList<>();
    ArrayList<String> grpCodeToBeAdded=new ArrayList<>();
   // @BindView(R.id.backButtonCropPlanFormsec)
    ImageView backButtonCropPlanFormsec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_input_indent_calculation_for_water_body);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        dataprovider = new Dataprovider(this);
        Intent intent = getIntent();
        waterBodyId = intent.getStringExtra("waterBodyId");
        waterBodyAreas = intent.getStringExtra("wbArea");
        Pgcode= intent.getStringExtra("pgcode");
        PgName= intent.getStringExtra("grpname");
       // CropPlanningMemberModel=intent.getStringArrayListExtra("membersarray");
        try {
            CropPlanningMemberModel = (ArrayList<String>) getIntent().getSerializableExtra("membersarray");
        }catch (Exception e){
            e.printStackTrace();
        }
        flag=intent.getStringExtra("flag");
        pgName = findViewById(R.id.textView56);
        shg = findViewById(R.id.textView57);
        farmerName = findViewById(R.id.headingCropPlanLandInfo);
        recyclerView = findViewById(R.id.recyclerViewBaseLineLandInfo);

        //farmerName.setText("मत्स्य पालन योजना"+"("+DashboardActivity.memberName+")");
        pgName.setText(PgName);
        shg.setText("अनुमानित छेत्रफल = "+waterBodyAreas);

        addFisheryPlan = findViewById(R.id.dateCropPlanForm);
        if(flag.equals("view")){
            addFisheryPlan.setVisibility(View.GONE);
            contributionButton.hide();
        }else {
            addFisheryPlan.setVisibility(View.VISIBLE);
            contributionButton.show();
        }

        ArrayList<ProducerGroupMemberActivityModel> list = new ArrayList<>();
        list= dataprovider.getProducerGrpMemList(Pgcode);
        try {
            if (list.size() > 0) {
                for (int j = 0; j < CropPlanningMemberModel.size(); j++) {
                    String memebrs = CropPlanningMemberModel.get(j).toString();
                    for (int i = 0; i < list.size(); i++) {
                        if (memebrs.equals(list.get(i).getMemberName())) {
                            grpCodeToBeAdded.add(list.get(i).getGroupCode());
                            grpMemCodeToBeAdded.add(list.get(i).getGroupMemberCode());
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        addFisheryPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(InputIndentCalculationForWaterBody.this, FishBreedingWaterBodyListForm.class);
                intent1.putExtra("waterBodyId",waterBodyId);
                intent1.putExtra("wbArea",waterBodyAreas);
                intent1.putExtra("pgcode",Pgcode);
                intent1.putExtra("grpname",PgName);
                intent1.putExtra("membersarray",(Serializable)CropPlanningMemberModel);
                intent1.putExtra("grpCodeToBeAdded",(Serializable)grpCodeToBeAdded);
                intent1.putExtra("grpMemCodeToBeAdded",(Serializable)grpMemCodeToBeAdded);
                startActivity(intent1);
                finish();
            }
        });

        backButtonCropPlanFormsec=(ImageView)findViewById(R.id.backButtonCropPlanFormsec);
        backButtonCropPlanFormsec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(InputIndentCalculationForWaterBody.this, AddWaterbodynew.class);
                intent.putExtra("pgCode",Pgcode);
                intent.putExtra("pgName",PgName);
                intent.putExtra("grpname", AddWaterbodynew.grpname);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        populatingRecyclerView();
    }

    private void populatingRecyclerView() {
        Intent intent = getIntent();
        waterBodyId = intent.getStringExtra("waterBodyId");
        fisheryPlantblList = Select.from(FisheryPlantbl.class)
                .where(Condition.prop("waterbodyid").eq(waterBodyId))
                .list();

        fisheryPlantblList1 = new ArrayList<>();
        fisheryPlanIdUniquelist = new ArrayList<>();

        for (int i = 0; i < fisheryPlantblList.size(); i++) {
            String fisheryplanid = fisheryPlantblList.get(i).getFisheryplanid();
            if (!fisheryPlanIdUniquelist.contains(fisheryplanid)) {
                fisheryPlanIdUniquelist.add(fisheryplanid);
                FisheryPlantbl model = new FisheryPlantbl();
                model.setFisheryplandate(fisheryPlantblList.get(i).getFisheryplandate());
                model.setTotalamount(fisheryPlantblList.get(i).getTotalamount());
                model.setSelectedfishery(fisheryPlantblList.get(i).getSelectedfishery());
                model.setFisheryplanid(fisheryPlantblList.get(i).getFisheryplanid());
                model.setIsexported(fisheryPlantblList.get(i).getIsexported());
                fisheryPlantblList1.add(model);
            }
        }

        if (fisheryPlantblList1.size() > 0) {
            aAdapter = new FisheryPlanAdapter(this, fisheryPlantblList1,waterBodyAreas,waterBodyId,flag);
            LinearLayoutManager verticalLayoutmanager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(aAdapter);
        }
    }

    @OnClick(R.id.contributionButton)
    public void onViewClicked() {
        Intent intent = new Intent(InputIndentCalculationForWaterBody.this, ContributionJoharActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(InputIndentCalculationForWaterBody.this, AddWaterbodynew.class);
        intent.putExtra("pgCode",Pgcode);
        intent.putExtra("pgName",PgName);
        intent.putExtra("grpname", AddWaterbodynew.grpname);
        startActivity(intent);
        finish();
    }
}
