package com.jslpsmis.fishery.interactor;

public interface Updateable {
    public void update();
}
